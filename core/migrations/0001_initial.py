# Generated by Django 2.1.11 on 2019-12-01 16:48

import core.models.student_info
import core.models.user_account
from django.conf import settings
import django.contrib.auth.models
import django.contrib.auth.validators
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0009_alter_user_last_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserAccount',
            fields=[
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.', max_length=150, unique=True, validators=[django.contrib.auth.validators.UnicodeUsernameValidator()], verbose_name='username')),
                ('first_name', models.CharField(blank=True, max_length=30, verbose_name='first name')),
                ('last_name', models.CharField(blank=True, max_length=150, verbose_name='last name')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='email address')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('role', models.CharField(choices=[('Student', 'student'), ('Tutor', 'tutor'), ('Reviewer', 'reviewer')], max_length=50)),
                ('user_id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('fullname', models.CharField(blank=True, max_length=70, verbose_name='full name')),
                ('is_admin', models.BooleanField(default=False)),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
                'abstract': False,
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
                ('corrector', core.models.user_account.TutorReviewerManager()),
            ],
        ),
        migrations.CreateModel(
            name='ExamType',
            fields=[
                ('exam_type_id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('module_reference', models.CharField(max_length=50, unique=True)),
                ('total_score', models.PositiveIntegerField()),
                ('pass_score', models.PositiveIntegerField()),
                ('pass_only', models.BooleanField(default=False)),
            ],
            options={
                'verbose_name': 'ExamType',
                'verbose_name_plural': 'ExamTypes',
            },
        ),
        migrations.CreateModel(
            name='Feedback',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('score', models.DecimalField(decimal_places=2, default=0, max_digits=5)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('is_final', models.BooleanField(default=False)),
                ('final_by_reviewer', models.BooleanField(default=False)),
            ],
            options={
                'verbose_name': 'Feedback',
                'verbose_name_plural': 'Feedback Set',
            },
        ),
        migrations.CreateModel(
            name='FeedbackComment',
            fields=[
                ('comment_id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('text', models.TextField(blank=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('visible_to_student', models.BooleanField(default=True)),
                ('of_line', models.PositiveIntegerField(default=0)),
                ('of_feedback', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='feedback_lines', to='core.Feedback')),
                ('of_tutor', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='comment_list', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Feedback Comment',
                'verbose_name_plural': 'Feedback Comments',
                'ordering': ('created',),
            },
        ),
        migrations.CreateModel(
            name='FeedbackLabel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True)),
                ('description', models.TextField()),
                ('colour', models.CharField(default='#b0b0b0', max_length=7, validators=[django.core.validators.RegexValidator(code='nomatch', message='Colour must be in format: #[0-9A-F]{7}', regex='^#[0-9A-F]{6}$')])),
                ('feedback', models.ManyToManyField(related_name='labels', to='core.Feedback')),
                ('feedback_comments', models.ManyToManyField(related_name='labels', to='core.FeedbackComment')),
            ],
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('group_id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=120)),
            ],
        ),
        migrations.CreateModel(
            name='MetaSubmission',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('done_assignments', models.PositiveIntegerField(default=0)),
                ('has_active_assignment', models.BooleanField(default=False)),
                ('has_feedback', models.BooleanField(default=False)),
                ('has_final_feedback', models.BooleanField(default=False)),
                ('feedback_authors', models.ManyToManyField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='SolutionComment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('of_line', models.PositiveIntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='StudentInfo',
            fields=[
                ('student_id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('has_logged_in', models.BooleanField(default=False)),
                ('matrikel_no', models.CharField(default=core.models.student_info.random_matrikel_no, max_length=30, unique=True)),
                ('total_score', models.PositiveIntegerField(default=0)),
                ('passes_exam', models.BooleanField(default=False)),
                ('exam', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='students', to='core.ExamType')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='student', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Student',
                'verbose_name_plural': 'Student Set',
            },
        ),
        migrations.CreateModel(
            name='Submission',
            fields=[
                ('submission_id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('seen_by_student', models.BooleanField(default=False)),
                ('text', models.TextField(blank=True)),
                ('source_code', models.TextField(blank=True, editable=False, null=True)),
                ('source_code_available', models.BooleanField(default=False, editable=False)),
                ('student', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='submissions', to='core.StudentInfo')),
            ],
            options={
                'verbose_name': 'Submission',
                'verbose_name_plural': 'Submission Set',
                'ordering': ('type__name',),
            },
        ),
        migrations.CreateModel(
            name='SubmissionType',
            fields=[
                ('submission_type_id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=100, unique=True)),
                ('full_score', models.PositiveIntegerField(default=0)),
                ('description', models.TextField()),
                ('solution', models.TextField()),
                ('programming_language', models.CharField(choices=[('c', 'C syntax highlighting'), ('java', 'Java syntax highlighting'), ('mipsasm', 'Mips syntax highlighting'), ('haskell', 'Haskell syntax highlighting'), ('python', 'Python syntax highlighting'), ('plaintext', 'No syntax highlighting')], default='c', max_length=25)),
            ],
            options={
                'verbose_name': 'SubmissionType',
                'verbose_name_plural': 'SubmissionType Set',
            },
        ),
        migrations.CreateModel(
            name='Test',
            fields=[
                ('test_id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=30)),
                ('label', models.CharField(max_length=50)),
                ('annotation', models.TextField()),
                ('submission', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='tests', to='core.Submission')),
            ],
            options={
                'verbose_name': 'Test',
                'verbose_name_plural': 'Tests',
            },
        ),
        migrations.CreateModel(
            name='TutorSubmissionAssignment',
            fields=[
                ('assignment_id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('stage', models.CharField(choices=[('feedback-creation', 'No feedback was ever assigned'), ('feedback-validation', 'Feedback exists but is not validated'), ('feedback-review', 'Review by exam reviewer required')], default='feedback-creation', max_length=60)),
                ('is_done', models.BooleanField(default=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='assignments', to=settings.AUTH_USER_MODEL)),
                ('submission', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='assignments', to='core.Submission')),
            ],
        ),
        migrations.AddField(
            model_name='submission',
            name='type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='submissions', to='core.SubmissionType'),
        ),
        migrations.AddField(
            model_name='solutioncomment',
            name='of_submission_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='solution_comments', to='core.SubmissionType'),
        ),
        migrations.AddField(
            model_name='solutioncomment',
            name='of_user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='solution_comments', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='metasubmission',
            name='submission',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='meta', to='core.Submission'),
        ),
        migrations.AddField(
            model_name='feedback',
            name='of_submission',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='feedback', to='core.Submission'),
        ),
        migrations.AddField(
            model_name='useraccount',
            name='group',
            field=models.ManyToManyField(blank=True, related_name='group', to='core.Group'),
        ),
        migrations.AddField(
            model_name='useraccount',
            name='groups',
            field=models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups'),
        ),
        migrations.AddField(
            model_name='useraccount',
            name='user_permissions',
            field=models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions'),
        ),
        migrations.AlterUniqueTogether(
            name='test',
            unique_together={('submission', 'name')},
        ),
        migrations.AlterUniqueTogether(
            name='submission',
            unique_together={('type', 'student')},
        ),
        migrations.AlterUniqueTogether(
            name='feedbackcomment',
            unique_together={('of_line', 'of_tutor', 'of_feedback')},
        ),
    ]
