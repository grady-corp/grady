import logging

from django.db.models import Case, When, IntegerField, Sum, Q

from rest_framework import mixins, viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from core import models, permissions, serializers
from core.models import SubmissionType, FeedbackLabel

log = logging.getLogger(__name__)


class LabelApiViewSet(viewsets.GenericViewSet,
                      mixins.CreateModelMixin,
                      mixins.UpdateModelMixin,
                      mixins.ListModelMixin):
    permission_classes = (permissions.IsTutorOrReviewer, )
    queryset = models.FeedbackLabel.objects.all()
    serializer_class = serializers.LabelSerializer

    def get_permissions(self):
        if self.action == 'list':
            return [IsAuthenticated(), ]
        else:
            return super().get_permissions()


class LabelStatistics(viewsets.ViewSet):

    permission_classes = (permissions.IsTutorOrReviewer, )

    def list(self, *args, **kwargs):
        # TODO This is horribly ugly and should be killed with fire
        # however, i'm unsure whether there is a better way to retrieve the
        # information that hits the database less often
        labels = FeedbackLabel.objects.all()

        counts = list(SubmissionType.objects.annotate(
            **{str(label.pk): Sum(
                Case(
                    # if the feedback has a label or there is a visible comment with that
                    # label add 1 to the count
                    When(
                        Q(submissions__feedback__labels=label) |
                        Q(submissions__feedback__feedback_lines__labels=label) &
                        Q(submissions__feedback__feedback_lines__visible_to_student=True),
                        then=1),
                    output_field=IntegerField(),
                    default=0
                )
            ) for label in labels}
        ).values('pk', *[str(label.pk) for label in labels]))

        return Response(list(counts))
