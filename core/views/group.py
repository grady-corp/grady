import logging

from rest_framework import mixins, viewsets

from core import models, permissions, serializers

log = logging.getLogger(__name__)


class GroupApiViewSet(viewsets.GenericViewSet,
                      mixins.ListModelMixin):
    permission_classes = (permissions.IsTutorOrReviewer, )
    queryset = models.Group.objects.all()
    serializer_class = serializers.GroupSerializer
