import constance
from rest_framework.exceptions import PermissionDenied

from core import models

config = constance.config


class NoAssignmentForTutor(Exception):
    pass


def tutor_attempts_to_patch_first_feedback_final(feedback_serializer,
                                                 user,
                                                 assignment=None):
    # override assignment logic in exercise mode
    if user.role == models.UserAccount.REVIEWER \
            or user.role == models.UserAccount.TUTOR and config.EXERCISE_MODE:
        return False
    if user.role == models.UserAccount.TUTOR and assignment is None:
        raise NoAssignmentForTutor()
    is_final_set = feedback_serializer.validated_data.get('is_final', False)
    in_creation = assignment.stage == models.TutorSubmissionAssignment.FEEDBACK_CREATION  # noqa
    single_correction = config.SINGLE_CORRECTION
    return is_final_set and in_creation and not single_correction


def get_implicit_assignment_for_user(submission, user):
    """ Check for tutor if it exists. Not relevant for reviewer """
    try:
        return models.TutorSubmissionAssignment.objects.get(
            owner=user,
            submission=submission
        )
    except models.TutorSubmissionAssignment.DoesNotExist:
        if user.role == models.UserAccount.REVIEWER \
                or (user.role == models.UserAccount.TUTOR and config.EXERCISE_MODE):
            return None

        raise PermissionDenied(
            detail='This user has no permission to create this feedback')
