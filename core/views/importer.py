from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.exceptions import ValidationError
from core.permissions import IsReviewer
from util.importer import parse_and_import_hektor_json


class ImportApiViewSet(APIView):
    permission_classes = (IsReviewer, )

    def post(self, request):
        exam_data = request.data

        if not exam_data:
            return Response({"Error": "You need to submit the exam data to be imported"},
                            status.HTTP_400_BAD_REQUEST)

        try:
            parse_and_import_hektor_json(exam_data)
        except ValidationError as err:
            return Response({"ValidationError": err.detail},
                            status.HTTP_409_CONFLICT)

        return Response({}, status.HTTP_201_CREATED)
