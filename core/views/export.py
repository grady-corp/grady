from django.db import transaction

from rest_framework.response import Response
from rest_framework.views import APIView

import xkcdpass.xkcd_password as xp

from core.models import StudentInfo, UserAccount, ExamType, SubmissionType
from core.permissions import IsReviewer
from core.serializers import SubmissionTypeSerializer, \
    ExamSerializer, UserAccountSerializer
from core.serializers.student import StudentExportSerializer
from core.serializers.tutor import CorrectorSerializer

words = xp.generate_wordlist(wordfile=xp.locate_wordfile(), min_length=5, max_length=8)


@transaction.atomic
def _set_student_passwords():
    student_password_dict = {}
    # TODO use bulk update with django makepassword function
    for student in UserAccount.get_students():
        password = xp.generate_xkcdpassword(words, numwords=3, delimiter='-')
        student.set_password(password)
        student.save()
        student_password_dict[student.pk] = password

    return student_password_dict


class StudentJSONExport(APIView):
    permission_classes = (IsReviewer, )

    def post(self, request, format=None):
        set_passwords = request.data.get('set_passwords')
        passwords = _set_student_passwords() if set_passwords else None

        content = [
            {'Matrikel': student.matrikel_no,
             'Name': student.user.fullname,
             'Username': student.user.username,
             'Email': student.user.email,
             'Sum': student.overall_score,
             'Exam': student.exam.module_reference,
             'Password': passwords[student.user.pk] if set_passwords else '********',
             'Scores': [
                 {
                     'type': submission_type,
                     'score': score
                 } for submission_type, score in student.score_per_submission().items()]
             } for student
            in StudentInfo.get_annotated_score_submission_list()]
        return Response(content)


class InstanceExport(APIView):
    permission_classes = (IsReviewer, )

    def get(self, request):
        exam_types_serializer = ExamSerializer(ExamType.objects.all(), many=True)
        submission_types_serializer = SubmissionTypeSerializer(
            SubmissionType.objects.all(), many=True)
        tutors_serializer = CorrectorSerializer(
            UserAccount.corrector.with_feedback_count(),
            many=True)
        reviewer_serializer = UserAccountSerializer(UserAccount.get_reviewers(), many=True)
        student_serializer = StudentExportSerializer(StudentInfo.objects.all(), many=True)

        content = {
            "examTypes": exam_types_serializer.data,
            "submissionTypes": submission_types_serializer.data,
            "students": student_serializer.data,
            "tutors": tutors_serializer.data,
            "reviewers": reviewer_serializer.data
        }
        return Response(content)
