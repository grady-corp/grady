import logging
from multiprocessing import Lock

import constance
from rest_framework import mixins, status, viewsets
from rest_framework.exceptions import PermissionDenied
from rest_framework.response import Response

from core import models, permissions, serializers
from core.views.util import tutor_attempts_to_patch_first_feedback_final, \
    get_implicit_assignment_for_user

log = logging.getLogger(__name__)
config = constance.config


class FeedbackApiView(
        mixins.CreateModelMixin,
        mixins.RetrieveModelMixin,
        mixins.ListModelMixin,
        viewsets.GenericViewSet):
    """ Gets a list of an individual exam by Id if provided """
    permission_classes = (permissions.IsTutorOrReviewer,)
    lookup_field = 'of_submission__pk'
    lookup_url_kwarg = 'submission_pk'

    def _tutor_attempts_to_change_final_feedback_of_reviewer(self, serializer):
        feedback_final_by_reviewer = serializer.instance.final_by_reviewer
        user_is_tutor = self.request.user.role == models.UserAccount.TUTOR
        return feedback_final_by_reviewer and user_is_tutor

    def _tutor_attempts_to_set_first_feedback_final(self, serializer):
        is_final_set = serializer.validated_data.get('is_final', False)
        user_is_tutor = self.request.user.role == models.UserAccount.TUTOR
        return is_final_set and user_is_tutor

    def get_serializer_class(self):
        if config.EXERCISE_MODE:
            return serializers.FeedbackWithStudentSerializer
        return serializers.FeedbackSerializer

    def get_queryset(self):
        base_queryset = models.Feedback.objects \
            .select_related('of_submission') \
            .select_related('of_submission__type') \
            .select_related('of_submission__student') \
            .select_related('of_submission__student__user') \
            .all()

        if self.request.user.is_reviewer():
            return base_queryset \
                .prefetch_related('feedback_lines') \
                .prefetch_related('feedback_lines__of_tutor') \
                .all()

        user_groups = self.request.user.exercise_groups.all()
        if self.request.user.is_tutor() and config.EXERCISE_MODE:
            return base_queryset.filter(
                of_submission__student__user__exercise_groups__in=user_groups
            )

        return base_queryset.filter(
            of_submission__assignments__owner=self.request.user
        )

    def create(self, request, *args, **kwargs):
        if request.user.is_tutor() and not config.EXERCISE_MODE:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        # update MetaSubmission information
        meta = serializer.validated_data.get('of_submission').meta
        meta.feedback_authors.add(self.request.user)
        return Response(serializer.data,
                        status=status.HTTP_201_CREATED)

    def partial_update(self, request, **kwargs):
        feedback = self.get_object()
        serializer = self.get_serializer(feedback, data=request.data,
                                         partial=True)
        serializer.is_valid(raise_exception=True)

        assignment = get_implicit_assignment_for_user(feedback.of_submission, self.request.user)

        if self._tutor_attempts_to_change_final_feedback_of_reviewer(serializer):  # noqa
            raise PermissionDenied(
                detail="Changing feedback set to final by a reviewer is not allowed.")

        if tutor_attempts_to_patch_first_feedback_final(serializer, self.request.user, assignment):
            raise PermissionDenied(
                detail='Cannot set the first feedback final.')
        serializer.save()
        return Response(serializer.data)


class FeedbackCommentApiView(
        mixins.DestroyModelMixin,
        viewsets.GenericViewSet):
    """ Gets a list of an individual exam by Id if provided """
    permission_classes = (permissions.IsTutorOrReviewer,)
    serializer_class = serializers.FeedbackCommentSerializer

    def get_queryset(self):
        base_queryset = models.FeedbackComment.objects.all()

        user = self.request.user
        if user.role == models.UserAccount.REVIEWER:
            return base_queryset
        return base_queryset.filter(of_tutor=user)

    def destroy(self, request, *args, **kwargs):
        with Lock():
            instance = self.get_object()
            if instance.of_feedback.feedback_lines.count() == 1 and \
                    not instance.of_feedback.is_full_score():
                raise PermissionDenied(detail="Last comment can not be deleted for submissions "
                                              "with non full score")

            self.perform_destroy(instance)
            return Response(status=status.HTTP_204_NO_CONTENT)
