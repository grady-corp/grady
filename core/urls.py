from django.urls import path
from rest_framework.routers import DefaultRouter

from core import views

# Create a router and register our viewsets with it.

router = DefaultRouter()
router.register("student", views.StudentReviewerApiViewSet, basename="student")
router.register("examtype", views.ExamApiViewSet, basename="examtype")
router.register("feedback", views.FeedbackApiView, basename="feedback")
router.register(
    "feedback-comment", views.FeedbackCommentApiView, basename="feedback-comment"
)
router.register("submission", views.SubmissionViewSet, basename="submission")
router.register(
    "submissiontype", views.SubmissionTypeApiView, basename="submissiontype"
)
router.register("corrector", views.CorrectorApiViewSet, basename="corrector")
router.register("assignment", views.AssignmentApiViewSet, basename="assignment")
router.register("statistics", views.StatisticsEndpoint, basename="statistics")
router.register("user", views.UserAccountViewSet, basename="user")
router.register("label", views.LabelApiViewSet, basename="label")
router.register("label-statistics", views.LabelStatistics, basename="label-statistics")
router.register(
    "solution-comment", views.SolutionCommentApiViewSet, basename="solution-comment"
)
router.register("group", views.GroupApiViewSet, basename="group")
router.register("config", views.InstanceConfigurationViewSet, basename="config")

# regular views that are not viewsets
regular_views_urlpatterns = [
    path("student-page/", views.StudentSelfApiView.as_view(), name="student-page"),
    path(
        "student-submissions/",
        views.StudentSelfSubmissionsApiView.as_view(),
        name="student-submissions",
    ),
    path("instance/export/", views.InstanceExport.as_view(), name="instance-export"),
    path("export/json/", views.StudentJSONExport.as_view(), name="export-json"),
    path("import/", views.ImportApiViewSet.as_view(), name="import-json"),
]

urlpatterns = [
    *router.urls,
    *regular_views_urlpatterns,
]
