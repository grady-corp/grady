import logging
import uuid

import constance
from django.contrib.auth import get_user_model
from django.db import models

from core.models.submission import Submission

log = logging.getLogger(__name__)
config = constance.config


class Feedback(models.Model):
    """
    Attributes
    ----------
    score : PositiveIntegerField
        A score that has been assigned to he submission. Is final if it was
        accepted.
    created : DateTimeField
        When the feedback was initially created
    modified: DateTimeField
        Timestamp indicating the last time the feedback was saved
    of_submission : OneToOneField
        The submission this feedback belongs to. It finally determines how many
        points a student receives for his submission.
    origin : IntegerField
        Of whom was this feedback originally created. She below for the choices
    final_by_reviewer: BooleanField
        Whether or not this feedback was set to final by a reviewer once
    """
    score = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    is_final = models.BooleanField(default=False)
    final_by_reviewer = models.BooleanField(default=False)

    of_submission = models.OneToOneField(
        Submission,
        on_delete=models.CASCADE,
        related_name='feedback')

    # the denominators that are allowed for the decimal score interpreted as a fraction
    ALLOWED_DENOMINATORS = [1, 2]

    class Meta:
        verbose_name = "Feedback"
        verbose_name_plural = "Feedback Set"

    def __str__(self) -> str:
        return 'Feedback for {}'.format(self.of_submission)

    def is_full_score(self) -> bool:
        return self.of_submission.type.full_score == self.score

    def get_full_score(self) -> int:
        return self.of_submission.type.full_score


class FeedbackComment(models.Model):
    """ This Class contains the Feedback for a specific line of a Submission"""
    comment_id = models.UUIDField(primary_key=True,
                                  default=uuid.uuid4,
                                  editable=False)
    text = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    visible_to_student = models.BooleanField(default=True)

    of_line = models.PositiveIntegerField(default=0)
    of_tutor = models.ForeignKey(
        get_user_model(),
        related_name="comment_list",
        on_delete=models.PROTECT
    )
    of_feedback = models.ForeignKey(
        Feedback,
        related_name="feedback_lines",
        on_delete=models.CASCADE,
        null=True
    )

    class Meta:
        verbose_name = "Feedback Comment"
        verbose_name_plural = "Feedback Comments"
        ordering = ('created',)
        unique_together = ('of_line', 'of_tutor', 'of_feedback')

    def __str__(self):
        return 'Comment on line {} of tutor {}: "{}"'.format(self.of_line,
                                                             self.of_tutor,
                                                             self.text)
