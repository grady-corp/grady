import logging
import uuid

import constance
from django.db import models

from core.models import Submission, UserAccount, MetaSubmission

log = logging.getLogger(__name__)
config = constance.config


class DeletionOfDoneAssignmentsNotPermitted(Exception):
    pass


class CanOnlyCallFinishOnUnfinishedAssignments(Exception):
    pass


class SubmissionTypeDepleted(Exception):
    pass


class NotMoreThanTwoOpenAssignmentsAllowed(Exception):
    pass


class TutorSubmissionAssignmentManager(models.Manager):

    @staticmethod
    def available_assignments(create_assignment_options):
        stage = create_assignment_options['stage']
        owner = create_assignment_options['owner']
        submission_type = create_assignment_options['submission_type']
        group = create_assignment_options.get('group')

        stage = TutorSubmissionAssignment.assignment_count_on_stage[stage]
        candidates = MetaSubmission.objects.filter(
            submission__type__pk=submission_type,
            done_assignments=stage,
            has_final_feedback=False,
            has_active_assignment=False,
        ).exclude(
            feedback_authors=owner
        )
        if group is not None:
            candidates = candidates.filter(
                submission__student__user__exercise_groups__pk=group
            )
        return candidates


class TutorSubmissionAssignment(models.Model):
    objects = TutorSubmissionAssignmentManager()

    FEEDBACK_CREATION = 'feedback-creation'
    FEEDBACK_VALIDATION = 'feedback-validation'
    FEEDBACK_REVIEW = 'feedback-review'

    stages = (
        (FEEDBACK_CREATION, 'No feedback was ever assigned'),
        (FEEDBACK_VALIDATION, 'Feedback exists but is not validated'),
        (FEEDBACK_REVIEW, 'Review by exam reviewer required'),
    )

    assignment_count_on_stage = {
        FEEDBACK_CREATION: 0,
        FEEDBACK_VALIDATION: 1,
        FEEDBACK_REVIEW: 2,
    }

    owner = models.ForeignKey(UserAccount,
                              on_delete=models.CASCADE,
                              related_name='assignments')

    assignment_id = models.UUIDField(primary_key=True,
                                     default=uuid.uuid4,
                                     editable=False)
    submission = models.ForeignKey(Submission,
                                   on_delete=models.CASCADE,
                                   related_name='assignments')

    stage = models.CharField(choices=stages,
                             max_length=60,
                             default=FEEDBACK_CREATION)

    is_done = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return (f'{self.owner} assigned to {self.submission}'
                f' (done={self.is_done})')

    def finish(self):
        self.refresh_from_db()
        if self.is_done:
            raise CanOnlyCallFinishOnUnfinishedAssignments()

        meta = self.submission.meta
        meta.feedback_authors.add(self.owner)
        meta.done_assignments += 1
        meta.has_active_assignment = False
        self.is_done = True
        self.save()
        meta.save()

    def delete(self, *args, **kwargs):
        if self.is_done:
            raise DeletionOfDoneAssignmentsNotPermitted()
        super().delete(*args, **kwargs)
