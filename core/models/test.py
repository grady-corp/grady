import logging
import uuid

import constance
from django.db import models

log = logging.getLogger(__name__)
config = constance.config


class Test(models.Model):
    """Tests contain information that has been unapproved by automated tests,
    and directly belongs to a submission. Often certain Feedback was already
    given by information provided by these tests.

    Attributes
    ----------
    annotation : TextField
        All the output of the test (e.g. compiler output)
    label : CharField
        Indicates SUCCES or FAILURE
    name : CharField
        The name of the test that was performed
    submission : ForeignKey
        The submission the tests where unapproved on
    """
    test_id = models.UUIDField(primary_key=True,
                               default=uuid.uuid4,
                               editable=False)

    name = models.CharField(max_length=30)
    label = models.CharField(max_length=50)
    annotation = models.TextField()
    submission = models.ForeignKey('submission',
                                   related_name='tests',
                                   on_delete=models.CASCADE,)

    class Meta:
        verbose_name = "Test"
        verbose_name_plural = "Tests"
        unique_together = (('submission', 'name'),)

    def __str__(self) -> str:
        return f'{self.name} {self.label}'
