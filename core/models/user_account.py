import logging
import uuid

import constance
from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models
from django.db.models import Case, Count, IntegerField, Q, Value, When
from django.apps import apps

from core.models import Group

log = logging.getLogger(__name__)
config = constance.config


class TutorReviewerManager(UserManager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .filter(Q(role=UserAccount.TUTOR) | Q(role=UserAccount.REVIEWER))
        )

    def with_feedback_count(self):
        def _get_counter(stage):
            return Count(
                Case(
                    When(
                        Q(assignments__stage=stage) & Q(assignments__is_done=True),
                        then=Value(1),
                    )
                ),
                output_field=IntegerField(),
            )

        assignment_model = apps.get_model("core", "TutorSubmissionAssignment")  # noqa

        return (
            self.get_queryset()
            .annotate(feedback_created=_get_counter(assignment_model.FEEDBACK_CREATION))
            .annotate(
                feedback_validated=_get_counter(assignment_model.FEEDBACK_VALIDATION)
            )
        )


def group_default():
    return [Group.objects.get_or_create(name="Default Group")[0].pk]


class UserAccount(AbstractUser):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    Username and password are required. Other fields are optional.
    """

    STUDENT = "Student"
    TUTOR = "Tutor"
    REVIEWER = "Reviewer"

    ROLE_CHOICES = ((STUDENT, "student"), (TUTOR, "tutor"), (REVIEWER, "reviewer"))

    # Fields
    role = models.CharField(max_length=50, choices=ROLE_CHOICES)
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    exercise_groups = models.ManyToManyField(
        Group, blank=True, related_name="users", default=group_default
    )

    fullname = models.CharField("full name", max_length=70, blank=True)
    is_admin = models.BooleanField(default=False)

    # Managers
    objects = UserManager()
    corrector = TutorReviewerManager()

    # Helper methods
    def is_student(self):
        return self.role == "Student"

    def is_tutor(self):
        return self.role == "Tutor"

    def is_reviewer(self):
        return self.role == "Reviewer"

    def set_groups(self, groups):
        if groups == [] or groups is None:
            self.exercise_groups.set(group_default())
        else:
            self.exercise_groups.set(groups)

    # All of these methods are deprecated and should be replaced by custom
    # Managers (see tutor manager)
    @classmethod
    def get_students(cls):
        return cls.objects.filter(role=cls.STUDENT)

    @classmethod
    def get_tutors(cls):
        return cls.objects.filter(role=cls.TUTOR)

    @classmethod
    def get_reviewers(cls):
        return cls.objects.filter(role=cls.REVIEWER)
