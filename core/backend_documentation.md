
# check available Docker Containers:
> docker container list --all

# Enter the postgres DB on the Docker Container:
> docker exec -it c811b28f8a15 psql -U postgres

### in the container
# list all Tables
> \dt
# check out a Table
> SELECT * FROM {{TableName}};

# kill every core_ Table
DO $$ 
DECLARE 
  t_name text;
BEGIN
  FOR t_name IN 
    SELECT table_name 
    FROM information_schema.tables 
    WHERE table_name LIKE 'core_%' 
  LOOP
    EXECUTE 'DROP TABLE IF EXISTS ' || t_name || ' CASCADE';
  END LOOP;
END $$;