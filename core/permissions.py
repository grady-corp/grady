import logging

import constance
from django.http import HttpRequest
from django.views import View
from rest_framework import permissions

log = logging.getLogger(__name__)
config = constance.config


class IsUserRoleGenericPermission(permissions.BasePermission):
    """ Generic class that encapsulates how to identify someone
    as a member of a user Group """

    def has_permission(self, request: HttpRequest, view: View) -> bool:
        """ required by BasePermission. Check if user is instance of any
        of the models provided in class' models attribute """
        assert self.roles is not None, (
            "'%s' has to include a `roles` attribute"
            % self.__class__.__name__
        )

        user = request.user
        is_authorized = user.is_superuser or (user.is_authenticated and
                                              user.role in self.roles)

        return is_authorized


class IsStudent(IsUserRoleGenericPermission):
    """ Has student permissions """
    roles = ('Student', )


class IsReviewer(IsUserRoleGenericPermission):
    """ Has reviewer permissions """
    roles = ('Reviewer', )


class IsTutor(IsUserRoleGenericPermission):
    """ Has tutor permissions """
    roles = ('Tutor', )


class IsTutorOrReviewer(IsUserRoleGenericPermission):
    """ Has tutor or reviewer permissions """
    roles = ('Tutor', 'Reviewer')


class GenericIsConfigEnabled(permissions.BasePermission):
    """
    Generic class that encapsulates how to check if a runtime config is set.
    """
    def has_permission(self, request, view) -> bool:
        assert self.required_configs is not None, "Need to include at least one config attribute"

        return all(getattr(config, cfg) for cfg in self.required_configs)


class SolutionsEnabledToStudents(GenericIsConfigEnabled):
    required_configs = ["SHOW_SOLUTION_TO_STUDENTS"]
