import csv
import re
import secrets
import sys

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = ('All student passwords will be changed'
            'and a list of these password will be printed')

    def add_arguments(self, parser):
        parser.add_argument(
            'instance',
            help="Name of the instance that generated the passwords"
        )

    def _handle(self, *args, output=sys.stdout, instance="", **kwargs):
        with open('/usr/share/dict/words') as f:
            # strip punctuation
            words = set(re.sub(r"[^a-z]", "\n", f.read().lower()).split('\n'))
            choose_from = list({word.strip().lower()
                                for word in words if 5 < len(word) < 8})

        writer = csv.writer(output)
        writer.writerow(
            ['Name', 'Matrikel', 'Username', 'password', 'instance'])

        for student in get_user_model().get_students():
            password = '-'.join(secrets.choice(choose_from) for _ in range(3))

            student.set_password(password)
            student.save()

            if not student.fullname:
                student.fullname = '__no_name__'

            writer.writerow([student.fullname, student.student.matrikel_no,
                             student.username, password, instance])

    def handle(self, *args, **options):
        self._handle(*args, **options)
