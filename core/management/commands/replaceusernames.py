import argparse
import json
import sys

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = ('replaces all usernames based on a '
            'matrikel_no -> new_name dict (input should be JSON)')

    def add_arguments(self, parser):
        parser.add_argument(
            'matno2username_dict',
            help='the mapping as a JSON file',
            default=sys.stdin,
            type=argparse.FileType('r')
        )

    def _handle(self, matno2username_dict, **kwargs):
        matno2username = json.JSONDecoder().decode(matno2username_dict.read())
        for student in get_user_model().get_students():
            if student.student.matrikel_no in matno2username:
                new_name = matno2username[student.student.matrikel_no]
                student.username = new_name
                student.save()

    def handle(self, *args, **options):
        self._handle(*args, **options)
