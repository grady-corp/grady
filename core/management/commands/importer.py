from django.core.management.base import BaseCommand

import util.importer


class Command(BaseCommand):
    help = 'Start the Grady command line importer'

    def handle(self, *args, **kwargs):
        util.importer.start()
