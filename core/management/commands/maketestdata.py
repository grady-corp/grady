from django.core.management.base import BaseCommand

from util.factories import init_test_instance


class Command(BaseCommand):
    help = 'Creates some initial test data for the application'

    def handle(self, *args, **options):
        init_test_instance()
