from django.core.management.base import BaseCommand

from core import models


class Command(BaseCommand):
    help = 'Extract all submissions from this instance'

    def handle(self, *args, **kwargs):
        for submission in models.Submission.objects.filter(
                feedback__isnull=False).order_by('type'):
            print(submission.feedback.score, repr(submission.text),
                  file=open(str(submission.type).replace(' ', '_'), 'a'))
