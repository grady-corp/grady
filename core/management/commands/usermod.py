from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'All user accounts will be disabled'

    def add_arguments(self, parser):
        parser.add_argument(
            'switch',
            choices=('enable', 'disable'),
            default='enable',
            help='enable all users (enable) or disable all (disable)'
        )
        filter_group = parser.add_mutually_exclusive_group()
        filter_group.add_argument(
            '--exclude',
            default=(),
            nargs='+',
            help='Provide all users you want to exclude from the operation'
        )
        filter_group.add_argument(
            '--include',
            help=('Provide users you want to operate on'
                  'Everything else is untouched'),
            nargs='+',
            default=())

    def handle(self, switch, exclude=None, include=None, *args, **kwargs):
        if include:
            for user in get_user_model().objects.filter(username__in=include):
                user.is_active = switch == 'enable'
                user.save()
        else:  # this includes nothing set
            for user in get_user_model().objects.exclude(username__in=exclude):
                user.is_active = switch == 'enable'
                user.save()
