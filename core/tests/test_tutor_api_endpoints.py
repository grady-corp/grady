""" Two api endpoints are currently planned

    * GET /tutor/:id to retrive information about some tutor
    * POST /tutor/:username/:email create a new tutor and email password
    * GET /tutorlist list of all tutors with their scores
"""
from django.contrib.auth import get_user_model
from constance.test import override_config
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import (
    APIClient,
    APIRequestFactory,
    APITestCase,
    force_authenticate,
)

from core.models import Feedback, TutorSubmissionAssignment
from core.views import CorrectorApiViewSet
from util.factories import GradyUserFactory, make_test_data

NUMBER_OF_TUTORS = 3


class TutorDeleteTest(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.factory = APIRequestFactory()
        cls.user_factory = GradyUserFactory()

    def setUp(self):
        self.tutor = self.user_factory.make_tutor(username="UFO")
        self.reviewer = self.user_factory.make_reviewer()
        self.request = self.factory.delete(
            reverse("corrector-detail", args=[str(self.tutor.pk)])
        )
        self.view = CorrectorApiViewSet.as_view({"delete": "destroy"})

        force_authenticate(self.request, user=self.reviewer)
        self.response = self.view(self.request, pk=str(self.tutor.pk))

    def test_can_delete_tutor_soapbox(self):
        """see if the tutor was deleted"""
        self.assertEqual(0, get_user_model().get_tutors().count())

    def test_user_is_deleted_too(self):
        """see if the associated user was deleted (reviewer remains)"""
        self.assertNotIn(self.tutor, get_user_model().objects.all())


class TutorListTests(APITestCase):
    @classmethod
    def setUpTestData(cls):
        data = make_test_data(
            data_dict={
                "exams": [
                    {
                        "module_reference": "Test Exam 01",
                        "total_score": 100,
                        "pass_score": 60,
                    }
                ],
                "submission_types": [
                    {
                        "name": "01. Sort this or that",
                        "full_score": 35,
                        "description": "Very complicated",
                        "solution": "Trivial!",
                    }
                ],
                "students": [
                    {"username": "student01", "exam": "Test Exam 01"},
                    {"username": "student02", "exam": "Test Exam 01"},
                ],
                "tutors": [{"username": "tutor01"}, {"username": "tutor02"}],
                "reviewers": [{"username": "reviewer"}],
                "submissions": [
                    {
                        "text": "function blabl\n" "   on multi lines\n",
                        "type": "01. Sort this or that",
                        "user": "student01",
                    },
                    {
                        "text": "function blabl\n" "   on multi lines\n",
                        "type": "01. Sort this or that",
                        "user": "student02",
                    },
                ],
            }
        )

        def feedback_cycle(tutor, stage):
            submissions = TutorSubmissionAssignment.objects.available_assignments(
                {
                    "owner": tutor,
                    "stage": stage,
                    "submission_type": data["submission_types"][0].pk,
                }
            )
            assignment = TutorSubmissionAssignment.objects.create(
                owner=tutor, stage=stage, submission=submissions.first().submission
            )
            Feedback.objects.update_or_create(
                of_submission=assignment.submission, score=35
            )
            assignment.finish()

        tutor01 = data["tutors"][0]
        tutor02 = data["tutors"][1]
        cls.reviewer = data["reviewers"][0]

        feedback_cycle(tutor01, TutorSubmissionAssignment.FEEDBACK_CREATION)
        feedback_cycle(tutor02, TutorSubmissionAssignment.FEEDBACK_VALIDATION)

    def setUp(self):
        factory = APIRequestFactory()
        request = factory.get(reverse("corrector-list"))
        view = CorrectorApiViewSet.as_view({"get": "list"})
        force_authenticate(request, user=self.reviewer)
        self.response = view(request)

    def test_can_access(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_get_a_list_of_all_correctos(self):
        self.assertEqual(3, len(self.response.data))

    def test_feedback_created_count_matches_database(self):
        def verify_fields(tutor_obj):
            t = get_user_model().objects.get(username=tutor_obj["username"])
            feedback_created_count = TutorSubmissionAssignment.objects.filter(
                is_done=True,
                stage=TutorSubmissionAssignment.FEEDBACK_CREATION,  # noqa
                owner=t,
            ).count()
            return feedback_created_count == tutor_obj["feedback_created"]

        self.assertTrue(all(map(verify_fields, self.response.data)))

    def test_feedback_validated_count_matches_database(self):
        def verify_fields(tutor_obj):
            t = get_user_model().objects.get(username=tutor_obj["username"])
            feedback_validated_cnt = TutorSubmissionAssignment.objects.filter(
                is_done=True,
                stage=TutorSubmissionAssignment.FEEDBACK_VALIDATION,  # noqa
                owner=t,
            ).count()
            return feedback_validated_cnt == tutor_obj["feedback_validated"]

        self.assertTrue(all(map(verify_fields, self.response.data)))

    def test_sum_of_done_assignments(self):
        self.assertEqual(
            sum(
                obj["feedback_created"] + obj["feedback_validated"]
                for obj in self.response.data
            ),
            TutorSubmissionAssignment.objects.filter(is_done=True).count(),
        )


class TutorCreateTests(APITestCase):
    USERNAME = "some_weird_name"

    @classmethod
    def setUpTestData(cls):
        cls.factory = APIRequestFactory()
        cls.user_factory = GradyUserFactory()

    def setUp(self):
        self.reviewer = self.user_factory.make_reviewer()
        self.request = self.factory.post(
            reverse("corrector-list"), {"username": self.USERNAME}
        )
        self.view = CorrectorApiViewSet.as_view({"post": "create"})

        force_authenticate(self.request, user=self.reviewer)
        self.response = self.view(self.request, username=self.USERNAME)

    def test_can_access(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_can_create(self):
        self.assertEqual(self.USERNAME, get_user_model().get_tutors().first().username)


class TutorDetailViewTests(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user_factory = GradyUserFactory()

    def setUp(self):
        self.tutor = self.user_factory.make_tutor(username="fetterotto")
        self.reviewer = self.user_factory.make_reviewer()
        self.client = APIClient()
        self.client.force_authenticate(user=self.reviewer)

        url = reverse("corrector-detail", kwargs={"pk": str(self.tutor.pk)})
        self.response = self.client.get(url, format="json")

    def test_can_access(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_can_view_tutor(self):
        self.assertEqual(self.response.data["username"], self.tutor.username)


class TutorRegisterTests(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user_factory = GradyUserFactory()

    def setUp(self):
        self.reviewer = self.user_factory.make_reviewer()
        self.client = APIClient()

    @override_config(REGISTRATION_PASSWORD="pw")
    def test_reviewer_can_activate_tutor(self):
        response = self.client.post(
            "/api/corrector/register/",
            {
                "username": "hans",
                "password": "safeandsound",
                "registration_password": "pw",
            },
        )

        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

        self.client.force_authenticate(self.reviewer)
        response = self.client.put(
            "/api/corrector/%s/" % response.data["pk"],
            {"username": "hans", "is_active": True},
        )

        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_trottle_is_not_active_while_testing(self):
        r = self.client.post("/api/corrector/register/", {"username": "hans"})
        r = self.client.post("/api/corrector/register/", {"username": "the"})
        r = self.client.post("/api/corrector/register/", {"username": "brave"})
        r = self.client.post("/api/corrector/register/", {"username": "fears"})
        r = self.client.post("/api/corrector/register/", {"username": "spiders"})

        self.assertNotEqual(status.HTTP_429_TOO_MANY_REQUESTS, r.status_code)
