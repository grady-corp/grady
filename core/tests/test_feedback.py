import unittest

from constance.test import override_config
from rest_framework import status
from rest_framework.test import APIRequestFactory, APITestCase

from core.models import (
    Feedback,
    FeedbackComment,
    Submission,
    SubmissionType,
    FeedbackLabel,
    TutorSubmissionAssignment,
    MetaSubmission,
)
from util.factories import GradyUserFactory, make_test_data, make_exams


class FeedbackRetrieveTestCase(APITestCase):
    factory = GradyUserFactory()
    EXPECTED_SCORE = 23

    @classmethod
    def setUpTestData(cls):
        cls.score = 23
        cls.tutor = cls.factory.make_tutor()
        cls.exam = make_exams(
            exams=[
                {
                    "module_reference": "Test Exam 01",
                    "total_score": 100,
                    "pass_score": 60,
                }
            ]
        )[0]
        cls.student = cls.factory.make_student(exam=cls.exam)
        cls.reviewer = cls.factory.make_reviewer()
        cls.tutors = [cls.tutor, cls.reviewer]
        cls.request_factory = APIRequestFactory()
        cls.submission_type = SubmissionType.objects.create(
            name="Cooking some crystal with Jesse"
        )
        cls.sub = Submission.objects.create(
            student=cls.student.student, type=cls.submission_type
        )
        cls.feedback = Feedback.objects.create(
            score=23, is_final=False, of_submission=cls.sub
        )

        for line in range(1, 3):
            for tutor in cls.tutors:
                FeedbackComment.objects.create(
                    text="fortytwo",
                    of_feedback=cls.feedback,
                    of_tutor=tutor,
                    of_line=line,
                )

    def setUp(self):
        self.client.force_authenticate(user=self.reviewer)
        self.response = self.client.get(f"/api/feedback/{self.sub.pk}/")
        self.data = self.response.data

    def test_only_one_final_comment_per_line(self):
        comments_on_first_line = FeedbackComment.objects.filter(of_line=1)
        self.assertEqual(2, comments_on_first_line.count())
        final_comments = [
            comment
            for comment in comments_on_first_line.all()
            if comment.visible_to_student
        ]
        self.assertEqual(1, len(final_comments))

    def test_can_retrieve_feedback_via_endpoint(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_if_feedback_contains_correct_score(self):
        self.assertIn("score", self.data)
        self.assertEqual(self.data.get("score"), self.EXPECTED_SCORE)

    def test_if_feedback_contains_linekeys(self):
        self.assertIn("feedback_lines", self.data)
        self.assertIn(1, self.data["feedback_lines"])
        self.assertIn(2, self.data["feedback_lines"])

    def test_if_feedback_contains_final(self):
        self.assertIn("is_final", self.data)
        self.assertIsNotNone(self.data["is_final"])

    def test_if_comment_contains_text(self):
        self.assertIn("text", self.data["feedback_lines"][1][0])
        self.assertEqual("fortytwo", self.data["feedback_lines"][1][0]["text"])

    def test_if_comment_contains_created(self):
        self.assertIn("created", self.data["feedback_lines"][1][0])
        self.assertIsNotNone(self.data["feedback_lines"][1][0]["created"])

    def test_if_comment_has_tutor(self):
        self.assertIn("of_tutor", self.data["feedback_lines"][1][0])
        self.assertEqual(
            self.tutor.username, self.data["feedback_lines"][1][0]["of_tutor"]
        )

    def test_if_comment_has_final(self):
        self.assertIn("visible_to_student", self.data["feedback_lines"][1][0])
        self.assertIsNotNone(self.data["feedback_lines"][1][0]["visible_to_student"])


class FeedbackCreateTestCase(APITestCase):
    def url(self):
        return f"/api/assignment/{self.assignment.pk}/finish/"

    @classmethod
    def setUpTestData(cls):
        cls.user_factory = GradyUserFactory()
        cls.tutor = cls.user_factory.make_tutor(password="p")
        cls.reviewer = cls.user_factory.make_reviewer(password="p")
        cls.exam = make_exams(
            exams=[
                {
                    "module_reference": "Test Exam 01",
                    "total_score": 100,
                    "pass_score": 60,
                }
            ]
        )[0]
        cls.student = cls.user_factory.make_student(exam=cls.exam)
        cls.submission_type = SubmissionType.objects.create(
            name="Cooking some crystal with Jesse", full_score=100
        )
        text = """ First line of defense
        We do not have a second line
        security via obscurity
        is very bad. """
        cls.sub = Submission.objects.create(
            student=cls.student.student, type=cls.submission_type, text=text
        )
        cls.fst_label = FeedbackLabel.objects.create(name="Label1", description="Bla")
        cls.snd_label = FeedbackLabel.objects.create(name="Label2", description="Bla")

    def setUp(self):
        self.sub.refresh_from_db()
        self.fst_label.refresh_from_db()
        self.snd_label.refresh_from_db()
        self.client.force_authenticate(user=self.tutor)
        self.assignment = TutorSubmissionAssignment.objects.create(
            submission=Submission.objects.first(),
            owner=self.tutor,
        )

    def test_cannot_create_feedback_without_feedback_lines(self):
        # TODO this test has to be adapted to test the various constraints
        # e.g. feedback without lines can only be given if the score is equal
        # to the max Score for this submission
        data = {
            "score": 10,
            "is_final": False,
            "of_submission": self.assignment.submission.pk,
        }
        self.assertEqual(Feedback.objects.count(), 0)
        response = self.client.post(self.url(), data, format="json")
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
        self.assertEqual(Feedback.objects.count(), 0)

    def test_cannot_create_feedback_with_score_higher_than_max(self):
        data = {
            "score": 101,
            "is_final": False,
            "of_submission": self.assignment.submission.pk,
        }
        self.assertEqual(Feedback.objects.count(), 0)
        response = self.client.post(self.url(), data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Feedback.objects.count(), 0)

    def test_tutor_cannot_set_feedback_final_on_creation(self):
        data = {
            "score": 100,
            "is_final": True,
            "of_submission": self.assignment.submission.pk,
        }
        response = self.client.post(self.url(), data, format="json")
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)
        self.assertEqual(Feedback.objects.count(), 0)

    @override_config(SINGLE_CORRECTION=True)
    def test_tutor_can_set_feedback_final_on_creation_with_single_correction_enabled(
        self,
    ):
        data = {
            "score": 100,
            "is_final": True,
            "of_submission": self.assignment.submission.pk,
        }
        response = self.client.post(self.url(), data, format="json")
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assertEqual(Feedback.objects.count(), 1)

    def test_tutor_has_to_write_a_line_if_score_is_not_100_percent(self):
        data = {
            "score": 50,
            "is_final": False,
            "of_submission": self.assignment.submission.pk,
        }
        response = self.client.post(self.url(), data, format="json")
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
        self.assertEqual(Feedback.objects.count(), 0)

    def test_cannot_create_feedback_with_score_less_than_zero(self):
        data = {
            "score": -1,
            "is_final": False,
            "of_submission": self.assignment.submission.pk,
        }
        self.assertEqual(Feedback.objects.count(), 0)
        response = self.client.post(self.url(), data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Feedback.objects.count(), 0)

    def test_cannot_create_feedback_with_score_with_invalid_fractional_denominator(
        self,
    ):
        data = {
            "score": 1.500000001,
            "is_final": False,
            "of_submission": self.assignment.submission.pk,
        }
        self.assertEqual(Feedback.objects.count(), 0)
        response = self.client.post(self.url(), data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Feedback.objects.count(), 0)

    def test_can_create_with_labels(self):
        data = {
            "score": 0,
            "is_final": False,
            "of_submission": self.assignment.submission.pk,
            "labels": [self.fst_label.pk, self.snd_label.pk],
            "feedback_lines": {
                "2": {"text": "Why you no learn how to code, man?", "labels": []}
            },
        }
        self.assertEqual(self.fst_label.feedback.count(), 0)
        response = self.client.post(self.url(), data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.fst_label.refresh_from_db()
        self.snd_label.refresh_from_db()
        self.assertEqual(self.fst_label.feedback.count(), 1)
        self.assertEqual(self.snd_label.feedback.count(), 1)
        self.assertEqual(Feedback.objects.first().labels.count(), 2)

    def test_can_create_feedback_with_half_points(self):
        data = {
            "score": 0.5,
            "is_final": False,
            "of_submission": self.assignment.submission.pk,
            "feedback_lines": {
                "2": {"text": "Why you no learn how to code, man?", "labels": []}
            },
        }
        self.client.post(self.url(), data, format="json")
        object_score = self.sub.feedback.score
        self.assertEqual(object_score, 0.5)

    def test_check_score_is_set_accordingly(self):
        data = {
            "score": 5,
            "is_final": False,
            "of_submission": self.assignment.submission.pk,
            "feedback_lines": {
                "4": {"text": "Why you no learn how to code, man?", "labels": []}
            },
        }
        self.client.post(self.url(), data, format="json")
        object_score = self.sub.feedback.score
        self.assertEqual(object_score, 5)

    def test_can_create_feedback_with_comment(self):
        data = {
            "score": 0,
            "is_final": False,
            "of_submission": self.assignment.submission.pk,
            "feedback_lines": {"3": {"text": "Nice meth!", "labels": []}},
        }
        self.assertEqual(FeedbackComment.objects.count(), 0)
        response = self.client.post(self.url(), data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(FeedbackComment.objects.count(), 1)

    def test_feedback_comment_is_created_correctly(self):
        data = {
            "score": 0,
            "is_final": False,
            "of_submission": self.assignment.submission.pk,
            "feedback_lines": {"3": {"text": "Nice meth!", "labels": []}},
        }
        self.client.post(self.url(), data, format="json")
        comment = FeedbackComment.objects.first()
        self.assertEqual(comment.of_tutor, self.tutor)
        self.assertEqual(comment.text, "Nice meth!")
        self.assertIsNotNone(comment.created)
        self.assertEqual(comment.of_line, 3)
        self.assertTrue(comment.visible_to_student)

    def test_tutor_cannot_create_without_assignment(self):
        data = {
            "score": 0,
            "of_submission": self.assignment.submission.pk,
            "feedback_lines": {
                "2": {"text": "Well, at least you tried.", "labels": []},
            },
        }
        self.assignment.delete()
        response = self.client.post(self.url(), data, format="json")
        self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)

    def test_reviewer_can_create_without_assignment(self):
        data = {
            "score": 0,
            "of_submission": self.assignment.submission.pk,
            "feedback_lines": {
                "2": {"text": "This is not particularly good...", "labels": []},
            },
        }
        self.assignment.delete()
        self.client.force_authenticate(user=self.reviewer)
        response = self.client.post("/api/feedback/", data, format="json")
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

    @override_config(EXERCISE_MODE=True)
    def test_tutor_can_create_without_assignment_in_exercise_mode(self):
        data = {
            "score": 0,
            "of_submission": self.assignment.submission.pk,
            "feedback_lines": {
                "2": {"text": "You have failed!1!!11", "labels": []},
            },
        }
        self.assignment.delete()
        response = self.client.post("/api/feedback/", data, format="json")
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

    def test_cannot_create_with_someoneelses_assignment(self):
        data = {
            "score": 0,
            "of_submission": self.assignment.submission.pk,
            "feedback_lines": {
                "1": {"text": "Well, at least you tried.", "labels": []},
            },
        }
        other_tutor = self.user_factory.make_tutor("Berta")
        self.client.force_authenticate(other_tutor)
        response = self.client.post(self.url(), data, format="json")
        # returns 404 since the other users assignment is not visible to this one
        self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)

    def test_can_create_multiple_feedback_comments(self):
        data = {
            "score": 0,
            "is_final": False,
            "of_submission": self.assignment.submission.pk,
            "feedback_lines": {
                "1": {"text": "Nice meth!", "labels": []},
                "3": {"text": "Good one!", "labels": []},
            },
        }
        self.client.post(self.url(), data, format="json")
        first_comment = FeedbackComment.objects.get(text="Nice meth!")
        self.assertEqual(first_comment.of_tutor, self.tutor)
        self.assertIsNotNone(first_comment.created)
        self.assertEqual(first_comment.of_line, 1)
        self.assertTrue(first_comment.visible_to_student)

        second_comment = FeedbackComment.objects.get(text="Good one!")
        self.assertEqual(second_comment.of_tutor, self.tutor)
        self.assertIsNotNone(second_comment.created)
        self.assertEqual(second_comment.of_line, 3)
        self.assertTrue(second_comment.visible_to_student)


class FeedbackPatchTestCase(APITestCase):
    def finish_url(self):
        return f"/api/assignment/{self.assignment.pk}/finish/"

    @classmethod
    def setUpTestData(cls):
        cls.burl = "/api/feedback/"
        cls.data = make_test_data(
            {
                "exams": [
                    {
                        "module_reference": "Test Exam 01",
                        "total_score": 100,
                        "pass_score": 60,
                    }
                ],
                "submission_types": [
                    {
                        "name": "01. Sort this or that",
                        "full_score": 35,
                        "description": "Very complicated",
                        "solution": "Trivial!",
                    }
                ],
                "students": [
                    {"username": "student01", "exam": "Test Exam 01"},
                    {
                        "username": "student02",
                        "exam": "Test Exam 01",
                    },
                ],
                "tutors": [{"username": "tutor01"}, {"username": "tutor02"}],
                "reviewers": [
                    {
                        "username": "reviewer01",
                    }
                ],
                "submissions": [
                    {
                        "text": "function blabl\n"
                        "   on multi lines\n"
                        "       for blabla in bla:\n",
                        "type": "01. Sort this or that",
                        "user": "student01",
                    },
                    {
                        "text": "conflict test",
                        "type": "01. Sort this or that",
                        "user": "student02",
                    },
                ],
            }
        )

        cls.fst_label = FeedbackLabel.objects.create(name="Label1", description="Bla")
        cls.snd_label = FeedbackLabel.objects.create(name="Label2", description="Bla")

        # construct submission that has conflicting feedback
        cls.conflict_submission = Submission.objects.get(
            student__user=cls.data["students"][1]
        )
        conflict_meta = MetaSubmission.objects.get(
            submission__submission_id=cls.conflict_submission.submission_id
        )
        conflict_meta.done_assignments = 2
        conflict_meta.has_final_feedback = False
        conflict_meta.has_active_assignment = False
        conflict_meta.has_feedback = True
        conflict_meta.save()

        Feedback.objects.create(
            **{
                "score": 0,
                "is_final": False,
                "final_by_reviewer": False,
                "of_submission": cls.conflict_submission,
            }
        )

    def setUp(self):
        self.tutor01 = self.data["tutors"][0]
        self.tutor02 = self.data["tutors"][1]
        self.reviewer = self.data["reviewers"][0]
        self.client.force_authenticate(user=self.tutor01)
        self.assignment = TutorSubmissionAssignment.objects.create(
            submission=Submission.objects.get(student__user=self.data["students"][0]),
            owner=self.tutor01,
        )
        data = {
            "score": 35,
            "is_final": False,
            "of_submission": self.assignment.submission.pk,
            "feedback_lines": {
                "2": {"text": "Very good.", "labels": []},
            },
        }
        response = self.client.post(self.finish_url(), data, format="json")
        self.feedback = Feedback.objects.get(
            of_submission=response.data["of_submission"]
        )
        self.url = f"{self.burl}{self.feedback.of_submission.submission_id}/"

        self.fst_label.refresh_from_db()
        self.snd_label.refresh_from_db()

    def test_can_patch_onto_the_own_feedback(self):
        data = {
            "feedback_lines": {
                "1": {"text": "Spam spam spam", "labels": []},
            }
        }
        response = self.client.patch(self.url, data, format="json")
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(
            "Spam spam spam", response.data["feedback_lines"][1][0]["text"]
        )
        self.assertEqual("Very good.", response.data["feedback_lines"][2][0]["text"])

    def test_can_update_a_single_line(self):
        data = {
            "feedback_lines": {
                "2": {"text": "Turns out this is rather bad.", "labels": []},
            }
        }

        response = self.client.patch(self.url, data, format="json")
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    @unittest.expectedFailure
    def test_tutor_can_not_update_when_there_is_a_new_assignment(self):
        # Step 1 - Create a new assignment for Tutor 2
        TutorSubmissionAssignment.objects.create(
            submission=Submission.objects.last(),
            owner=self.tutor02,
            stage="feedback-validation",
        )

        # Step 2 - Tutor 1 tries to patch
        data = {
            "feedback_lines": {
                "2": {"text": "Turns out this is rather bad.", "labels": []},
            }
        }

        response = self.client.patch(self.url, data, format="json")
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)

    def test_cannot_patch_first_feedback_final(self):
        data = {
            "feedback_lines": {
                "2": {"text": "Turns out this is rather bad.", "labels": []},
            },
            "is_final": True,
        }

        response = self.client.patch(self.url, data, format="json")
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)

    def test_reviewer_can_patch_first_feedback_final(self):
        data = {
            "feedback_lines": {
                "2": {"text": "Turns out this is rather bad.", "labels": []},
            },
            "is_final": True,
        }

        self.client.force_authenticate(user=self.reviewer)
        response = self.client.patch(self.url, data, format="json")
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_cannot_unset_final_on_third_feedback(self):
        data = {
            "feedback_lines": {
                "2": {"text": "this is good.", "labels": []},
            },
            "is_final": False,
        }

        url = f"{self.burl}{self.conflict_submission.submission_id}/"
        self.client.force_authenticate(user=self.reviewer)
        response = self.client.patch(url, data, format="json")
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)

    def tutor_can_patch_labels(self):
        data = {
            "feedback_lines": {
                "2": {
                    "text": "Turns out this is rather bad.",
                    "labels": [self.fst_label.pk, self.snd_label.pk],
                },
            }
        }

        self.assertEqual(FeedbackComment.objects.first().labels.count(), 0)
        response = self.client.patch(self.url, data, format="json")
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(FeedbackComment.objects.first().labels.count(), 2)


class FeedbackCommentApiEndpointTest(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.burl = "/api/feedback/"
        cls.data = make_test_data(
            {
                "exams": [
                    {
                        "module_reference": "Test Exam 01",
                        "total_score": 100,
                        "pass_score": 60,
                    }
                ],
                "submission_types": [
                    {
                        "name": "01. Sort this or that",
                        "full_score": 35,
                        "description": "Very complicated",
                        "solution": "Trivial!",
                    }
                ],
                "students": [{"username": "student01", "exam": "Test Exam 01"}],
                "tutors": [
                    {"username": "tutor01"},
                    {"username": "tutor02"},
                ],
                "reviewers": [
                    {"username": "reviewer01"},
                ],
                "submissions": [
                    {
                        "text": "function blabl\n"
                        "   on multi lines\n"
                        "       for blabla in bla:\n",
                        "type": "01. Sort this or that",
                        "user": "student01",
                        "feedback": {
                            "score": 5,
                            "is_final": True,
                            "feedback_lines": {
                                "1": [
                                    {"text": "This is very bad!", "of_tutor": "tutor01"}
                                ],
                                "2": [
                                    {
                                        "text": "And this is even worse!",
                                        "of_tutor": "tutor02",
                                    }
                                ],
                            },
                        },
                    }
                ],
            }
        )

    def setUp(self):
        self.url = "/api/feedback-comment/%s/"
        self.tutor01 = self.data["tutors"][0]
        self.tutor02 = self.data["tutors"][1]

    def test_tutor_can_delete_own_comment(self):
        self.client.force_authenticate(user=self.tutor01)
        comment = FeedbackComment.objects.get(of_tutor=self.tutor01)
        response = self.client.delete(self.url % comment.pk)
        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)

    def test_tutor_cannot_delete_foreign_comment(self):
        self.client.force_authenticate(user=self.tutor02)
        comment = FeedbackComment.objects.get(of_tutor=self.tutor02)
        self.client.force_authenticate(self.tutor01)
        response = self.client.delete(self.url % comment.pk)
        self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)

    def test_reviewer_can_delete_other_users_comments(self):
        reviewer = self.data["reviewers"][0]
        self.client.force_authenticate(user=reviewer)
        comment01 = FeedbackComment.objects.get(of_tutor=self.tutor01)
        comment02 = FeedbackComment.objects.get(of_tutor=self.tutor02)

        response = self.client.delete(self.url % comment01.pk)
        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)

        response = self.client.delete(self.url % comment02.pk)
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)
        self.assertTrue(
            FeedbackComment.objects.filter(of_tutor=self.tutor02).exists(),
            msg="Second comment should not be deleted for feedback with not full score",
        )
        try:
            FeedbackComment.objects.get(of_tutor=self.tutor01)
        except FeedbackComment.DoesNotExist:
            pass
        else:
            self.fail("No exception raised")
