from django.test import TestCase

from core import models
from core.models import StudentInfo
from util.factories import GradyUserFactory, make_exams


class FactoryTestCase(TestCase):

    factory = GradyUserFactory()

    def test_make_student(self):
        self.exam = make_exams(exams=[{
                'module_reference': 'Test Exam 01',
                'total_score': 100,
                'pass_score': 60,
            }])[0]
        user = self.factory.make_student(exam=self.exam)

        self.assertEqual(StudentInfo.objects.count(), 1)
        self.assertEqual(user.student.exam.module_reference, "Test Exam 01")
        self.assertEqual(len(str(user.student.matrikel_no)), 8)

    def test_can_create_reviewer(self):
        self.assertTrue(isinstance(self.factory.make_reviewer(),
                                   models.UserAccount))

    def test_reviewer_appears_in_query_set(self):
        self.assertIn(self.factory.make_reviewer(),
                      models.UserAccount.objects.all())

    def test_can_create_tutor(self):
        self.assertIn(self.factory.make_tutor(),
                      models.UserAccount.objects.all())

    def test_can_create_student_user(self):
        self.exam = make_exams(exams=[{
                'module_reference': 'Test Exam 01',
                'total_score': 100,
                'pass_score': 60,
            }])[0]
        self.assertIn(self.factory.make_student(exam=self.exam),
                      models.UserAccount.objects.all())

    def test_can_create_student_info(self):
        self.exam = make_exams(exams=[{
                'module_reference': 'Test Exam 01',
                'total_score': 100,
                'pass_score': 60,
            }])[0]
        self.assertIn(self.factory.make_student(exam=self.exam).student,
                      StudentInfo.objects.all())
