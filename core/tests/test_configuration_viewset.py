import constance

from rest_framework import status
from rest_framework.test import APITestCase
from util.factories import GradyUserFactory, make_exams

config = constance.config


class ConfigurationViewTestCase(APITestCase):
    factory = GradyUserFactory()

    @classmethod
    def setUpTestData(cls):
        cls.exam = make_exams(exams=[{
            'module_reference': 'Test Exam 01',
            'total_score': 100,
            'pass_score': 60,
        }])[0]
        cls.student = cls.factory.make_student(exam=cls.exam)
        cls.reviewer = cls.factory.make_reviewer()

    def setUp(self):
        self.client.force_authenticate(user=self.reviewer)
        self.rev_list_response = self.client.get('/api/config/')

        self.client.force_authenticate(user=self.student)
        self.stud_list_response = self.client.get('/api/config/')

        stud_patch_data = {
            "singleCorrection": True,
        }

        rev_patch_data = {
            "exerciseMode": True,
            "showSolutionToStudents": False
        }

        self.client.force_authenticate(user=self.reviewer)
        self.rev_patch_response = self.client.patch('/api/config/change_config/', rev_patch_data)

        self.client.force_authenticate(user=self.student)
        self.stud_patch_response = self.client.patch('/api/config/change_config/', stud_patch_data)

    def test_student_can_access(self):
        self.assertEqual(status.HTTP_200_OK, self.stud_list_response.status_code)

    def test_reviewer_can_access(self):
        self.assertEqual(status.HTTP_200_OK, self.rev_list_response.status_code)

    def test_student_can_not_patch_config(self):
        self.assertEqual(status.HTTP_403_FORBIDDEN, self.stud_patch_response.status_code)
        self.assertEqual(False, config.SINGLE_CORRECTION)

    def test_reviewers_can_patch_config(self):
        self.assertEqual(status.HTTP_206_PARTIAL_CONTENT, self.rev_patch_response.status_code)
        self.assertEqual(True, config.EXERCISE_MODE)
        self.assertEqual(False, config.SHOW_SOLUTION_TO_STUDENTS)
