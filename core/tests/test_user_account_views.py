from rest_framework import status
from rest_framework.test import (APIClient, APITestCase)

from util.factories import GradyUserFactory, make_exams


class TutorReviewerCanChangePasswordTests(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user_factory = GradyUserFactory()
        cls.data = {
            'old_password': 'l',
            'new_password': 'chompreviver0.'
        }

    def setUp(self):
        self.reviewer = self.user_factory.make_reviewer(password='l')
        self.tutor1 = self.user_factory.make_tutor(password='l')
        self.tutor2 = self.user_factory.make_tutor(password='l')
        self.client = APIClient()

    def _change_password(self, changing_user, user_to_change=None, data=None):
        if user_to_change is None:
            user_to_change = changing_user
        if data is None:
            data = self.data

        self.client.force_authenticate(user=changing_user)
        url = f"/api/user/{user_to_change.pk}/change_password/"
        return self.client.patch(url, data=data)

    def test_tutor_needs_to_provide_current_password(self):
        res = self._change_password(self.tutor1,
                                    data={'new_password': 'chompreviver0.'})
        self.assertEqual(status.HTTP_401_UNAUTHORIZED, res.status_code)
        ret = self.client.login(username=self.tutor1.username,
                                password='chompreviver0.')
        self.assertFalse(ret)

    def test_reviewer_needs_to_provide_current_password_for_self(self):
        res = self._change_password(self.reviewer,
                                    data={'new_password': 'chompreviver0.'})
        self.assertEqual(status.HTTP_401_UNAUTHORIZED, res.status_code)
        ret = self.client.login(username=self.tutor1.username,
                                password='chompreviver0.')
        self.assertFalse(ret)

    def test_tutor_can_change_own_password(self):
        res = self._change_password(self.tutor1)
        self.assertEqual(status.HTTP_200_OK, res.status_code)
        ret = self.client.login(username=self.tutor1.username,
                                password='chompreviver0.')
        self.assertTrue(ret)

    def test_tutor_cant_change_other_password(self):
        res = self._change_password(self.tutor1, self.tutor2)
        self.assertEqual(status.HTTP_403_FORBIDDEN, res.status_code)
        ret = self.client.login(username=self.tutor2.username,
                                password='chompreviver0.')
        self.assertFalse(ret)

    def test_reviewer_can_change_own_password(self):
        res = self._change_password(self.reviewer)
        self.assertEqual(status.HTTP_200_OK, res.status_code)
        ret = self.client.login(username=self.reviewer.username,
                                password='chompreviver0.')
        self.assertTrue(ret)

    def test_reviewer_can_change_tutor_password(self):
        res = self._change_password(self.reviewer, self.tutor1,
                                    data={'new_password': 'chompreviver0.'})
        self.assertEqual(status.HTTP_200_OK, res.status_code)
        ret = self.client.login(username=self.tutor1.username,
                                password='chompreviver0.')
        self.assertTrue(ret)

    def test_student_cant_change_password(self):
        self.exam = make_exams(exams=[{
                'module_reference': 'Test Exam 01',
                'total_score': 100,
                'pass_score': 60,
            }])[0]
        student = self.user_factory.make_student(password='l', exam=self.exam)
        res = self._change_password(student)
        self.assertEqual(status.HTTP_403_FORBIDDEN, res.status_code)
        ret = self.client.login(username=student.username,
                                password='chompreviver0.')
        self.assertFalse(ret)

    def test_reviewer_cannot_revoke_own_access(self):
        user_pk = self.reviewer.pk
        url = f"/api/user/{user_pk}/change_active/"
        data = {'is_active': False}
        self.client.force_authenticate(user=self.reviewer)
        res = self.client.patch(url, data)
        self.assertEqual(status.HTTP_403_FORBIDDEN, res.status_code)


class ReviewerCanChangeCorrectorRoleTests(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user_factory = GradyUserFactory()

    def setUp(self):
        self.reviewer1 = self.user_factory.make_reviewer()
        self.client = APIClient()

    def _set_role(self, new_value, changing_user, user_to_change):
        self.client.force_authenticate(user=changing_user)
        url = f"/api/user/{user_to_change.pk}/change_role/"
        return self.client.patch(url, data={'role': new_value})

    def _make_reviewer(self, changing_user, user_to_change):
        return self._set_role('Reviewer', changing_user, user_to_change)

    def _make_tutor(self, changing_user, user_to_change):
        return self._set_role('Tutor', changing_user, user_to_change)

    def test_reviewer_can_promote_tutor_to_reviewer(self):
        tutor = self.user_factory.make_tutor()
        response = self._make_reviewer(self.reviewer1, tutor)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        tutor.refresh_from_db()
        self.assertTrue(tutor.is_reviewer())

    def test_reviewer_can_demote_other_reviewer_to_tutor(self):
        reviewer2 = self.user_factory.make_reviewer()
        response = self._make_tutor(self.reviewer1, reviewer2)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        reviewer2.refresh_from_db()
        self.assertFalse(reviewer2.is_reviewer())

    def test_reviewer_cannot_promote_student_to_reviewer(self):
        exam = make_exams(exams=[{
                'module_reference': 'Test Exam 01',
                'total_score': 100,
                'pass_score': 60,
            }])[0]
        student = self.user_factory.make_student(exam=exam)
        response = self._make_reviewer(self.reviewer1, student)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_reviewer_cannot_promote_student_to_tutor(self):
        exam = make_exams(exams=[{
                'module_reference': 'Test Exam 01',
                'total_score': 100,
                'pass_score': 60,
            }])[0]
        student = self.user_factory.make_student(exam=exam)
        response = self._make_tutor(self.reviewer1, student)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_student_cannot_change_access_rights(self):
        exam = make_exams(exams=[{
                'module_reference': 'Test Exam 01',
                'total_score': 100,
                'pass_score': 60,
            }])[0]
        student = self.user_factory.make_student(exam=exam)
        response = self._make_reviewer(student, self.reviewer1)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_tutor_cannot_change_access_rights(self):
        tutor = self.user_factory.make_tutor()
        response = self._make_reviewer(tutor, self.reviewer1)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_reviewer_cannot_demote_self_to_tutor(self):
        response = self._make_tutor(self.reviewer1, self.reviewer1)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
