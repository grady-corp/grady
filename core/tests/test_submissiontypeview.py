""" Tests that we can receive information about different submission types """

from constance.test import override_config
from django.urls import reverse
from rest_framework import status
from rest_framework.test import (APIRequestFactory, APITestCase,
                                 force_authenticate)

from core.models import SubmissionType
from core.views import SubmissionTypeApiView
from util.factories import GradyUserFactory, make_exams

# TODO: add tests to test the remaining counts in conjunction with the assignment logic
# TODO: also test for pass only and stuff


class SubmissionTypeViewTestList(APITestCase):

    @classmethod
    def setUpTestData(cls):
        cls.factory = APIRequestFactory()
        cls.user_factory = GradyUserFactory()

    def setUp(self):
        self.request = self.factory.get(reverse('submissiontype-list'))
        SubmissionType.objects.create(name='Hard question',
                                      full_score=20,
                                      description='Whatever')
        force_authenticate(self.request,
                           self.user_factory.make_reviewer())
        self.view = SubmissionTypeApiView.as_view({'get': 'list'})
        self.response = self.view(self.request)

    def test_can_access_when_authenticated(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_get_all_available_submissiontypes(self):
        self.assertEqual(1, len(self.response.data))

    def test_get_sumbission_type_name(self):
        self.assertEqual('Hard question', self.response.data[0]['name'])

    def test_get_full_score(self):
        self.assertEqual(20, self.response.data[0]['full_score'])


class SubmissionTypeViewTestRetrieve(APITestCase):

    @classmethod
    def setUpTestData(cls):
        cls.factory = APIRequestFactory()
        cls.user_factory = GradyUserFactory()
        cls.exam = make_exams(exams=[{
            'module_reference': 'Test Exam 01',
            'total_score': 100,
            'pass_score': 60,
        }])[0]
        cls.student = cls.user_factory.make_student(exam=cls.exam)

    def setUp(self):
        self.request = self.factory.get('/api/submissiontype/')
        SubmissionType.objects.create(name='Hard question',
                                      full_score=20,
                                      description='Whatever')
        self.pk = SubmissionType.objects.first().pk
        force_authenticate(self.request,
                           self.user_factory.make_reviewer())
        self.view = SubmissionTypeApiView.as_view({'get': 'retrieve'})
        self.response = self.view(self.request, pk=self.pk)

    def test_can_access_when_authenticated(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_students_can_not_access(self):
        request = self.factory.get('/api/submissiontype/')
        force_authenticate(request, self.student)
        view = SubmissionTypeApiView.as_view({'get': 'retrieve'})
        response = view(request, pk=self.pk)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    @override_config(SHOW_SOLUTION_TO_STUDENTS=True)
    def test_student_can_access_when_config_is_set(self):
        request = self.factory.get('/api/submissiontype/')
        force_authenticate(request, self.student)
        view = SubmissionTypeApiView.as_view({'get': 'retrieve'})
        response = view(request, pk=self.pk)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_id(self):
        self.assertEqual(str(self.pk), self.response.data['pk'])

    def test_get_sumbission_type_name(self):
        self.assertEqual('Hard question', self.response.data['name'])

    def test_get_full_score(self):
        self.assertEqual(20, self.response.data['full_score'])

    def test_get_descritpion(self):
        self.assertEqual('Whatever', self.response.data['description'])

    def test_there_is_no_solution_to_nothing(self):
        self.assertEqual('', self.response.data['solution'])
