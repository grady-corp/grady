from django.urls import reverse
from rest_framework.test import (APIRequestFactory, APITestCase,
                                 force_authenticate)

from core.models import SubmissionType
from core.views import StudentSelfApiView, StudentSelfSubmissionsApiView
from util.factories import make_test_data


class StudentPageTests(APITestCase):

    @classmethod
    def setUpTestData(cls):
        cls.factory = APIRequestFactory()

    def setUp(self):
        self.test_data = make_test_data(data_dict={
            'exams': [{
                'module_reference': 'TestExam B.Inf.0042',
                'total_score': 42,
                'pass_score': 21
            }],
            'submission_types': [{
                'name': 'problem01',
                'full_score': 10,
                'description': 'Very hard',
                'solution': 'Impossible!'
            }],
            'students': [{
                'username': 'user01',
                'fullname': 'us er01',
                'exam': 'TestExam B.Inf.0042'
            }],
            'tutors': [{
                'username': 'tutor01'
            }],
            'reviewers': [{
                'username': 'reviewer'
            }],
            'submissions': [{
                'user': 'user01',
                'type': 'problem01',
                'text': 'Too hard for me ;-(',
                'feedback': {
                    'text': 'Very bad!',
                    'score': 3,
                    'feedback_lines': {
                        '1': [{
                            'text': 'This is very bad!',
                            'of_tutor': 'tutor01'
                        }],
                    }
                }
            }]
        })

        self.student = self.test_data['students'][0]
        self.student_info = self.student.student
        self.tutor = self.test_data['tutors'][0]
        self.reviewer = self.test_data['reviewers'][0]
        self.submission = self.test_data['submissions'][0]
        self.feedback = self.submission.feedback

        self.request = self.factory.get(reverse('student-page'))
        self.view = StudentSelfApiView.as_view()
        force_authenticate(self.request, user=self.student)
        self.response = self.view(self.request)

        self.exam_obj = self.response.data['exam']
        self.submission_list = self.response.data['submissions']
        self.submission_list_first_entry = self.submission_list[0]

    def test_student_information_contains_name(self):
        self.assertEqual(
            self.response.data['name'], self.student.fullname)

    def test_all_student_submissions_are_loded(self):
        self.assertEqual(len(self.submission_list),
                         SubmissionType.objects.count())

    # Tests concerning exam data
    def test_exam_data_contains_module_reference(self):
        self.assertEqual(
            self.exam_obj["module_reference"],
            self.student_info.exam.module_reference)

    def test_exam_data_contains_total_score(self):
        self.assertEqual(
            self.exam_obj["total_score"], self.student_info.exam.total_score)

    def test_exam_data_contains_pass_score(self):
        self.assertEqual(
            self.exam_obj["pass_score"], self.student_info.exam.pass_score)

    def test_exam_data_contains_pass_only_field(self):
        self.assertEqual(
            self.exam_obj["pass_only"], self.student_info.exam.pass_only)

    # Tests concerning submission data
    def test_a_student_submissions_contains_type_name(self):
        self.assertEqual(
            self.submission_list_first_entry['type']['name'],
            self.student_info.submissions.first().type.name)

    def test_a_student_submissions_contains_type_id(self):
        self.assertEqual(
            self.submission_list_first_entry['type']['pk'],
            str(self.student_info.submissions.first().type.pk))

    def test_submission_data_contains_full_score(self):
        self.assertEqual(
            self.submission_list_first_entry['type']['full_score'],
            self.student_info.submissions.first().type.full_score)

    def test_submission_data_contains_feedback_score(self):
        self.assertEqual(
            self.submission_list_first_entry['feedback']['score'],
            self.student_info.submissions.first().feedback.score)

    # We don't want a matriculation number here
    def test_matriculation_number_is_not_send(self):
        self.assertNotIn('matrikel_no', self.submission_list_first_entry)


class StudentSelfSubmissionsTests(APITestCase):

    @classmethod
    def setUpTestData(cls):
        cls.factory = APIRequestFactory()

    def setUp(self):
        self.test_data = make_test_data(data_dict={
            'exams': [{
                'module_reference': 'Test Exam 01',
                'total_score': 100,
                'pass_score': 60,
            }],
            'submission_types': [{
                'name': 'problem01',
                'full_score': 10,
                'description': 'Very hard',
                'solution': 'Impossible!'
            }],
            'students': [{
                'username': 'user01',
                'exam': 'Test Exam 01'
            }],
            'tutors': [
                {
                    'username': 'tutor01'
                },
                {
                    'username': 'tutor02'
                }
            ],
            'submissions': [{
                'user': 'user01',
                'type': 'problem01',
                'text': 'Too hard for me ;-(',
                'feedback': {
                    'text': 'Very bad!',
                    'score': 3,
                    'feedback_lines': {
                        '1': [
                            {
                                'text': 'This is very bad!',
                                'of_tutor': 'tutor01',
                                # explicitness to required
                                # will also be set automatically
                                'visible_to_student': False
                            },
                            {
                                'text': 'This is good!',
                                'of_tutor': 'tutor02'
                            }
                        ],
                    }
                }
            }]
        })

        self.student = self.test_data['students'][0]
        self.student_info = self.student.student
        self.tutor = self.test_data['tutors'][0]
        self.submission = self.test_data['submissions'][0]
        self.feedback = self.submission.feedback

        self.request = self.factory.get(reverse('student-submissions'))
        self.view = StudentSelfSubmissionsApiView.as_view()

        force_authenticate(self.request, user=self.student)
        self.response = self.view(self.request)

        self.submission_list = self.response.data
        self.submission_list_first_entry = self.submission_list[0]

    # Tests concerning submission data
    def test_a_student_submissions_contains_type_name(self):
        self.assertEqual(
            self.submission_list_first_entry['type']['name'],
            self.student_info.submissions.first().type.name)

    def test_a_student_submissions_contains_type_id(self):
        self.assertEqual(
            self.submission_list_first_entry['type']['pk'],
            str(self.student_info.submissions.first().type.pk))

    def test_submission_data_contains_full_score(self):
        self.assertEqual(
            self.submission_list_first_entry['type']['full_score'],
            self.student_info.submissions.first().type.full_score)

    def test_submission_data_contains_description(self):
        self.assertEqual(
            self.submission_list_first_entry['type']['description'],
            self.student_info.submissions.first().type.description)

    def test_submission_data_not_contains_solution(self):
        self.assertNotIn('solution', self.submission_list_first_entry['type'])

    def test_submission_data_contains_final_status(self):
        self.assertEqual(
            self.submission_list_first_entry['feedback']['is_final'],
            self.student_info.submissions.first().feedback.is_final)

    def test_submission_data_contains_feedback_score(self):
        self.assertEqual(
            self.submission_list_first_entry['feedback']['score'],
            self.student_info.submissions.first().feedback.score)

    def test_submission_feedback_contains_submission_lines(self):
        self.assertIn(
            'feedback_lines',
            self.submission_list_first_entry['feedback']
        )

    def test_feedback_contains_one_comment_per_line(self):
        lines = self.submission_list_first_entry['feedback']['feedback_lines']
        self.assertEqual(len(lines[1]), 1)

    def test_feedback_comment_does_not_contain_tutor(self):
        lines = self.submission_list_first_entry['feedback']['feedback_lines']
        self.assertNotIn('of_tutor', lines[1][0])

    # We don't want a matriculation number here
    def test_matriculation_number_is_not_send(self):
        self.assertNotIn('matrikel_no', self.submission_list_first_entry)
