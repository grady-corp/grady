""" Tests that we can receive information about what exams where written """

from django.urls import reverse
from rest_framework import status
from rest_framework.test import (APIRequestFactory, APITestCase,
                                 force_authenticate)

from core.models import ExamType
from core.views import ExamApiViewSet
from util.factories import GradyUserFactory


class ExamListTest(APITestCase):
    """ briefly tests if we are able to retrieve data, and get correct fields
    """
    @classmethod
    def setUpTestData(cls):
        cls.factory = APIRequestFactory()
        cls.user_factory = GradyUserFactory()

    def setUp(self):
        self.request = self.factory.get(reverse('examtype-list'))
        self.examtype = ExamType.objects.create(module_reference='B.Inf.9000',
                                                total_score=90,
                                                pass_score=45)
        force_authenticate(self.request,
                           self.user_factory.make_reviewer())
        self.view = ExamApiViewSet.as_view({'get': 'list'})
        self.response = self.view(self.request)

    def test_can_access_when_authenticated(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_getting_all_available_exams(self):
        self.assertEqual(1, len(self.response.data))

        # Tests concerning exam data
    def test_exam_data_contains_module_reference(self):
        self.assertEqual('B.Inf.9000',
                         self.response.data[0]["module_reference"])

    def test_exam_data_contains_total_score(self):
        self.assertEqual(90, self.response.data[0]["total_score"])

    def test_exam_data_contains_pass_score(self):
        self.assertEqual(45, self.response.data[0]["pass_score"])

    def test_exam_data_contains_pass_only_field(self):
        self.assertEqual(False, self.response.data[0]["pass_only"])
