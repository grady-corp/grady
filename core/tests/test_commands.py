import json
import tempfile

from django.contrib.auth import get_user_model
from django.core.management import call_command
from django.test import TestCase

from util.factories import GradyUserFactory, make_exams


class CommandsTestCase(TestCase):

    factory = GradyUserFactory()

    def test_usermod(self):
        self.factory.make_tutor(username='otto')
        args = ['disable']
        opts = {'include': ('otto',)}
        call_command('usermod', *args, **opts)

        someone = get_user_model().objects.get(username='otto')
        self.assertFalse(someone.is_active)

    def test_replaceusernames(self):
        self.exam = make_exams(exams=[{
                'module_reference': 'Test Exam 01',
                'total_score': 100,
                'pass_score': 60,
            }])[0]
        self.factory.make_student(identifier=88884444, username='before', exam=self.exam)

        with tempfile.NamedTemporaryFile() as matno2username:
            matno2username.write(json.dumps({'88884444': 'after'}).encode())
            matno2username.flush()
            args = [matno2username.name]
            call_command('replaceusernames', *args, **{})

        student = get_user_model().objects.get(student__matrikel_no=88884444)
        self.assertEqual('after', student.username)
