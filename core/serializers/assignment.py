import secrets

from rest_framework import serializers

from core import models
from core.models import (Submission, TutorSubmissionAssignment)
from core.serializers import (DynamicFieldsModelSerializer, FeedbackSerializer,
                              TestSerializer)


class SubmissionAssignmentSerializer(DynamicFieldsModelSerializer):
    full_score = serializers.ReadOnlyField(source='type.full_score')
    tests = TestSerializer(many=True, read_only=True)

    class Meta:
        model = Submission
        fields = ('pk', 'type', 'text', 'full_score', 'tests', 'source_code_available')
        read_only_fields = ('text', 'type')


class AssignmentSerializer(DynamicFieldsModelSerializer):
    of_tutor = serializers.CharField(source='owner.username')

    class Meta:
        model = TutorSubmissionAssignment
        fields = ('pk', 'submission', 'is_done', 'owner', 'stage', 'of_tutor')
        read_only_fields = ('is_done', 'submission', 'owner')


class AssignmentDetailSerializer(AssignmentSerializer):
    feedback = FeedbackSerializer(source='submission.feedback', read_only=True)
    submission = SubmissionAssignmentSerializer(read_only=True)
    submission_type = serializers.UUIDField(write_only=True)
    group = serializers.UUIDField(write_only=True, required=False)

    class Meta:
        model = TutorSubmissionAssignment
        fields = ('pk', 'submission', 'feedback', 'is_done',
                  'owner', 'stage', 'submission_type', 'group')
        read_only_fields = ('is_done', 'submission', 'owner')

    def create(self, validated_data):
        owner = self.context['request'].user

        open_assignments = TutorSubmissionAssignment.objects.filter(
            owner=owner,
            is_done=False,
        )

        if len(open_assignments) > 2:
            raise models.NotMoreThanTwoOpenAssignmentsAllowed(
                'Not more than two active assignments allowed'
            )

        candidates = TutorSubmissionAssignment.objects.available_assignments({
            **validated_data,
            'owner': owner
        })

        length = len(candidates)

        if length == 0:
            raise models.SubmissionTypeDepleted(
                'There are no submissions left for the given criteria'
            )

        index = secrets.choice(range(length))

        return TutorSubmissionAssignment.objects.create(
            submission=candidates[index].submission,
            owner=owner,
            stage=validated_data.get('stage')
        )
