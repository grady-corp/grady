import logging

import constance
from django.db import transaction
from rest_framework import serializers

from core import models
from core.models import Feedback, UserAccount
from core.serializers import CommentDictionarySerializer
from util.factories import GradyUserFactory

from .generic import DynamicFieldsModelSerializer

config = constance.config
log = logging.getLogger(__name__)
user_factory = GradyUserFactory()


class FeedbackCommentSerializer(DynamicFieldsModelSerializer):
    of_tutor = serializers.StringRelatedField(source='of_tutor.username')
    labels = serializers.PrimaryKeyRelatedField(many=True, required=False,
                                                queryset=models.FeedbackLabel.objects.all())

    class Meta:
        model = models.FeedbackComment
        fields = ('pk',
                  'text',
                  'created',
                  'modified',
                  'of_tutor',
                  'of_line',
                  'labels',
                  'visible_to_student')
        # visible_to_student is kept in sync with modified, such that the latest modified
        # comment is the one that is visible
        read_only_fields = ('created', 'of_tutor', 'visible_to_student')
        extra_kwargs = {
            'of_feedback': {'write_only': True},
            'of_line': {'write_only': True},
        }
        list_serializer_class = CommentDictionarySerializer


class FeedbackSerializer(DynamicFieldsModelSerializer):
    feedback_lines = FeedbackCommentSerializer(many=True, required=False)
    of_submission_type = serializers.ReadOnlyField(
        source='of_submission.type.pk')
    feedback_stage_for_user = serializers.SerializerMethodField()
    labels = serializers.PrimaryKeyRelatedField(many=True, required=False,
                                                queryset=models.FeedbackLabel.objects.all())

    def get_feedback_stage_for_user(self, obj):
        """ Search for the assignment of this feedback and report in which
        stage the tutor has worked on it.

        TODO Note: This method is unorthodox since it mingles the rather dump
        feedback object with assignment logic. The reverse lookups in the
        method are not pre-fetched. Remove if possible. """
        if 'request' not in self.context:
            return

        # This is only required for tutors
        user = self.context['request'].user
        if user.role == models.UserAccount.REVIEWER:
            return None

        assignments = obj.of_submission.assignments.filter(owner=user)

        if assignments.count() == 0:
            return None

        return assignments[0].stage

    @transaction.atomic
    def create(self, validated_data) -> Feedback:
        submission = validated_data.pop('of_submission')
        feedback_lines = validated_data.pop('feedback_lines', [])
        labels = validated_data.pop('labels', [])
        user = self.context['request'].user
        if config.SINGLE_CORRECTION:
            is_final = True
            validated_data.pop('is_final')
        else:
            is_final = validated_data.pop('is_final', False)
        final_by_reviewer = is_final and \
            user.role == UserAccount.REVIEWER
        feedback = Feedback.objects.create(of_submission=submission,
                                           is_final=is_final,
                                           final_by_reviewer=final_by_reviewer,
                                           **validated_data)
        for label in labels:
            feedback.labels.add(label)

        submission.meta.feedback_authors.add(self.context['request'].user)

        for comment in feedback_lines:
            labels = comment.pop('labels', [])
            comment_instance = models.FeedbackComment.objects.create(
                of_feedback=feedback,
                of_tutor=self.context['request'].user,
                **comment
            )
            comment_instance.labels.set(labels)

        return feedback

    @transaction.atomic
    def update(self, feedback, validated_data):
        user = self.context['request'].user

        if user.role == UserAccount.REVIEWER:
            feedback.final_by_reviewer = self.context['request'].data['is_final']

        for comment in validated_data.pop('feedback_lines', []):
            labels = comment.pop('labels', None)
            comment_instance, _ = models.FeedbackComment.objects.update_or_create(
                of_feedback=feedback,
                of_tutor=self.context['request'].user,
                of_line=comment.get('of_line'),
                defaults={'text': comment.get('text')})

            if labels is not None:
                comment_instance.labels.set(labels)

        # Set all feedback final for when single correction is active
        if config.SINGLE_CORRECTION:
            feedback.is_final = True
            validated_data['is_final'] = True

        return super().update(feedback, validated_data)

    def validate_of_submission(self, submission):
        feedback = self.instance
        if feedback is not None and feedback.of_submission.pk != submission.pk:
            raise serializers.ValidationError(
                'It is not allowed to update this field.')

        return submission

    def validate_is_final(self, is_final):
        feedback = self.instance
        if feedback is None:
            return is_final

        user = self.context['request'].user
        meta = feedback.of_submission.meta
        user_in_authors = meta.feedback_authors.filter(id=user.id).exists()
        allowed_assignment_count = 3 if user_in_authors else 2
        if meta.done_assignments >= allowed_assignment_count and not is_final:
            raise serializers.ValidationError(
                'Conflict resolutions must be final'
            )

        return is_final

    def validate(self, data):
        if self.instance:
            score = data.get('score', self.instance.score)
            submission = data.get('of_submission', self.instance.of_submission)
        else:
            try:
                score = data.get('score')
                submission = data.get('of_submission')
            except KeyError:
                raise serializers.ValidationError(
                    'You need a score and a submission.')

        if not 0 <= score <= submission.type.full_score:
            raise serializers.ValidationError(
                f'Score has to be in range [0..{submission.type.full_score}].')

        if score.as_integer_ratio()[1] not in Feedback.ALLOWED_DENOMINATORS:
            raise serializers.ValidationError(
                f'For fractional scores, the denominator must be one of '
                f'{Feedback.ALLOWED_DENOMINATORS}'
            )

        has_full_score = score == submission.type.full_score
        has_feedback_lines = ('feedback_lines' in data and
                              len(data['feedback_lines']) > 0 or
                              self.instance is not None and
                              self.instance.feedback_lines.count() > 0)

        has_label_attached = ('labels' in data and len(data['labels']) > 0 or
                              self.instance is not None and
                              self.instance.labels.count() > 0)

        labels_get_deleted = 'labels' in data and len(data['labels']) == 0

        # a non-full scored feedback is considered valid if there is
        # at least one comment line or a label attached to the feedback
        if not (has_full_score or has_feedback_lines or
                (has_label_attached and not labels_get_deleted)):
            raise serializers.ValidationError(
                'Sorry, you have to explain why this does not get full score')

        if hasattr(submission, 'feedback') and not self.instance:
            raise serializers.ValidationError(
                'Feedback for this submission already exists')

        for comment in data.get('feedback_lines', {}):
            lines_in_submission = len(submission.text.split('\n'))

            if comment['text'] == '' and 'labels' not in comment:
                raise serializers.ValidationError(
                    "Cannot create feedback with an empty comment attached to it"
                )
            if not 0 < comment['of_line'] <= lines_in_submission:
                raise serializers.ValidationError(
                    "Cannot comment line number %d of %d" % (
                        comment['of_line'], lines_in_submission))

        return data

    class Meta:
        model = Feedback
        fields = ('pk', 'of_submission', 'is_final', 'score', 'feedback_lines',
                  'created', 'modified', 'of_submission_type', 'feedback_stage_for_user', 'labels')


class FeedbackWithStudentSerializer(FeedbackSerializer):
    of_student = serializers.ReadOnlyField(source='of_submission.student.user.fullname')

    class Meta:
        model = Feedback
        fields = ('pk', 'of_submission', 'is_final', 'score', 'feedback_lines', 'of_student',
                  'created', 'modified', 'of_submission_type', 'feedback_stage_for_user', 'labels')


class VisibleCommentFeedbackSerializer(FeedbackSerializer):
    feedback_lines = serializers.SerializerMethodField()
    of_submission_type = serializers.ReadOnlyField(
        source='of_submission.type.pk')

    def get_feedback_lines(self, feedback):
        comments = feedback.feedback_lines.filter(visible_to_student=True)
        serializer = FeedbackCommentSerializer(
            comments,
            many=True,
            fields=('pk', 'text', 'created', 'modified', 'of_line', 'labels')
        )
        # this is a weird hack because, for some reason, serializer.data
        # just won't contain the correct data. Instead .data returns a list
        # containing just the `of_line` attr of the serialized comments
        # after long debugging i found that for inexplicable reasons
        # `data.serializer._data` contains the correct data. No clue why.
        return serializer.data.serializer._data

    class Meta:
        model = Feedback
        fields = ('pk', 'of_submission', 'is_final', 'score', 'feedback_lines',
                  'created', 'of_submission_type', 'labels')
