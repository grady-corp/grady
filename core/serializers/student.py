from rest_framework import serializers

from core.models import StudentInfo
from core.serializers import DynamicFieldsModelSerializer, ExamSerializer
from core.serializers.submission import (SubmissionListSerializer,
                                         SubmissionNoTextFieldsSerializer,
                                         SubmissionNoTypeSerializer)


class StudentInfoSerializer(DynamicFieldsModelSerializer):
    name = serializers.ReadOnlyField(source='user.fullname')
    matrikel_no = serializers.ReadOnlyField(source='user.matrikel_no')
    exam = ExamSerializer()
    submissions = SubmissionListSerializer(many=True)

    class Meta:
        model = StudentInfo
        fields = ('pk',
                  'name',
                  'user',
                  'matrikel_no',
                  'exam',
                  'submissions',
                  'passes_exam')


class StudentInfoForListViewSerializer(DynamicFieldsModelSerializer):
    name = serializers.ReadOnlyField(source='user.fullname')
    user = serializers.ReadOnlyField(source='user.username')
    user_pk = serializers.ReadOnlyField(source='user.pk')
    exam = serializers.ReadOnlyField(source='exam.module_reference')
    submissions = SubmissionNoTextFieldsSerializer(many=True)
    is_active = serializers.BooleanField(source='user.is_active')

    class Meta:
        model = StudentInfo
        fields = ('pk',
                  'name',
                  'user',
                  'user_pk',
                  'exam',
                  'submissions',
                  'matrikel_no',
                  'passes_exam',
                  'is_active')


class StudentExportSerializer(DynamicFieldsModelSerializer):
    name = serializers.ReadOnlyField(source='user.fullname')
    user = serializers.ReadOnlyField(source='user.username')
    user_pk = serializers.ReadOnlyField(source='user.pk')
    exam = serializers.ReadOnlyField(source='exam.pk')
    email = serializers.ReadOnlyField(source='user.email')
    is_active = serializers.BooleanField(source='user.is_active')
    submissions = SubmissionNoTypeSerializer(many=True)

    class Meta:
        model = StudentInfo
        fields = ('pk',
                  'name',
                  'user',
                  'user_pk',
                  'exam',
                  'email',
                  'submissions',
                  'matrikel_no',
                  'passes_exam',
                  'is_active')
