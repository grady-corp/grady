from rest_framework import serializers

from core.models import FeedbackLabel


class LabelSerializer(serializers.ModelSerializer):
    class Meta:
        model = FeedbackLabel
        fields = (
            'pk',
            'name',
            'description',
            'colour'
        )
