from rest_framework import serializers

from core.models import Submission
from core.serializers import (DynamicFieldsModelSerializer, FeedbackSerializer,
                              SubmissionTypeListSerializer,
                              SubmissionTypeSerializer, TestSerializer,
                              VisibleCommentFeedbackSerializer)


class SubmissionNoTextFieldsSerializer(DynamicFieldsModelSerializer):
    score = serializers.ReadOnlyField(source='feedback.score')
    final = serializers.ReadOnlyField(source='feedback.is_final')
    full_score = serializers.ReadOnlyField(source='type.full_score')

    class Meta:
        model = Submission
        fields = ('pk', 'type', 'score', 'final', 'full_score')


class StudentSubmissionSerializer(DynamicFieldsModelSerializer):
    type = SubmissionTypeSerializer(
        # exclude solution from Type information
        fields=(('pk',
                 'name',
                 'full_score',
                 'description',
                 'programming_language')))
    feedback = VisibleCommentFeedbackSerializer()
    tests = TestSerializer(many=True)

    class Meta:
        model = Submission
        fields = ('pk', 'type', 'text', 'feedback', 'tests', 'source_code_available')


class StudentSubmissionWithSolutionSerializer(StudentSubmissionSerializer):
    type = SubmissionTypeSerializer()

    class Meta:
        model = Submission
        fields = ('pk', 'type', 'text', 'feedback', 'tests', 'source_code_available')


class SubmissionNoTypeSerializer(DynamicFieldsModelSerializer):
    feedback = FeedbackSerializer()
    full_score = serializers.ReadOnlyField(source='type.full_score')
    tests = TestSerializer(many=True)

    class Meta:
        model = Submission
        fields = ('pk', 'type', 'full_score', 'text', 'feedback', 'tests', 'source_code_available')


class SubmissionNoTypeWithStudentSerializer(SubmissionNoTypeSerializer):
    of_student = serializers.ReadOnlyField(source='student.user.fullname')

    class Meta:
        model = Submission
        fields = ('pk', 'type', 'full_score', 'text', 'feedback',
                  'tests', 'of_student', 'source_code_available')


class SubmissionListSerializer(DynamicFieldsModelSerializer):
    type = SubmissionTypeListSerializer(fields=('pk', 'name', 'full_score'))
    feedback = FeedbackSerializer()

    class Meta:
        model = Submission
        fields = ('pk', 'type', 'feedback')
