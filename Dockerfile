FROM node:fermium as node

# add the current (26.11.23) GitHub-Host RSA SSH-key to known_hosts
# (manually grabbed from https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/githubs-ssh-key-fingerprints)
RUN mkdir ~/.ssh \
    && touch ~/.ssh/known_hosts \
    && echo "github.com ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCj7ndNxQowgcQnjshcLrqPEiiphnt+VTTvDP6mHBL9j1aNUkY4Ue1gvwnGLVlOhGeYrnZaMgRK6+PKCUXaDbC7qtbW8gIkhL7aGCsOr/C56SJMy/BCZfxd1nWzAOxSDPgVsmerOBYfNqltV9/hWCqBywINIR+5dIg6JTJ72pcEpEjcYgXkE2YEFXV1JHnsKgbLWNlhScqb2UmyRkQyytRLtL+38TGxkxCflmO+5Z8CSSNY7GidjMIZ7Q4zMjA2n1nGrlTDkzwDCsw+wqFPGQA179cnfGWOWRVruj16z6XyvxvjJwbz0wQZ75XK5tKSb7FNyeIEs4TT4jk+S4dhPeAUC5y+bDYirYgM4GC7uEnztnZyaVWQ7B381AK4Qdrwt51ZqExKbQpTUNn+EjqoTwvqNj4kqx5QUCI0ThS/YkOxJCXmPUWZbhjpCg56i+2aB6CmK2JGhn57K5mj0MNdBXA4/WnwH6XoPWJzK5Nyu2zB3nAZp+S5hpQs+p1vN1/wsjk=" >> ~/.ssh/known_hosts

WORKDIR /app/
COPY frontend/package.json .
COPY frontend/yarn.lock .
RUN yarn

# CACHED
COPY frontend/ .
RUN yarn build

# FROM alpine:edge
# Python 3.11
# FROM alpine:3.19
FROM python:3.11-alpine

WORKDIR /code

# This set is needed otherwise the postgres driver wont work
# RUN apk update \
#     && apk add build-base gcc curl libzmq musl-dev zeromq-dev \
#     && apk add --no-cache postgresql-dev git

COPY Pipfile Pipfile.lock ./

# might need postgresql-dev gcc musl-dev libzmq zeromq
RUN apk update \
    && apk add --no-cache git postgresql-dev libpq \
    && pip install --no-cache-dir micropipenv \
    && micropipenv install --deploy \
    && pip uninstall -y micropipenv \
    && pip list \
    && python --version

# CACHED
COPY . .
COPY --from=node /app/dist /code/frontend/dist
COPY --from=node /app/dist/index.html /code/core/templates/index.html

ENV PYTHONUNBUFFERED 1
RUN python util/format_index.py
RUN python manage.py collectstatic --noinput

CMD ["./deploy.sh"]
