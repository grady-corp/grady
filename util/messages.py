import sys


def warn(*message):
    print('[W]', *message)


def debug(*message):
    print('[DEBUG]', *message)


def info(*message):
    print('[I]', *message)


def error(*message):
    print('[E]', *message)


def abort(*message):
    print('[FATAL]', *message)
    sys.exit('exiting...')


def exit(message='exiting...'):
    sys.exit(*message)
