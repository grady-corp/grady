from .default import *  # noqa
import os

dev = os.environ.get('DJANGO_DEV', False)

if not dev:
    from .live import *  # noqa
    from .url_hack import *  # noqa
