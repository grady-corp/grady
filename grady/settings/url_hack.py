""" Ok, what the hell? This is especially ugly, hence I keep it hidden in
this file. We have the requirement that the application instances should
run under http://$host/$instancename/. And therefore the frontend, whitenoise,
django, gunicorn and the top http proxy all have to handle this stuff.

Usage: Just set the SCRIPT_NAME env variable to /<name of your instance>
       and things will work. """

import os

FORCE_SCRIPT_NAME = os.environ.get('SCRIPT_NAME', '')
if FORCE_SCRIPT_NAME:
    FORCE_SCRIPT_NAME += '/'

STATIC_URL_BASE = '/static/'
STATIC_URL = os.path.join(FORCE_SCRIPT_NAME + STATIC_URL_BASE)
WHITENOISE_STATIC_PREFIX = STATIC_URL_BASE
