
# TagIDs (ID strings to identify nav elements)
overview        -   (sidebar element 1, tutor)  -   "homepage" - has ChangeLog, mini statistics
feedback        -   (sidebar element 2, tutor)  -   overview over feedback history and state
statistics      -   (sidebar element 3, tutor)  -   correction statistics
participants    -   (sidebar element 4, rev)    -   overview over all participating students and their scores
tutors          -   (sidebar element 5, rev)    -   overview over all correcting tutors
summary         -   (sidebar element 6, rev)    -   full exhaustive overview over the state of correction



This file serves the purpose to document Object structures
and how to access them through the API.
(it's not automated => error prone, but better than nothing)


the 3 index files that import mathjax
frontend/dist/index.html
frontend/public/index.html
core/templates/index.html


Feedback Object
(the feedback given by a tutor/reviewer for an assignment)
- created (timestamp for initial creation)
- feedbackLines (an array containing the lines that have comments {line-num, comment-text})
- feedbackStageForUser
- isFinal (boolean: is this feedback finalized or to be reviewed ?)
- labels (list of assigned labels - NOTE: only the label indices !)
- modified (timestamp for last modification)
- ofSubmission (the pk of the underlying submission)
- ofSubmissionType (the pk of the underlying task)
- pk (feedback "personal key")
- score (achieved score in this task)

you can get the feedback Objects like this
--- api.fetchAllFeedback()



Submission Object
(the submission of a student for a given task)
- feedback (the feedback object assigned to this submission IFF one is present)
- fullScore (the maximum possible score this submission could have reached)
- ofStudent (the NAME of the submitting student)
- pk (submission "personal key")
- sourceCodeAvailable
- tests
- text (the body of the submission as a consecutive text wall)
- type (the pk of the underlying task)

you can get the submission Objects like this
--- api.fetchSubmissionObj(SUBMISSION_PK)



Student Object
(data about a student and their submissions)
- exam (name of the exam this belongs to)
- isActive
- matrikelNo (actual matrikel num or encrypted pseudo name)
- name (actual name or encrypted pseudo name)
- passesExam (boolean: does this student pass yet ?)
- pk (student "personal key")
- submissions (array containing the students submissions
               - NOTE: only trimmed versions of submission obj !)
- userPk

you can get the student Objects like this
--- api.fetchAllStudents()