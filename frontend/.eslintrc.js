module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/recommended',
    '@vue/typescript',
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'semi': ['error', 'never'],
    'quotes': ['error', 'single'],
    'eqeqeq': 'error',
    'default-case': 'error',
    'guard-for-in': 'error',
    'yoda': 'error',
    'no-trailing-spaces': 'error',
    '@typescript-eslint/consistent-type-assertions': ['error', { assertionStyle: 'as' }]
  },
  parserOptions: {
    parser: '@typescript-eslint/parser',
  },
  plugins: ['@typescript-eslint'],
  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
      ],
      env: {
        mocha: true,
      },
    },
  ],
}
