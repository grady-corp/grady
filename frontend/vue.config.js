const path = require('path')

const projectRoot = path.resolve(__dirname)

module.exports = {
  assetsDir: 'static',
  devServer: {
    allowedHosts: ['localhost'],
    host: 'localhost'
  },
  configureWebpack: config => {
    config.resolve.alias['@'] = `${projectRoot}/src`

    if (process.env.NODE_ENV === 'development') {
      config.devtool = 'source-map'
    }

    // keep_fnames ist set to true because vuex-typex is dependant on the function names
    if (process.env.NODE_ENV === 'production') {
      config.optimization.minimizer[0].options.terserOptions.keep_fnames = true
    }
  }
}
