import Vue from 'vue'
import Component from 'vue-class-component'
import { Prop } from 'vue-property-decorator'
import { SubmissionNotes } from '@/store/modules/submission-notes'
import { FeedbackComment, FeedbackLabel } from '@/models'
import { FeedbackLabels } from '@/store/modules/feedback-labels'

enum FeedbackType {
  original = 'origFeedback',
  updated = 'updatedFeedback',
}

@Component
export default class commentLabelSelector extends Vue {
  @Prop({ type: String, required: true }) readonly lineNo!: string

  /**
   * Returns array of label pk's where feedbackType is
   * either "origFeedback" or "updatedFeedback"
   *
   * Will return null when labels property does not exist on the requested state's comment
   * This is the case when the labels have not been updated, as we don't want to have
   * the labels field in the object if the labels have not changed.
   */
  copyStateLabels(feedbackType: FeedbackType): number[] | null {
    const currentLine = this.getFeedbackLine(feedbackType)
    if (currentLine && currentLine.labels) {
      return currentLine.labels
    } else {
      return null
    }
  }

  /**
   * Gets the latest feedback line object for the current lineNo and the given feedbackType
   * @param feedbackType  The type to get the latest line from
   */
  getFeedbackLine (feedbackType: FeedbackType): FeedbackComment | undefined {

    // helper used to determine the correct type to reduce redundancy
    function isArray(val: FeedbackComment | FeedbackComment[]): val is FeedbackComment[] {
      return (val as FeedbackComment[]).length !== undefined
    }

    const stateLines = SubmissionNotes.state[feedbackType].feedbackLines
    if (stateLines && Object.keys(stateLines).length > 0) {
      let lines = stateLines[Number(this.lineNo)]
      if (!lines) return undefined

      if (isArray(lines)) {
        return lines.length > 0 ? lines[lines.length-1] : undefined
      } else {
        return lines
      }
    }

    return undefined
  }

  getUnchangedLabels() {
    const labelsOrig = this.copyStateLabels(FeedbackType.original)
    if (labelsOrig === null || labelsOrig.length === 0) {
      return new Array ()
    }
    const removedLabels = this.getRemovedLabels()
    const addedLabels = this.getAddedLabels()

    return labelsOrig.filter((val) => {
      return !removedLabels.includes(val) && !addedLabels.includes(val)
    })
  }

  getRemovedLabels() {
    const currentLine = this.getFeedbackLine(FeedbackType.updated)
    if (currentLine === undefined) return new Array()

    const labelsOrig = this.copyStateLabels(FeedbackType.original)
    const labelsUpdated = this.copyStateLabels(FeedbackType.updated)

    if (labelsOrig === null || labelsUpdated === null) {
      return new Array()
    }

    return labelsOrig.filter((val) => {
      return !labelsUpdated.includes(val)
    })
  }

  getAddedLabels() {
    const labelsOrig = this.copyStateLabels(FeedbackType.original)
    const labelsUpdated = this.copyStateLabels(FeedbackType.updated)

    if (labelsUpdated === null) {
      return new Array()
    }

    if (labelsOrig === null) {
      return labelsUpdated ? labelsUpdated : new Array()
    }

    return labelsUpdated.filter((val) => {
      return !labelsOrig.includes(val)
    })
  }

  /**
   * Maps label pk's to the objects stored in vuex store
   */
  mapPksToLabelObj(pkArr: number[]): FeedbackLabel[] {
    const mappedLabels = pkArr.map((val) => {
      const label = FeedbackLabels.availableLabels.find((label) => {
        return label.pk === val
      })

      if (!label) return undefined
      return {
        pk: val,
        name: label.name,
        description: label.description,
        colour: label.colour
      }
    }).filter((val): val is FeedbackLabel => {
      if (!val) {
        return false
      }

      return true
    })

    return mappedLabels
  }
}
