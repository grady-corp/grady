<template>
  <v-card>
    <v-card-title class="title">
      <span v-if="isTutor">Your</span><span v-else>Overall</span>&nbsp;feedback history
    </v-card-title>
    <v-data-table
      :headers="headers"
      :items="filteredFeedback"
      sort-by="student"
    >
      <template #top>
        <feedback-search-options
          v-model="searchOptions"
        />
      </template>
      <template #item="{ item }">
        <tr
          class="feedback-row"
          @click="showSubmission(item)"
        >
          <td>{{ item.ofSubmissionType }}</td>
          <td v-if="exerciseMode">
            {{ item.ofStudent }}
          </td>
          <td>{{ item.score }}</td>
          <td>{{ new Date(item.created).toLocaleString() }}</td>
          <td>{{ new Date(item.modified).toLocaleString() }}</td>
          <td>
            <v-icon v-if="item.isFinal">
              check
            </v-icon>
            <v-icon v-else>
              clear
            </v-icon>
          </td>
          <td>
            <v-btn
              icon
              :color="item.mark"
              @click="changeMark(item.ofSubmission, item.mark)"
            />
          </td>
        </tr>
      </template>
    </v-data-table>
  </v-card>
</template>

<script lang="ts">
import Vue from 'vue'
import { mapState, mapGetters } from 'vuex'
import Component from 'vue-class-component'
import { getObjectValueByPath } from '@/util/helpers'
import FeedbackSearchOptions, { FeedbackSearchOptionsModel } from '@/components/feedback_list/FeedbackSearchOptions.vue'
import { FeedbackTable as FeedbackModule, FeedbackHistoryItem } from '@/store/modules/feedback_list/feedback-table'
import { FeedbackStageEnum, Feedback, FeedbackLabel } from '@/models'
import { actions } from '@/store/actions'
import { getters } from '@/store/getters'
import { Authentication } from '../../store/modules/authentication'
import { ConfigModule } from '../../store/modules/config'

function extractLabelsFromFeedback(feedback: FeedbackHistoryItem): Set<number> {
  let labels: Set<number> = new Set(feedback.labels)

  Object.values(feedback.feedbackLines || {}).forEach(comments => {
    comments.forEach(comment => {
      if (!comment.visibleToStudent) {
        return
      }
      comment.labels.forEach(label => {
        labels.add(label)
      })
    })
  })

  return labels
}

@Component({
  components: {
    FeedbackSearchOptions
  }
})
export default class FeedbackTable extends Vue {
  get isTutor () { return Authentication.isTutor }
  get exerciseMode () { return ConfigModule.state.config.instanceSettings.exerciseMode }

  get headers(): {text: string, value: string, align?: string}[] {
    return [
      { text: 'Type', align: 'left', value: 'ofSubmissionType' },
      ...(this.exerciseMode ? [{ text: 'Student', value: 'ofStudent' }] : []),
      { text: 'score', value: 'score' },
      { text: 'Created', value: 'created' },
      { text: 'Modified', value: 'modified', },
      { text: 'Final', value: 'final' },
      { text: 'Mark', value: 'mark' }
    ]
  }

  get feedback () {
    return Object.values(FeedbackModule.state.feedbackHist)
  }

  searchOptions: FeedbackSearchOptionsModel = {
    searchString: '',
    showFinal: true,
    caseSensitive: false,
    useRegex: false,
    filterByTutors: [],
    filterByStage: undefined,
    filterByLabels: [],
    filterByExcludingLabels: [],
  }

  get queryFoundInString(): (s: string) => boolean {
    if (this.searchOptions.useRegex) {
      const flags = this.searchOptions.caseSensitive ? 'u' : 'iu'
      try {
        const re = new RegExp(this.searchOptions.searchString , flags)
        return s => re.test(s)
      } catch {
        return _ => true
      }
    } else {
      if (this.searchOptions.caseSensitive)
        return s => s.includes(this.searchOptions.searchString)
      else
        return s => s.toLowerCase().includes(this.searchOptions.searchString.toLowerCase())
    }
  }

  queryFoundInFields(f: Feedback): boolean {
    return f.ofSubmissionType !== undefined && this.queryFoundInString(f.ofSubmissionType) ||
           this.exerciseMode && f.ofStudent !== undefined && this.queryFoundInString(f.ofStudent) ||
           f.created !== undefined && this.queryFoundInString(f.created) ||
           f.modified !== undefined && this.queryFoundInString(f.modified)
  }

  queryFoundInComments(feedback: Feedback): boolean {
    return Object.values(feedback.feedbackLines ?? {})
                 .some(line => line.map(comment => comment.text).some(this.queryFoundInString))
  }

  allLabelsFromFilterFoundOn(f: Feedback): boolean {
    const fLabels = extractLabelsFromFeedback(f)
    return this.searchOptions.filterByLabels.every(l => fLabels.has(l.pk))
  }

  noExcludedLabelFoundOn(f: Feedback): boolean {
    const fLabels = extractLabelsFromFeedback(f)
    return this.searchOptions.filterByExcludingLabels.every(l => !fLabels.has(l.pk))
  }

  // TODO: it is possible that a user is not assigned to the feedback, but still made a comment on it
  // this happens when a reviewer adds a comment to a feedback that he has never corrected himself
  // we could either ignore this case or add every user that has made a comment to a feedback
  // to the list of associated users in the filteredTutorsContributedToFeedback method

  filteredTutorsContributedToFeedback(f: FeedbackHistoryItem): boolean {
    if (this.searchOptions.filterByTutors.length === 0)
      return true

    const stages = this.searchOptions.filterByStage ? [this.searchOptions.filterByStage]
                                                    : Object.values(FeedbackStageEnum)
    const associatedTutors = stages.map(stage => f.history?.[stage]?.ofTutor).filter(x => !!x)
    return this.searchOptions.filterByTutors.some(tutor => associatedTutors.includes(tutor.username))
  }

  get filteredFeedback() {
    return this.feedback.filter(f => {
      return (!f.isFinal || this.searchOptions.showFinal) &&
             (this.queryFoundInFields(f) || this.queryFoundInComments(f)) &&
             this.allLabelsFromFilterFoundOn(f) &&
             this.noExcludedLabelFoundOn(f) &&
             this.filteredTutorsContributedToFeedback(f)
    })
  }

  showSubmission (feedback: Feedback) {
    this.$router.push(`/feedback/${feedback.ofSubmission}`)
  }

  changeMark(submissionPk: string, currColor: string) {
    const colorArr = ['red lighten-2', 'blue lighten-2', 'green lighten-2', 'transparent']
    const newColor = colorArr[(colorArr.indexOf(currColor) + 1) % colorArr.length]
    FeedbackModule.SET_MARK_COLOR({submissionPk, color: newColor})
  }
}
</script>

<style scoped>
  .feedback-row {
    cursor: pointer;
  }
</style>
