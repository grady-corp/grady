
import './class-component-hooks'

import Vue from 'vue'
import store from './store/store'
import App from './App.vue'
import router from './router/index'
import Vuetify from 'vuetify'
import Notifications from 'vue-notification'
// the following imports have fakeout types declared within frontend\@types\*.d.ts
import Clipboard from 'v-clipboard'
import VueKatex from 'vue-katex-auto-render'

import 'vuetify/dist/vuetify.min.css'
import 'highlight.js/styles/atom-one-light.css'

import '@/util/shortkeys'

Vue.use(Vuetify)
Vue.use(Notifications)
Vue.use(Clipboard)
// need to use this form of inclusion rather tan .use()
Vue.directive('katex', VueKatex)

Vue.config.productionTip = false

const el = process.env.NODE_ENV === 'test' ? undefined : '#app'

const vuetify = new Vuetify({
 icons: {
    iconfont: 'md',
  },
})

export default new Vue({
  vuetify,
  el: el,
  router: router,
  store,
  render: h => h(App)
}).$mount(el)
