import { RootState } from '@/store/store'
import { getStoreBuilder } from 'vuex-typex'

const mb = getStoreBuilder<RootState>()

const stateGetter = mb.state()

const correctedGetter = mb.read(function corrected (state) {
  const progresses = state.statistics.submissionTypeProgress
  return progresses.length > 0 && progresses.every(progress => {
    return progress.feedbackFinal === progress.submissionCount
  })
})
const submissionGetter = mb.read(function submission (state) {
  return (pk: string) => {
    return state.submissions[pk]
  }
})
const submissionTypeGetter = mb.read(function submissionType (state) {
  return (pk: string) => {
    return state.submissionTypes[pk]
  }
})

export const getters = {
  get state () { return stateGetter() },
  get corrected () { return correctedGetter() },
  get submission () { return submissionGetter() },
  get submissionType () { return submissionTypeGetter() }
}
