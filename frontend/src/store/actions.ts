import { ActionContext } from 'vuex'
import { BareActionContext, getStoreBuilder } from 'vuex-typex'

import { mutations as mut } from './mutations'
import { Authentication } from '@/store/modules/authentication'
import { SubmissionNotes } from '@/store/modules/submission-notes'
import * as api from '@/api'
import router from '@/router/index'
import { RootState } from '@/store/store'
import { FeedbackTable } from '@/store/modules/feedback_list/feedback-table'
import { Assignments } from '@/store/modules/assignments'
import { TutorOverview } from './modules/tutor-overview'
import { StudentPage } from './modules/student-page'
import { ConfigModule } from './modules/config'

async function getExamTypes (context: BareActionContext<RootState, RootState>) {
  const examTypes = await api.fetchExamTypes()
  mut.SET_EXAM_TYPES(examTypes)
}
async function updateSubmissionTypes (){
  const submissionTypes = await api.fetchSubmissionTypes()
  submissionTypes.forEach(type => {
    mut.UPDATE_SUBMISSION_TYPE(type)
  })
}

async function updateSubmissionType (
  context: BareActionContext<RootState, RootState>,
  pk: string
) {
  const submissionType = await api.fetchSubmissionType(pk)
  mut.UPDATE_SUBMISSION_TYPE(submissionType)
}

async function getStudents (
  context: BareActionContext<RootState, RootState>,
  opt: { studentPks: Array<string>} = {
    studentPks: []
  }
) {
  if (opt.studentPks.length === 0) {
    const students = await api.fetchAllStudents()
    mut.SET_STUDENTS(students)
    return students
  } else {
    const students = await Promise.all(
      opt.studentPks.map((pk: string) =>
        api.fetchStudent({ pk })
      )
    )
    students.forEach(student => mut.SET_STUDENT(student))
    return students
  }
}
async function getSubmissionFeedbackTest (
  context: BareActionContext<RootState, RootState>,
  submissionPkObj: { pk: string }
) {
  const submission = await api.fetchSubmissionFeedbackTests(submissionPkObj)
  mut.SET_SUBMISSION(submission)
}
async function getStatistics () {
  const statistics = await api.fetchStatistics()
  mut.SET_STATISTICS(statistics)
}

function resetState ({ message }: {message: string}) {
  FeedbackTable.RESET_STATE()
  Assignments.RESET_STATE()
  SubmissionNotes.RESET_STATE()
  StudentPage.RESET_STATE()
  Authentication.RESET_STATE()
  Authentication.SET_MESSAGE(message)
  TutorOverview.RESET_STATE()
  mut.RESET_STATE()
}

function logout (
  context: BareActionContext<RootState, RootState>,
  message = ''
) {
  if (Authentication.isStudent && !ConfigModule.state.config.instanceSettings.exerciseMode) {
    Authentication.deactivateUserAccount()
  }
  if (Authentication.isTutorOrReviewer) {
    Assignments.cleanAssignments()
  }
  router.push({ name: 'login' }, () => {
    resetState({ message })
  })
}

const mb = getStoreBuilder<RootState>()

export const actions = {
  updateSubmissionTypes: mb.dispatch(updateSubmissionTypes),
  updateSubmissionType: mb.dispatch(updateSubmissionType),
  getExamTypes: mb.dispatch(getExamTypes),
  getStudents: mb.dispatch(getStudents),
  getSubmissionFeedbackTest: mb.dispatch(getSubmissionFeedbackTest),
  getStatistics: mb.dispatch(getStatistics),
  logout: mb.dispatch(logout)
}
