const gradySays = [
  'Now let\'s see if we can improve this with a little water, sir.',
  'Won\'t keep you a moment, sir.',
  'Grady, sir. Delbert Grady.',
  'Yes, sir.',
  'That\'s right, sir.',
  'Why no, sir. I don\'t believe so.',
  'Ah ha, it\'s coming off now, sir.',
  'Why no, sir. I don\'t believe so.',
  'Yes, sir.  I have a wife and two daughters, sir.',
  'Oh, they\'re somewhere around.  I\'m not quite sure at the moment, sir.',
  'That\'s strange, sir.  I don\'t have any recollection of that at all.',
  'I\'m sorry to differ with you, sir, but you are the caretaker.',
  'You have always been the caretaker, I should know, sir.',
  'I\'ve always been here.',
  'Indeed, he is, Mr. Torrance. A very willful boy. ',
  'A rather naughty boy, if I may be so bold, sir.',
  'Perhaps they need a good talking to, if you don\'t mind my saying so. Perhaps a bit more.',
  'My girls, sir, they didn\'t care for the Overlook at first.',
  'One of them actually stole a packet of matches and tried to burn it down.',
  'But I corrected them, sir.',
  'And when my wife tried to prevent me from doing my duty... I corrected her.'
]

export default gradySays
