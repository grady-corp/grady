import Vuex, { Plugin } from 'vuex'
import Vue from 'vue'
import { getStoreBuilder } from 'vuex-typex'

// @ts-ignore
import './modules/ui'
// @ts-ignore
import './modules/authentication'
// @ts-ignore
import './modules/feedback_list/feedback-table'
// @ts-ignore
import './modules/assignments'
// @ts-ignore
import './modules/submission-notes'
// @ts-ignore
import './modules/student-page'
// @ts-ignore
import './modules/tutor-overview'
// @ts-ignore
import './modules/config'

import './mutations'
import './actions'
import './getters'

import { UIState } from './modules/ui'
import { AuthState } from './modules/authentication'
import { AssignmentsState } from './modules/assignments'
import { FeedbackTableState } from './modules/feedback_list/feedback-table'
import { SubmissionNotesState } from './modules/submission-notes'
import { StudentPageState } from './modules/student-page'
import { TutorOverviewState } from './modules/tutor-overview'

import {
  Exam,
  Statistics,
  StudentInfoForListView,
  SubmissionNoType,
  SubmissionType,
} from '@/models'
import { FeedbackLabelsState } from './modules/feedback-labels'
import { ConfigState } from './modules/config'
import { StudentMap } from './mutations'

Vue.use(Vuex)

export interface RootInitialState {
    lastAppInteraction: number
    examTypes: {[pk: string]: Exam}
    submissionTypes: {[pk: string]: SubmissionType}
    submissions: {[pk: string]: SubmissionNoType}
    students: {[pk: string]: StudentInfoForListView}
    studentMap: StudentMap // is used to map obfuscated student data back to the original
    statistics: Statistics
}

export interface RootState extends RootInitialState{
  UI: UIState,
  Authentication: AuthState,
  FeedbackTable: FeedbackTableState,
  Assignments: AssignmentsState,
  SubmissionNotes: SubmissionNotesState,
  StudentPage: StudentPageState,
  TutorOverview: TutorOverviewState,
  FeedbackLabels: FeedbackLabelsState
  ConfigModule: ConfigState
}

export function initialState (): RootInitialState {
  return {
    lastAppInteraction: Date.now(),
    examTypes: {},
    submissionTypes: {},
    submissions: {},
    students: {},
    studentMap: {}, // is used to map obfuscated student data back to the original
    statistics: {
      submissionsPerType: 0,
      submissionsPerStudent: 0,
      currentMeanScore: 0,
      submissionTypeProgress: []
    }
  }
}

export const persistedStateKey = 'grady'

const store = getStoreBuilder<RootState>().vuexStore({
  strict: process.env.NODE_ENV === 'development',
  // TODO is there a better way than casting the initialState ?
  state: initialState() as RootState
})

export default store
