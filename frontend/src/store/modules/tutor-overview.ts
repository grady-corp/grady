import { fetchAllTutors, fetchActiveAssignments, deleteAssignment } from '@/api'
import { Tutor, Assignment } from '@/models'
import { RootState } from '../store'
import { getStoreBuilder, BareActionContext } from 'vuex-typex'
import { objectifyArray } from '@/util/helpers'

export interface TutorOverviewState {
    tutors: Tutor[],
    activeAssignments: {[ownerName: string]: Assignment[]}
}

function initialState (): TutorOverviewState {
  return {
    tutors: [],
    activeAssignments: {}
  }
}

type Context = BareActionContext<TutorOverviewState, RootState>

const mb = getStoreBuilder<RootState>().module('TutorOverview', initialState())

const stateGetter = mb.state()

function SET_TUTORS (state: TutorOverviewState, tutors: Tutor[]) {
  state.tutors = tutors
}

function SET_ASSIGNMENTS (state: TutorOverviewState, assignments: Assignment[]) {
  state.activeAssignments = assignments.reduce((acc: {[ownerName: string]: Assignment[]}, curr) => {
    if (!curr.owner) {
      throw new Error('Assignments must have owner information')
    }
    acc[curr.owner] ? acc[curr.owner].push(curr) : acc[curr.owner] = [curr]
    return acc
  }, {})
}

function RESET_STATE (state: TutorOverviewState) {
  Object.assign(state, initialState())
}

async function getTutors () {
  const tutors = await fetchAllTutors()
  TutorOverview.SET_TUTORS(tutors)
}

async function getActiveAssignments () {
  const assignments = await fetchActiveAssignments()
  TutorOverview.SET_ASSIGNMENTS(assignments)
}

async function deleteActiveAssignmentsOfTutor ({ state }: Context, tutor: Tutor) {
  const assignments = state.activeAssignments[tutor.pk]
  const promises = assignments.map(assignment => { return deleteAssignment({ assignment }) })
  Promise.all(promises).finally(() => {
    TutorOverview.getActiveAssignments()
  })
}

export const TutorOverview = {
  get state () { return stateGetter() },

  SET_TUTORS: mb.commit(SET_TUTORS),
  SET_ASSIGNMENTS: mb.commit(SET_ASSIGNMENTS),
  RESET_STATE: mb.commit(RESET_STATE),

  getTutors: mb.dispatch(getTutors),
  getActiveAssignments: mb.dispatch(getActiveAssignments),
  deleteActiveAssignmentsOfTutor: mb.dispatch(deleteActiveAssignmentsOfTutor)
}
