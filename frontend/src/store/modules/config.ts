import { getStoreBuilder } from 'vuex-typex'
import { RootState } from '@/store/store'
import { Config } from '@/models'
import * as api from '@/api'

export interface ConfigState {
  config: Config
}

function initialState (): ConfigState {
  return {
    config: {
      timeDelta: 0,
      version: '',
      instanceSettings: {
        exerciseMode: false,
        singleCorrection: false,
        stopOnPass: false,
        showSolutionToStudents: false,
      }
    }
  }
}

const mb = getStoreBuilder<RootState>().module('ConfigModule', initialState())

const stateGetter = mb.state()

function SET_CONFIG (state: ConfigState, config: Config) {
  state.config = config
}

async function getConfig() {
  const config = await api.fetchConfig()
  ConfigModule.SET_CONFIG(config)
}


export const ConfigModule = {
  get state () { return stateGetter() },

  SET_CONFIG: mb.commit(SET_CONFIG),

  getConfig: mb.dispatch(getConfig)
}
