import { fetchStudentSelfData, fetchStudentSubmissions } from '@/api'
import { Exam, Submission, SubmissionList } from '@/models'
import { RootState } from '@/store/store'
import { Module } from 'vuex'
import { getStoreBuilder } from 'vuex-typex'
import { mutations } from '../mutations'

export interface StudentPageState {
  studentName: string
  exam?: Exam
  submissionsForList: Array<SubmissionList>
  submissionData: {[typePk: string]: Submission}
  visited: {[typePk: string]: boolean}
  loaded: boolean
}

function initialState (): StudentPageState {
  return {
    studentName: '',
    exam: undefined,
    submissionsForList: [],
    submissionData: {},
    visited: {},
    loaded: false
  }
}

const mb = getStoreBuilder<RootState>().module('SutdenPage', initialState())

const stateGetter = mb.state()

function SET_STUDENT_NAME (state: StudentPageState, name: string) {
  state.studentName = name
}
function SET_EXAM (state: StudentPageState, exam: Exam) {
  state.exam = exam
}
function SET_SUBMISSIONS_FOR_LIST (state: StudentPageState, submissions: SubmissionList[]) {
  state.submissionsForList = submissions
}
/**
 * Reduces the array submissionData returned by the /api/student-submissions
 * into an object where the keys are the SubmissionType id's and the values
 * the former array elements. This is done to have direct access to the data
 * via the SubmissionType id.
 */
function SET_FULL_SUBMISSION_DATA (state: StudentPageState, submissionData: Array<Submission>) {
  state.submissionData = submissionData.reduce((acc: {[pk: string]: Submission}, cur) => {
    acc[cur.type.pk] = cur
    return acc
  }, {})
}
function SET_VISITED (state: StudentPageState, visited: {index: string, visited: boolean}) {
  state.visited = { ...state.visited, [visited.index]: visited.visited }
}
function SET_LOADED (state: StudentPageState, loaded: boolean) {
  state.loaded = loaded
}
function RESET_STATE (state: StudentPageState) {
  Object.assign(state, initialState())
}

async function getStudentData () {
    const studentData = await fetchStudentSelfData()
    StudentPage.SET_STUDENT_NAME(studentData.name || '')
    StudentPage.SET_EXAM(studentData.exam)
    StudentPage.SET_SUBMISSIONS_FOR_LIST(studentData.submissions)
    StudentPage.SET_LOADED(true)
}

async function getStudentSubmissions () {
    const submissions = await fetchStudentSubmissions()
    StudentPage.SET_FULL_SUBMISSION_DATA(submissions)
    for (const submission of submissions) {
      mutations.UPDATE_SUBMISSION_TYPE(submission.type)
    }
}

export const StudentPage = {
  get state () { return stateGetter() },

  SET_STUDENT_NAME: mb.commit(SET_STUDENT_NAME),
  SET_EXAM: mb.commit(SET_EXAM),
  SET_SUBMISSIONS_FOR_LIST: mb.commit(SET_SUBMISSIONS_FOR_LIST),
  SET_FULL_SUBMISSION_DATA: mb.commit(SET_FULL_SUBMISSION_DATA),
  SET_VISITED: mb.commit(SET_VISITED),
  SET_LOADED: mb.commit(SET_LOADED),
  RESET_STATE: mb.commit(RESET_STATE),

  getStudentData: mb.dispatch(getStudentData),
  getStudentSubmissions: mb.dispatch(getStudentSubmissions)
}
