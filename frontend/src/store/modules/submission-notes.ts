import Vue from 'vue'
import * as hljs from 'highlight.js'
import * as api from '@/api'
import { Feedback, FeedbackComment, SubmissionNoType, CreateUpdateFeedback, SubmissionType } from '@/models'
import { RootState } from '@/store/store'
import { getStoreBuilder, BareActionContext } from 'vuex-typex'
import { syntaxPostProcess, highlightSubmissionText } from '@/util/helpers'
import { AxiosResponse } from 'axios'
import { Assignments } from './assignments'

export const subNotesEventBus = new Vue()

export interface SubmissionNotesState {
  submission: SubmissionNoType
  ui: {
    showEditorOnLine: { [lineNo: number]: boolean }
    selectedCommentOnLine: { [lineNo: number]: FeedbackComment }
    showFeedback: boolean
  },
  hasOrigFeedback: boolean
  origFeedback: Feedback
  updatedFeedback: CreateUpdateFeedback
  commentsMarkedForDeletion: { [pk: string]: FeedbackComment }
  changedLabels: boolean
}

function initialState (): SubmissionNotesState {
  return {
    submission: {
      text: '',
      pk: '',
      type: '',
      tests: [],
      sourceCodeAvailable: false
    },
    ui: {
      showEditorOnLine: {},
      selectedCommentOnLine: {},
      showFeedback: true
    },
    hasOrigFeedback: false,
    origFeedback: {
      pk: 0,
      score: undefined,
      isFinal: false,
      feedbackLines: {},
      labels: [],
    },
    updatedFeedback: {
      pk: 0,
      score: undefined,
      feedbackLines: {},
      labels: [],
    },
    commentsMarkedForDeletion: {},
    changedLabels: false
  }
}

const mb = getStoreBuilder<RootState>().module('SubmissionNotes', initialState())

const stateGetter = mb.state()

const submissionTypeGetter = mb.read(function submissionType(state, getters, rootState) {
  return rootState.submissionTypes[state.submission.type]
})
// highlight the submission the reduce the string
// submission.text into an object where the keys are the
// line indexes starting at one and the values the corresponding submission line
// this makes iterating over the submission much more pleasant
const submissionGetter = mb.read(function submission(state, getters) {
  //const language = getters.submissionType ? getters.submissionType.programmingLanguage : 'c'
  //return highlightSubmissionText(state.submission.text || '', language)
  return state.submission.text
})
const scoreGetter = mb.read(function score(state) {
  return state.updatedFeedback.score !== undefined ? state.updatedFeedback.score : state.origFeedback.score
})
const workInProgressGetter = mb.read(function workInProgress(state) {
  const openEditor = Object.values(state.ui.showEditorOnLine).reduce((acc, curr) => acc || curr, false)
  const feedbackWritten = Object.entries(state.updatedFeedback.feedbackLines || {}).length > 0
  return openEditor || feedbackWritten || state.changedLabels
})
const isFeedbackCreationGetter = mb.read(function isFeedbackCreation(state) {
  return !state.origFeedback['feedbackStageForUser'] ||
    state.origFeedback['feedbackStageForUser'] === 'feedback-creation'
})

/**
 * Getter function to determine if the current stage is feedback review / conflict resolution.
 */
const isConflictResolution = mb.read(function isConflictResolution(state) {
  return state.origFeedback['feedbackStageForUser'] &&
    state.origFeedback['feedbackStageForUser'] === 'feedback-review'
})

function SET_SUBMISSION(state: SubmissionNotesState, submission: SubmissionNoType) {
  state.submission = submission
  if (submission.feedback !== undefined) {
    SET_ORIG_FEEDBACK(state, submission.feedback)
  }
}
function SET_ORIG_FEEDBACK(state: SubmissionNotesState, feedback: Feedback) {
  if (feedback) {
    state.origFeedback = feedback
    state.hasOrigFeedback = true
  }
}
function SET_SHOW_FEEDBACK(state: SubmissionNotesState, val: boolean) {
  state.ui.showFeedback = val
}
function UPDATE_FEEDBACK_LINE(state: SubmissionNotesState, feedback: { lineNo: number, comment: Partial<FeedbackComment> }) {
  // explicit .toString() on lineNo is necessary because of Vue.set typings
  if (state.updatedFeedback.feedbackLines) {
    Vue.set(state.updatedFeedback.feedbackLines, feedback.lineNo.toString(), feedback.comment)
  }
}
function SET_FEEDBACK_LABELS(state: SubmissionNotesState, labels: number[]) {
  state.changedLabels = true
  state.updatedFeedback.labels = labels
}
function ADD_FEEDBACK_LABEL(state: SubmissionNotesState, label: number) {
  state.changedLabels = true
  state.updatedFeedback.labels.push(label)
}
function REMOVE_FEEDBACK_LABEL(state: SubmissionNotesState, label: number) {
  state.changedLabels = true
  const tmp = state.updatedFeedback.labels.filter((val) => {
    return val !== label
  })
  state.updatedFeedback.labels = tmp
}
function UPDATE_FEEDBACK_SCORE(state: SubmissionNotesState, score: number) {
  state.updatedFeedback.score = score
}
function DELETE_FEEDBACK_LINE(state: SubmissionNotesState, lineNo: number) {
  if (state.updatedFeedback.feedbackLines) {
    // Vue dosn't like objects that are indexed with numbers...
    Vue.delete(state.updatedFeedback.feedbackLines, lineNo as unknown as string)
  }
}
function TOGGLE_EDITOR_ON_LINE(state: SubmissionNotesState, { lineNo, comment }: { lineNo: number, comment: FeedbackComment }) {
  Vue.set(state.ui.selectedCommentOnLine, lineNo as unknown as string, comment)
  Vue.set(state.ui.showEditorOnLine, lineNo as unknown as string, !state.ui.showEditorOnLine[lineNo])
}
function MARK_COMMENT_FOR_DELETION(state: SubmissionNotesState, comment: FeedbackComment) {
  Vue.set(state.commentsMarkedForDeletion, comment.pk, comment)
}
function UN_MARK_COMMENT_FOR_DELETION(state: SubmissionNotesState, comment: FeedbackComment) {
  Vue.delete(state.commentsMarkedForDeletion, comment.pk)
}
function RESET_MARKED_COMMENTS_FOR_DELETE(state: SubmissionNotesState) {
  state.commentsMarkedForDeletion = {}
}
function RESET_UPDATED_FEEDBACK(state: SubmissionNotesState) {
  state.updatedFeedback = initialState().updatedFeedback
}
function RESET_STATE(state: SubmissionNotesState) {
  Object.assign(state, initialState())
}

async function deleteComments({ state }: BareActionContext<SubmissionNotesState, RootState>) {
  return Promise.all(
    Object.values(state.commentsMarkedForDeletion).map(comment => {
      return api.deleteComment(comment)
    })
  )
}
async function submitFeedback(
{ state }: BareActionContext<SubmissionNotesState, RootState>,
{ isFinal = false}):
Promise<AxiosResponse<void>[]> {

  let feedback: Partial<CreateUpdateFeedback> = {
    isFinal: isFinal,
    ofSubmission: state.submission.pk,
    feedbackLines: {},
    labels: state.updatedFeedback.labels
  }

  // omit labels for the request
  if (!state.changedLabels) {
    delete feedback.labels
  }

  if (state.updatedFeedback.score !== undefined) {
    feedback.score = state.updatedFeedback.score
  } else {
    feedback.score = state.origFeedback.score
  }

  // set the comments for the feedback lines accordingly
  for (const key of Object.keys(state.updatedFeedback.feedbackLines)) {
    const numKey = Number(key)

    numKey && feedback.feedbackLines
      && (feedback.feedbackLines[numKey] = state.updatedFeedback.feedbackLines[numKey])
  }

  const assignment = Assignments.state.currentAssignment
  if (assignment) {
    await api.submitFeedbackForAssignment({ feedback , assignment})
  } else if (state.hasOrigFeedback) {
    feedback.pk = state.origFeedback.pk
    await api.submitUpdatedFeedback({ feedback } as { feedback: CreateUpdateFeedback })
  } else {
    await api.submitFeedback({feedback} as { feedback: CreateUpdateFeedback })
  }
  // delete those comments that have been marked for deletion
  return SubmissionNotes.deleteComments()
}

export const SubmissionNotes = {
  get state() { return stateGetter() },
  get submissionType() { return submissionTypeGetter() },
  get submission() { return submissionGetter() },
  get score() { return scoreGetter() },
  get workInProgress() { return workInProgressGetter() },
  get isFeedbackCreation() { return isFeedbackCreationGetter() },
  get isConflictResolution() { return isFeedbackCreationGetter() },

  SET_SUBMISSION: mb.commit(SET_SUBMISSION),
  SET_ORIG_FEEDBACK: mb.commit(SET_ORIG_FEEDBACK),
  SET_SHOW_FEEDBACK: mb.commit(SET_SHOW_FEEDBACK),
  SET_FEEDBACK_LABELS: mb.commit(SET_FEEDBACK_LABELS),
  ADD_FEEDBACK_LABEL: mb.commit(ADD_FEEDBACK_LABEL),
  REMOVE_FEEDBACK_LABEL: mb.commit(REMOVE_FEEDBACK_LABEL),
  UPDATE_FEEDBACK_LINE: mb.commit(UPDATE_FEEDBACK_LINE),
  UPDATE_FEEDBACK_SCORE: mb.commit(UPDATE_FEEDBACK_SCORE),
  DELETE_FEEDBACK_LINE: mb.commit(DELETE_FEEDBACK_LINE),
  TOGGLE_EDITOR_ON_LINE: mb.commit(TOGGLE_EDITOR_ON_LINE),
  MARK_COMMENT_FOR_DELETION: mb.commit(MARK_COMMENT_FOR_DELETION),
  UN_MARK_COMMENT_FOR_DELETION: mb.commit(UN_MARK_COMMENT_FOR_DELETION),
  RESET_MARKED_COMMENTS_FOR_DELETE: mb.commit(RESET_MARKED_COMMENTS_FOR_DELETE),
  RESET_UPDATED_FEEDBACK: mb.commit(RESET_UPDATED_FEEDBACK),
  RESET_STATE: mb.commit(RESET_STATE),

  deleteComments: mb.dispatch(deleteComments),
  submitFeedback: mb.dispatch(submitFeedback)
}
