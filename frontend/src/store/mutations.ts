import Vue from 'vue'
import { getStoreBuilder } from 'vuex-typex'

import { initialState, RootState } from '@/store/store'
import { Exam, Statistics, StudentInfoForListView, SubmissionNoType, SubmissionType} from '@/models'

export const mb = getStoreBuilder<RootState>()

function SET_EXAM_TYPES (state: RootState, examTypes: Array<Exam>) {
  state.examTypes = examTypes.reduce((acc: {[pk: string]: Exam}, curr) => {
    acc[curr.pk] = curr
    return acc
  }, {})
}
function SET_STUDENTS (state: RootState, students: Array<StudentInfoForListView>) {
  state.students = students.reduce((acc: {[pk: string]: StudentInfoForListView}, curr) => {
    acc[curr.pk] = mapStudent(curr, state.studentMap)
    return acc
  }, {})
}
function SET_STUDENT (state: RootState, student: StudentInfoForListView) {
  Vue.set(state.students, student.pk, mapStudent({
    ...state.students[student.pk],
    ...student
  }, state.studentMap))
}
// TODO proper types for student map
export interface StudentMap {
  [pseudoMatrikelNo: string]: {
    matrikelNo: string,
    name: string,
    email: string
  }
}
function SET_STUDENT_MAP (state: RootState, map: StudentMap) {
  state.studentMap = map
}
function SET_SUBMISSION (state: RootState, submission: SubmissionNoType) {
  Vue.set(state.submissions, submission.pk, submission)
}
function SET_STATISTICS (state: RootState, statistics: Statistics) {
  state.statistics = {
    ...state.statistics,
    ...statistics
  }
}
function UPDATE_SUBMISSION_TYPE (state: RootState, submissionType: SubmissionType) {
  const updatedSubmissionType = {
    ...state.submissionTypes[submissionType.pk],
    ...submissionType
  }
  Vue.set(state.submissionTypes, submissionType.pk, updatedSubmissionType)
}
// this func is being exported to use it's name in the latInteractionPlugin
export function SET_LAST_INTERACTION (state: RootState) {
  state.lastAppInteraction = Date.now()
}
function RESET_STATE (state: RootState) {
  Object.assign(state, initialState())
}

function mapStudent (student: StudentInfoForListView, map: any) {
  if (Object.keys(map).length > 0) {
    if (!student.matrikelNo) {
      throw Error('Student objects need matrikelNo key in order to apply mapping')
    }
    return {
      ...student,
      ...map[student.matrikelNo]
    }
  }
  return student
}

export const mutations = {
  SET_LAST_INTERACTION: mb.commit(SET_LAST_INTERACTION),
  SET_EXAM_TYPES: mb.commit(SET_EXAM_TYPES),
  SET_STUDENTS: mb.commit(SET_STUDENTS),
  SET_STUDENT: mb.commit(SET_STUDENT),
  SET_STUDENT_MAP: mb.commit(SET_STUDENT_MAP),
  SET_SUBMISSION: mb.commit(SET_SUBMISSION),
  SET_STATISTICS: mb.commit(SET_STATISTICS),
  UPDATE_SUBMISSION_TYPE: mb.commit(UPDATE_SUBMISSION_TYPE),
  RESET_STATE: mb.commit(RESET_STATE)
}
