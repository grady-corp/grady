import Vue from 'vue'

const shortkeyTypes = {
  numeric: (key: string) => { return Number(key) >= 0 && Number(key) <= 9 }
}

const handlerFunc = (el: any, bind: any) => {
  return (event: KeyboardEvent) => {
    // handle numeric key press
    if (bind.value === 'numeric' && shortkeyTypes.numeric(event.key)) {
      const e = new KeyboardEvent('shortkey', { bubbles: false, key: event.key })
      el.dispatchEvent(e)
    }
  }
}

// add the v-shortkey directive to Vue
// usage: <tag v-shortkey="<shortkeyType>" @shortkey="<handlerFunc>"></tag>
Vue.directive('shortkey', {
  bind: (el, bind) => {
    window.addEventListener('keypress', handlerFunc(el, bind))
  },
  unbind: (el, bind) => {
    window.removeEventListener('keypress', handlerFunc(el, bind))
  }
})