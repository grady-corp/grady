type Rule = (value: unknown) => true | string

export const required: Rule = function (v) {
	return !!v || 'This field is required.'
}
