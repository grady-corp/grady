from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains

from constance.test import override_config
from core.models import UserAccount
from util.factories import make_test_data

from functional_tests.util import (
    GradyTestCase,
    login,
    reset_browser_after_test,
    wait_cond_for_element,
)


class LoginPageTest(GradyTestCase):
    def setUp(self):
        self.test_data = make_test_data(
            data_dict={
                "exams": [
                    {
                        "module_reference": "Test Exam 01",
                        "total_score": 100,
                        "pass_score": 60,
                    }
                ],
                "submission_types": [
                    {
                        "name": "01. Sort this or that",
                        "full_score": 35,
                        "description": "Very complicated",
                        "solution": "Trivial!",
                    },
                    {
                        "name": "02. Merge this or that or maybe even this",
                        "full_score": 35,
                        "description": "Very complicated",
                        "solution": "Trivial!",
                    },
                ],
                "students": [
                    {"username": "student01", "password": "p", "exam": "Test Exam 01"},
                    {"username": "student02", "password": "p", "exam": "Test Exam 01"},
                ],
                "tutors": [
                    {"username": "tutor01", "password": "p"},
                    {"username": "tutor02", "password": "p"},
                ],
                "reviewers": [{"username": "reviewer", "password": "p"}],
                "submissions": [
                    {
                        "text": "function blabl\n"
                        "   on multi lines\n"
                        "       for blabla in bla:\n"
                        "           lorem ipsum und so\n",
                        "type": "01. Sort this or that",
                        "user": "student01",
                        "feedback": {
                            "score": 5,
                            "is_final": True,
                            "feedback_lines": {
                                "1": [
                                    {"text": "This is very bad!", "of_tutor": "tutor01"}
                                ],
                            },
                        },
                    },
                    {
                        "text": "function blabl\n"
                        "       asasxasx\n"
                        "           lorem ipsum und so\n",
                        "type": "02. Merge this or that or maybe even this",
                        "user": "student01",
                    },
                    {
                        "text": "function blabl\n"
                        "   on multi lines\n"
                        "       asasxasx\n"
                        "           lorem ipsum und so\n",
                        "type": "01. Sort this or that",
                        "user": "student02",
                    },
                    {
                        "text": "function lorem ipsum etc\n",
                        "type": "02. Merge this or that or maybe even this",
                        "user": "student02",
                    },
                ],
            }
        )

    def tearDown(self):
        self.saveScreenshots()
        reset_browser_after_test(self.browser, self.live_server_url)

    def _login(self, account):
        login(self.browser, self.live_server_url, account.username, 'p')

    def test_tutor_can_login(self):
        tutor = self.test_data["tutors"][0]
        self._login(tutor)
        self.assertTrue(self.browser.current_url.endswith("#/home"))

    def test_reviewer_can_login(self):
        reviewer = self.test_data["reviewers"][0]
        self._login(reviewer)
        self.assertTrue(self.browser.current_url.endswith("#/home"))

    def test_student_can_login(self):
        student = self.test_data["students"][0]
        self._login(student)
        self.assertTrue(self.browser.current_url.endswith("#/home"))

    @override_config(REGISTRATION_PASSWORD="pw")
    def test_can_register_account(self):
        username = "danny"
        password = "redrum-is-murder-reversed"
        instance_password = "pw"
        self.browser.get(self.live_server_url)
        self.browser.find_element(By.ID, "register-btn").click()
        self.browser.find_element(By.ID, "gdpr-notice")
        # self.browser.find_element(By.ID, 'accept-gdpr-notice').click()
        accept_btn = self.browser.find_element(By.ID, "accept-gdpr-notice")
        ActionChains(self.browser).move_to_element(accept_btn).click().perform()
        # enter username
        WebDriverWait(self.browser, 5).until(
            wait_cond_for_element(self.browser, 'input-register-username')
        )
        username_input = self.browser.find_element(By.ID, "input-register-username")
        username_input.send_keys(username)
        # enter REG pw
        WebDriverWait(self.browser, 5).until(
            wait_cond_for_element(self.browser, 'input-register-instance-password')
        )
        instance_password_input = self.browser.find_element(
            By.ID, "input-register-instance-password"
        )
        instance_password_input.send_keys(instance_password)
        # enter pw
        WebDriverWait(self.browser, 5).until(
            wait_cond_for_element(self.browser, 'input-register-password')
        )
        password_input = self.browser.find_element(By.ID, "input-register-password")
        password_input.send_keys(password)
        # enter pw-repeat
        WebDriverWait(self.browser, 5).until(
            wait_cond_for_element(self.browser, 'input-register-password-repeat')
        )
        password_repeat_input = self.browser.find_element(
            By.ID, "input-register-password-repeat"
        )
        password_repeat_input.send_keys(password)
        # submit
        WebDriverWait(self.browser, 5).until(
            wait_cond_for_element(self.browser, 'register-submit')
        )
        register_submit_el = self.browser.find_element(By.ID, "register-submit")
        register_submit_el.click()
        WebDriverWait(self.browser, 10).until_not(ec.visibility_of(register_submit_el))
        tutor = UserAccount.objects.get(username=username)
        self.assertEqual(UserAccount.TUTOR, tutor.role)
        self.assertFalse(tutor.is_active, "Tutors should be inactive after registered")
