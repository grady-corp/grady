from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By

from core import models
from core.models import UserAccount
from functional_tests.util import (
    GradyTestCase,
    login,
    wait_cond_for_element,
    subscription_menu_loaded_inactive,
    subscription_menu_loaded_active,
    extract_hrefs_hashes,
    reset_browser_after_test,
)
from util import factory_boys as fact


# This is a little hack to have Super test class which implements common behaviour
# and tests but is not executed. In order to have the testrunner ignore the
# FrontPageTestsTutorReviewer class we need to define it within a class which does
# not inherit from unittest
class UntestedParent:
    class FrontPageTestsTutorReviewer(GradyTestCase):
        username = None
        password = None
        role = None

        def setUp(self):
            self.sub_types = [None, None, None, None]
            self.sub_types[0] = fact.SubmissionTypeFactory.create()
            self.sub_types[1] = fact.SubmissionTypeFactory.create()
            self.sub_types[2] = fact.SubmissionTypeFactory.create()
            self.sub_types[3] = fact.SubmissionTypeFactory.create()

        def tearDown(self):
            self.saveScreenshots()
            reset_browser_after_test(self.browser, self.live_server_url)

        def _login(self):
            login(self.browser, self.live_server_url, self.username, self.password)

        def test_statistics_are_shown(self):
            self._login()
            WebDriverWait(self.browser, 10).until(
                wait_cond_for_element(self.browser, "correction-statistics")
            )
            statistics = self.browser.find_element(By.ID, "correction-statistics")
            title = statistics.find_element(By.CLASS_NAME, "title")
            self.assertEqual("Statistics", title.text)

        def test_existing_submission_types_are_displayed(self):
            self._login()
            WebDriverWait(self.browser, 10).until(subscription_menu_loaded_inactive)
            # find all the loaded submission elements and extract their text
            subscr_menu = self.browser.find_element(By.NAME, "subscription-menu")
            subtype_list_found = subscr_menu.find_elements(
                By.CLASS_NAME, "inactive-subscription"
            )
            subtype_names_found = []
            for subtype in subtype_list_found:
                subtype_names_found.append(subtype.get_attribute("innerText"))

            # compare against the existing subtypes from the DB
            subtype_list_expected = models.SubmissionType.objects.all()
            for subtype in subtype_list_expected:
                self.assertIn(subtype.name, subtype_names_found)

        def test_available_tasks_are_shown(self):
            # for this one, we need some submissions or none are available
            for i in range(0, 4):
                fact.SubmissionFactory.create(type=self.sub_types[i])
            self._login()
            WebDriverWait(self.browser, 10).until(
                # this function avoids a potential race condition, where the menu has finished
                # loading, but isnt displaying any open submissions yet, because it hasn't
                # polled for them yet. This leads to no hyperlink-elements being found -> fail
                subscription_menu_loaded_active
            )
            subscription_menu = self.browser.find_element(By.NAME, "subscription-menu")
            # this time, we expect that there are actual links present
            subtype_links_found = extract_hrefs_hashes(
                subscription_menu.find_elements(By.TAG_NAME, "a")
            )
            # compare the found links with the expected routes
            subtype_list_expected = models.SubmissionType.objects.all()
            default_group = models.Group.objects.first()
            for subtype in subtype_list_expected:
                self.assertIn(
                    f"/correction/{subtype.pk}/feedback-creation/{default_group.pk}",
                    subtype_links_found,
                )


class FrontPageTestsTutor(UntestedParent.FrontPageTestsTutorReviewer):
    def setUp(self):
        super().setUp()
        self.username = "tutor"
        self.password = "p"
        self.role = UserAccount.TUTOR
        fact.UserAccountFactory(username=self.username, password=self.password)

    def tearDown(self):
        reset_browser_after_test(self.browser, self.live_server_url)

    def test_side_bar_contains_correct_items(self):
        self._login()
        drawer = self.browser.find_element(By.CLASS_NAME, "v-navigation-drawer")
        links = extract_hrefs_hashes(drawer.find_elements(By.TAG_NAME, "a"))
        self.assertTrue(
            all(link in links for link in ["/home", "/feedback", "/statistics"])
        )
        footer = drawer.find_element(By.CLASS_NAME, "sidebar-footer")
        feedback_link = footer.find_element(By.CSS_SELECTOR, "#feedback-btn")
        self.assertEqual(
            "https://gitlab.gwdg.de/grady-corp/grady/issues",
            feedback_link.get_attribute("href"),
        )


class FrontPageTestsReviewer(UntestedParent.FrontPageTestsTutorReviewer):
    def setUp(self):
        super().setUp()
        self.username = "reviewer"
        self.password = "p"
        self.role = UserAccount.REVIEWER
        fact.UserAccountFactory(
            username=self.username, password=self.password, role=self.role
        )

    def tearDown(self):
        reset_browser_after_test(self.browser, self.live_server_url)

    def test_side_bar_contains_correct_items(self):
        self._login()
        drawer = self.browser.find_element(By.CLASS_NAME, "v-navigation-drawer")
        links = extract_hrefs_hashes(drawer.find_elements(By.TAG_NAME, "a"))
        self.assertTrue(
            all(
                link in links
                for link in [
                    "/home",
                    "/feedback",
                    "/statistics",
                    "/participant-overview",
                    "/tutor-overview",
                    "/exhaustive-overview",
                ]
            )
        )
        footer = drawer.find_element(By.CLASS_NAME, "sidebar-footer")
        feedback_link = footer.find_element(By.CSS_SELECTOR, "#feedback-btn")
        self.assertEqual(
            "https://gitlab.gwdg.de/grady-corp/grady/issues",
            feedback_link.get_attribute("href"),
        )
