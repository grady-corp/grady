import json
import os
from pathlib import Path
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains


from core.models import UserAccount
from functional_tests.util import (
    GradyTestCase,
    login,
    reset_browser_after_test,
    wait_cond_for_element,
    check_for_element,
)
from util import factory_boys as fact


def expect_file_to_be_downloaded(path):
    """
    Checks if a file has finished downloading by checking if a file exists at the path and
    no `.part` file is present in the directory containing path
    :param path: path to check
    :return:
    """

    def condition(*args):
        file_present = Path(path).is_file()
        partial_file_present = any(
            dir_path.suffix == ".part" for dir_path in Path(path).parent.iterdir()
        )
        return file_present and not partial_file_present

    return condition


JSON_EXPORT_FILE = os.path.join(os.path.dirname(__file__), "export.json")


class ExportTestModal(GradyTestCase):
    username = None
    password = None
    role = None

    def setUp(self):
        self.username = "reviewer"
        self.password = "p"
        self.role = UserAccount.REVIEWER
        fact.UserAccountFactory(
            username=self.username, password=self.password, role=self.role
        )
        # create ONLY submission type
        self.sub_types = [None]
        self.sub_types[0] = fact.SubmissionTypeFactory.create()

    def tearDown(self):
        self.saveScreenshots()
        reset_browser_after_test(self.browser, self.live_server_url)

    def _login(self):
        login(self.browser, self.live_server_url, self.username, self.password)

    def _export_btn_is_not_green(self, *args):
        exports_btn = self.browser.find_element(By.ID, "export-btn")
        return "success" not in exports_btn.get_attribute("class")

    def _export_btn_is_green(self, *args):
        exports_btn = self.browser.find_element(By.ID, "export-btn")
        return "success" in exports_btn.get_attribute("class")

    def test_export_red_uncorrected_submissions(self):
        # create an actual submission
        fact.SubmissionFactory.create(type=self.sub_types[0])
        # login and check
        self._login()
        WebDriverWait(self.browser, 10).until(self._export_btn_is_not_green)

    def test_export_green_all_corrected(self):
        self._login()
        WebDriverWait(self.browser, 10).until(self._export_btn_is_green)

    def test_export_warning_tooltip_uncorrected_submissions(self):
        # create an actual submission
        fact.SubmissionFactory.create(type=self.sub_types[0])
        # login and check
        self._login()
        self.browser.execute_script(
            "document.getElementById('export-btn').dispatchEvent(new Event('mouseenter'));"
        )
        WebDriverWait(self.browser, 5).until(
            wait_cond_for_element(self.browser, "uncorrected-tooltip")
        )
        self.assertEqual(False, check_for_element(self.browser, "corrected-tooltip"))
        self.assertEqual(True, check_for_element(self.browser, "uncorrected-tooltip"))

    def test_export_all_good_tooltip_all_corrected(self):
        self._login()
        self.browser.execute_script(
            "document.getElementById('export-btn').dispatchEvent(new Event('mouseenter'));"
        )
        WebDriverWait(self.browser, 5).until(
            wait_cond_for_element(self.browser, "corrected-tooltip")
        )
        self.assertEqual(True, check_for_element(self.browser, "corrected-tooltip"))
        self.assertEqual(False, check_for_element(self.browser, "uncorrected-tooltip"))

    def test_export_list_popup_contains_correct_items(self):
        self._login()
        export_btn = self.browser.find_element(By.ID, "export-btn")
        export_btn.click()
        export_menu = self.browser.find_element(
            By.CLASS_NAME, "menuable__content__active"
        )
        export_list = export_menu.find_element(By.CLASS_NAME, "v-list")
        list_elements = export_list.find_elements(By.TAG_NAME, "div")
        self.assertEqual(2, len(list_elements))
        self.assertEqual("Export student scores", list_elements[0].text)
        self.assertEqual("Export whole instance data", list_elements[1].text)

    def test_export_instance(self):
        # create an actual submission
        fact.SubmissionFactory.create(type=self.sub_types[0])
        # login and check
        self._login()
        self.browser.find_element(By.ID, "export-btn").click()
        self.browser.find_element(By.ID, "export-list1").click()
        instance_export_modal = self.browser.find_element(
            By.ID, "instance-export-modal"
        )
        export_btn = instance_export_modal.find_element(By.ID, "instance-export-dl")
        ActionChains(self.browser).move_to_element(export_btn).click().perform()
        WebDriverWait(self.browser, 10).until(
            expect_file_to_be_downloaded(JSON_EXPORT_FILE)
        )
        try:
            with open(JSON_EXPORT_FILE) as f:
                data = json.load(f)
            self.assertEqual(
                data["examTypes"][0]["moduleReference"], "B.Inf.4242 Test Module"
            )
        except Exception as e:
            raise e
        finally:
            os.remove(JSON_EXPORT_FILE)

    def test_export_student_info_as_json(self):
        # create a student + an actual submission of THAT student
        temp_student = fact.StudentInfoFactory()
        fact.SubmissionFactory.create(type=self.sub_types[0], student=temp_student)
        # login and check
        self._login()
        self.browser.find_element(By.ID, "export-btn").click()
        self.browser.find_element(By.ID, "export-list0").click()
        data_export_modal = self.browser.find_element(By.ID, "data-export-modal")
        data_export_btn = data_export_modal.find_element(
            By.ID, "export-data-download-btn"
        )
        ActionChains(self.browser).move_to_element(data_export_btn).click().perform()
        WebDriverWait(self.browser, 10).until(
            expect_file_to_be_downloaded(JSON_EXPORT_FILE)
        )
        try:
            with open(JSON_EXPORT_FILE) as f:
                data = json.load(f)
            self.assertEqual(data[0]["Matrikel"], temp_student.matrikel_no)
            self.assertEqual(data[0]["Username"], temp_student.user.username)
            self.assertEqual(data[0]["Exam"], "B.Inf.4242 Test Module")
            self.assertEqual(data[0]["Scores"][0]["score"], 0)
        except Exception as e:
            raise e
        finally:
            os.remove(JSON_EXPORT_FILE)
