import os
import sys  # use this to call sys.__std__.write() to log errors
from itertools import islice
from typing import Sequence
from outcome import Outcome, Error as OutcomeError
import time
import datetime

from selenium import webdriver
from selenium.webdriver import FirefoxProfile
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import (
    StaleElementReferenceException,
    NoSuchElementException,
)
from selenium.webdriver.common.by import By
from django.core.exceptions import ObjectDoesNotExist
from django.test import LiveServerTestCase

SCREENSHOTS = os.path.join(os.path.dirname(__file__), "screenshots")


class GradyTestCase(LiveServerTestCase):
    browser: webdriver.Firefox = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.browser = create_browser()

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        cls.browser.quit()

    def saveScreenshots(self):
        try:
            os.mkdir(SCREENSHOTS)
        except FileExistsError:
            pass
        outcome: Outcome = self._outcome
        print(outcome)
        if isinstance(outcome, OutcomeError):
            print(outcome.error)
            # try:
            #     outcome.unwrap()
            # except Exception as error:
            # print(error)
            self.browser.get_screenshot_as_file(
                os.path.join(SCREENSHOTS, self.id() + ".png")
            )


def create_browser() -> webdriver.Firefox:
    profile = FirefoxProfile()
    profile.set_preference("dom.disable_beforeunload", True)
    profile.set_preference("browser.download.folderList", 2)
    profile.set_preference("browser.download.dir", os.path.dirname(__file__))
    profile.set_preference("browser.download.useDownloadDir", True)
    profile.set_preference("browser.download.panel.shown", False)
    profile.set_preference(
        "browser.helperApps.neverAsk.saveToDisk",
        "application/vnd.hzn-3d-crossword;video/3gpp;video/3gpp2;application/vnd.mseq;application/vnd.3m.post-it-notes;application/vnd.3gpp.pic-bw-large;application/vnd.3gpp.pic-bw-small;application/vnd.3gpp.pic-bw-var;application/vnd.3gp2.tcap;application/x-7z-compressed;application/x-abiword;application/x-ace-compressed;application/vnd.americandynamics.acc;application/vnd.acucobol;application/vnd.acucorp;audio/adpcm;application/x-authorware-bin;application/x-athorware-map;application/x-authorware-seg;application/vnd.adobe.air-application-installer-package+zip;application/x-shockwave-flash;application/vnd.adobe.fxp;application/pdf;application/vnd.cups-ppd;application/x-director;applicaion/vnd.adobe.xdp+xml;application/vnd.adobe.xfdf;audio/x-aac;application/vnd.ahead.space;application/vnd.airzip.filesecure.azf;application/vnd.airzip.filesecure.azs;application/vnd.amazon.ebook;application/vnd.amiga.ami;applicatin/andrew-inset;application/vnd.android.package-archive;application/vnd.anser-web-certificate-issue-initiation;application/vnd.anser-web-funds-transfer-initiation;application/vnd.antix.game-component;application/vnd.apple.installe+xml;application/applixware;application/vnd.hhe.lesson-player;application/vnd.aristanetworks.swi;text/x-asm;application/atomcat+xml;application/atomsvc+xml;application/atom+xml;application/pkix-attr-cert;audio/x-aiff;video/x-msvieo;application/vnd.audiograph;image/vnd.dxf;model/vnd.dwf;text/plain-bas;application/x-bcpio;application/octet-stream;image/bmp;application/x-bittorrent;application/vnd.rim.cod;application/vnd.blueice.multipass;application/vnd.bm;application/x-sh;image/prs.btif;application/vnd.businessobjects;application/x-bzip;application/x-bzip2;application/x-csh;text/x-c;application/vnd.chemdraw+xml;text/css;chemical/x-cdx;chemical/x-cml;chemical/x-csml;application/vn.contact.cmsg;application/vnd.claymore;application/vnd.clonk.c4group;image/vnd.dvb.subtitle;application/cdmi-capability;application/cdmi-container;application/cdmi-domain;application/cdmi-object;application/cdmi-queue;applicationvnd.cluetrust.cartomobile-config;application/vnd.cluetrust.cartomobile-config-pkg;image/x-cmu-raster;model/vnd.collada+xml;text/csv;application/mac-compactpro;application/vnd.wap.wmlc;image/cgm;x-conference/x-cooltalk;image/x-cmx;application/vnd.xara;application/vnd.cosmocaller;application/x-cpio;application/vnd.crick.clicker;application/vnd.crick.clicker.keyboard;application/vnd.crick.clicker.palette;application/vnd.crick.clicker.template;application/vn.crick.clicker.wordbank;application/vnd.criticaltools.wbs+xml;application/vnd.rig.cryptonote;chemical/x-cif;chemical/x-cmdf;application/cu-seeme;application/prs.cww;text/vnd.curl;text/vnd.curl.dcurl;text/vnd.curl.mcurl;text/vnd.crl.scurl;application/vnd.curl.car;application/vnd.curl.pcurl;application/vnd.yellowriver-custom-menu;application/dssc+der;application/dssc+xml;application/x-debian-package;audio/vnd.dece.audio;image/vnd.dece.graphic;video/vnd.dec.hd;video/vnd.dece.mobile;video/vnd.uvvu.mp4;video/vnd.dece.pd;video/vnd.dece.sd;video/vnd.dece.video;application/x-dvi;application/vnd.fdsn.seed;application/x-dtbook+xml;application/x-dtbresource+xml;application/vnd.dvb.ait;applcation/vnd.dvb.service;audio/vnd.digital-winds;image/vnd.djvu;application/xml-dtd;application/vnd.dolby.mlp;application/x-doom;application/vnd.dpgraph;audio/vnd.dra;application/vnd.dreamfactory;audio/vnd.dts;audio/vnd.dts.hd;imag/vnd.dwg;application/vnd.dynageo;application/ecmascript;application/vnd.ecowin.chart;image/vnd.fujixerox.edmics-mmr;image/vnd.fujixerox.edmics-rlc;application/exi;application/vnd.proteus.magazine;application/epub+zip;message/rfc82;application/vnd.enliven;application/vnd.is-xpr;image/vnd.xiff;application/vnd.xfdl;application/emma+xml;application/vnd.ezpix-album;application/vnd.ezpix-package;image/vnd.fst;video/vnd.fvt;image/vnd.fastbidsheet;application/vn.denovo.fcselayout-link;video/x-f4v;video/x-flv;image/vnd.fpx;image/vnd.net-fpx;text/vnd.fmi.flexstor;video/x-fli;application/vnd.fluxtime.clip;application/vnd.fdf;text/x-fortran;application/vnd.mif;application/vnd.framemaker;imae/x-freehand;application/vnd.fsc.weblaunch;application/vnd.frogans.fnc;application/vnd.frogans.ltf;application/vnd.fujixerox.ddd;application/vnd.fujixerox.docuworks;application/vnd.fujixerox.docuworks.binder;application/vnd.fujitu.oasys;application/vnd.fujitsu.oasys2;application/vnd.fujitsu.oasys3;application/vnd.fujitsu.oasysgp;application/vnd.fujitsu.oasysprs;application/x-futuresplash;application/vnd.fuzzysheet;image/g3fax;application/vnd.gmx;model/vn.gtw;application/vnd.genomatix.tuxedo;application/vnd.geogebra.file;application/vnd.geogebra.tool;model/vnd.gdl;application/vnd.geometry-explorer;application/vnd.geonext;application/vnd.geoplan;application/vnd.geospace;applicatio/x-font-ghostscript;application/x-font-bdf;application/x-gtar;application/x-texinfo;application/x-gnumeric;application/vnd.google-earth.kml+xml;application/vnd.google-earth.kmz;application/vnd.grafeq;image/gif;text/vnd.graphviz;aplication/vnd.groove-account;application/vnd.groove-help;application/vnd.groove-identity-message;application/vnd.groove-injector;application/vnd.groove-tool-message;application/vnd.groove-tool-template;application/vnd.groove-vcar;video/h261;video/h263;video/h264;application/vnd.hp-hpid;application/vnd.hp-hps;application/x-hdf;audio/vnd.rip;application/vnd.hbci;application/vnd.hp-jlyt;application/vnd.hp-pcl;application/vnd.hp-hpgl;application/vnd.yamaha.h-script;application/vnd.yamaha.hv-dic;application/vnd.yamaha.hv-voice;application/vnd.hydrostatix.sof-data;application/hyperstudio;application/vnd.hal+xml;text/html;application/vnd.ibm.rights-management;application/vnd.ibm.securecontainer;text/calendar;application/vnd.iccprofile;image/x-icon;application/vnd.igloader;image/ief;application/vnd.immervision-ivp;application/vnd.immervision-ivu;application/reginfo+xml;text/vnd.in3d.3dml;text/vnd.in3d.spot;mode/iges;application/vnd.intergeo;application/vnd.cinderella;application/vnd.intercon.formnet;application/vnd.isac.fcs;application/ipfix;application/pkix-cert;application/pkixcmp;application/pkix-crl;application/pkix-pkipath;applicaion/vnd.insors.igm;application/vnd.ipunplugged.rcprofile;application/vnd.irepository.package+xml;text/vnd.sun.j2me.app-descriptor;application/java-archive;application/java-vm;application/x-java-jnlp-file;application/java-serializd-object;text/x-java-source,java;application/javascript;application/json;application/vnd.joost.joda-archive;video/jpm;image/jpeg;video/jpeg;application/vnd.kahootz;application/vnd.chipnuts.karaoke-mmd;application/vnd.kde.karbon;aplication/vnd.kde.kchart;application/vnd.kde.kformula;application/vnd.kde.kivio;application/vnd.kde.kontour;application/vnd.kde.kpresenter;application/vnd.kde.kspread;application/vnd.kde.kword;application/vnd.kenameaapp;applicatin/vnd.kidspiration;application/vnd.kinar;application/vnd.kodak-descriptor;application/vnd.las.las+xml;application/x-latex;application/vnd.llamagraphics.life-balance.desktop;application/vnd.llamagraphics.life-balance.exchange+xml;application/vnd.jam;application/vnd.lotus-1-2-3;application/vnd.lotus-approach;application/vnd.lotus-freelance;application/vnd.lotus-notes;application/vnd.lotus-organizer;application/vnd.lotus-screencam;application/vnd.lotus-wordro;audio/vnd.lucent.voice;audio/x-mpegurl;video/x-m4v;application/mac-binhex40;application/vnd.macports.portpkg;application/vnd.osgeo.mapguide.package;application/marc;application/marcxml+xml;application/mxf;application/vnd.wolfrm.player;application/mathematica;application/mathml+xml;application/mbox;application/vnd.medcalcdata;application/mediaservercontrol+xml;application/vnd.mediastation.cdkey;application/vnd.mfer;application/vnd.mfmp;model/mesh;appliation/mads+xml;application/mets+xml;application/mods+xml;application/metalink4+xml;application/vnd.ms-powerpoint.template.macroenabled.12;application/vnd.ms-word.document.macroenabled.12;application/vnd.ms-word.template.macroenabed.12;application/vnd.mcd;application/vnd.micrografx.flo;application/vnd.micrografx.igx;application/vnd.eszigno3+xml;application/x-msaccess;video/x-ms-asf;application/x-msdownload;application/vnd.ms-artgalry;application/vnd.ms-ca-compressed;application/vnd.ms-ims;application/x-ms-application;application/x-msclip;image/vnd.ms-modi;application/vnd.ms-fontobject;application/vnd.ms-excel;application/vnd.ms-excel.addin.macroenabled.12;application/vnd.ms-excelsheet.binary.macroenabled.12;application/vnd.ms-excel.template.macroenabled.12;application/vnd.ms-excel.sheet.macroenabled.12;application/vnd.ms-htmlhelp;application/x-mscardfile;application/vnd.ms-lrm;application/x-msmediaview;aplication/x-msmoney;application/vnd.openxmlformats-officedocument.presentationml.presentation;application/vnd.openxmlformats-officedocument.presentationml.slide;application/vnd.openxmlformats-officedocument.presentationml.slideshw;application/vnd.openxmlformats-officedocument.presentationml.template;application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;application/vnd.openxmlformats-officedocument.spreadsheetml.template;application/vnd.openxmformats-officedocument.wordprocessingml.document;application/vnd.openxmlformats-officedocument.wordprocessingml.template;application/x-msbinder;application/vnd.ms-officetheme;application/onenote;audio/vnd.ms-playready.media.pya;vdeo/vnd.ms-playready.media.pyv;application/vnd.ms-powerpoint;application/vnd.ms-powerpoint.addin.macroenabled.12;application/vnd.ms-powerpoint.slide.macroenabled.12;application/vnd.ms-powerpoint.presentation.macroenabled.12;appliation/vnd.ms-powerpoint.slideshow.macroenabled.12;application/vnd.ms-project;application/x-mspublisher;application/x-msschedule;application/x-silverlight-app;application/vnd.ms-pki.stl;application/vnd.ms-pki.seccat;application/vn.visio;video/x-ms-wm;audio/x-ms-wma;audio/x-ms-wax;video/x-ms-wmx;application/x-ms-wmd;application/vnd.ms-wpl;application/x-ms-wmz;video/x-ms-wmv;video/x-ms-wvx;application/x-msmetafile;application/x-msterminal;application/msword;application/x-mswrite;application/vnd.ms-works;application/x-ms-xbap;application/vnd.ms-xpsdocument;audio/midi;application/vnd.ibm.minipay;application/vnd.ibm.modcap;application/vnd.jcp.javame.midlet-rms;application/vnd.tmobile-ivetv;application/x-mobipocket-ebook;application/vnd.mobius.mbk;application/vnd.mobius.dis;application/vnd.mobius.plc;application/vnd.mobius.mqy;application/vnd.mobius.msl;application/vnd.mobius.txf;application/vnd.mobius.daf;tex/vnd.fly;application/vnd.mophun.certificate;application/vnd.mophun.application;video/mj2;audio/mpeg;video/vnd.mpegurl;video/mpeg;application/mp21;audio/mp4;video/mp4;application/mp4;application/vnd.apple.mpegurl;application/vnd.msician;application/vnd.muvee.style;application/xv+xml;application/vnd.nokia.n-gage.data;application/vnd.nokia.n-gage.symbian.install;application/x-dtbncx+xml;application/x-netcdf;application/vnd.neurolanguage.nlu;application/vnd.na;application/vnd.noblenet-directory;application/vnd.noblenet-sealer;application/vnd.noblenet-web;application/vnd.nokia.radio-preset;application/vnd.nokia.radio-presets;text/n3;application/vnd.novadigm.edm;application/vnd.novadim.edx;application/vnd.novadigm.ext;application/vnd.flographit;audio/vnd.nuera.ecelp4800;audio/vnd.nuera.ecelp7470;audio/vnd.nuera.ecelp9600;application/oda;application/ogg;audio/ogg;video/ogg;application/vnd.oma.dd2+xml;applicatin/vnd.oasis.opendocument.text-web;application/oebps-package+xml;application/vnd.intu.qbo;application/vnd.openofficeorg.extension;application/vnd.yamaha.openscoreformat;audio/webm;video/webm;application/vnd.oasis.opendocument.char;application/vnd.oasis.opendocument.chart-template;application/vnd.oasis.opendocument.database;application/vnd.oasis.opendocument.formula;application/vnd.oasis.opendocument.formula-template;application/vnd.oasis.opendocument.grapics;application/vnd.oasis.opendocument.graphics-template;application/vnd.oasis.opendocument.image;application/vnd.oasis.opendocument.image-template;application/vnd.oasis.opendocument.presentation;application/vnd.oasis.opendocumen.presentation-template;application/vnd.oasis.opendocument.spreadsheet;application/vnd.oasis.opendocument.spreadsheet-template;application/vnd.oasis.opendocument.text;application/vnd.oasis.opendocument.text-master;application/vnd.asis.opendocument.text-template;image/ktx;application/vnd.sun.xml.calc;application/vnd.sun.xml.calc.template;application/vnd.sun.xml.draw;application/vnd.sun.xml.draw.template;application/vnd.sun.xml.impress;application/vnd.sun.xl.impress.template;application/vnd.sun.xml.math;application/vnd.sun.xml.writer;application/vnd.sun.xml.writer.global;application/vnd.sun.xml.writer.template;application/x-font-otf;application/vnd.yamaha.openscoreformat.osfpvg+xml;application/vnd.osgi.dp;application/vnd.palm;text/x-pascal;application/vnd.pawaafile;application/vnd.hp-pclxl;application/vnd.picsel;image/x-pcx;image/vnd.adobe.photoshop;application/pics-rules;image/x-pict;application/x-chat;aplication/pkcs10;application/x-pkcs12;application/pkcs7-mime;application/pkcs7-signature;application/x-pkcs7-certreqresp;application/x-pkcs7-certificates;application/pkcs8;application/vnd.pocketlearn;image/x-portable-anymap;image/-portable-bitmap;application/x-font-pcf;application/font-tdpfr;application/x-chess-pgn;image/x-portable-graymap;image/png;image/x-portable-pixmap;application/pskc+xml;application/vnd.ctc-posml;application/postscript;application/xfont-type1;application/vnd.powerbuilder6;application/pgp-encrypted;application/pgp-signature;application/vnd.previewsystems.box;application/vnd.pvi.ptid1;application/pls+xml;application/vnd.pg.format;application/vnd.pg.osasli;tex/prs.lines.tag;application/x-font-linux-psf;application/vnd.publishare-delta-tree;application/vnd.pmi.widget;application/vnd.quark.quarkxpress;application/vnd.epson.esf;application/vnd.epson.msf;application/vnd.epson.ssf;applicaton/vnd.epson.quickanime;application/vnd.intu.qfx;video/quicktime;application/x-rar-compressed;audio/x-pn-realaudio;audio/x-pn-realaudio-plugin;application/rsd+xml;application/vnd.rn-realmedia;application/vnd.realvnc.bed;applicatin/vnd.recordare.musicxml;application/vnd.recordare.musicxml+xml;application/relax-ng-compact-syntax;application/vnd.data-vision.rdz;application/rdf+xml;application/vnd.cloanto.rp9;application/vnd.jisp;application/rtf;text/richtex;application/vnd.route66.link66+xml;application/rss+xml;application/shf+xml;application/vnd.sailingtracker.track;image/svg+xml;application/vnd.sus-calendar;application/sru+xml;application/set-payment-initiation;application/set-reistration-initiation;application/vnd.sema;application/vnd.semd;application/vnd.semf;application/vnd.seemail;application/x-font-snf;application/scvp-vp-request;application/scvp-vp-response;application/scvp-cv-request;application/svp-cv-response;application/sdp;text/x-setext;video/x-sgi-movie;application/vnd.shana.informed.formdata;application/vnd.shana.informed.formtemplate;application/vnd.shana.informed.interchange;application/vnd.shana.informed.package;application/thraud+xml;application/x-shar;image/x-rgb;application/vnd.epson.salt;application/vnd.accpac.simply.aso;application/vnd.accpac.simply.imp;application/vnd.simtech-mindmapper;application/vnd.commonspace;application/vnd.ymaha.smaf-audio;application/vnd.smaf;application/vnd.yamaha.smaf-phrase;application/vnd.smart.teacher;application/vnd.svd;application/sparql-query;application/sparql-results+xml;application/srgs;application/srgs+xml;application/sml+xml;application/vnd.koan;text/sgml;application/vnd.stardivision.calc;application/vnd.stardivision.draw;application/vnd.stardivision.impress;application/vnd.stardivision.math;application/vnd.stardivision.writer;application/vnd.tardivision.writer-global;application/vnd.stepmania.stepchart;application/x-stuffit;application/x-stuffitx;application/vnd.solent.sdkm+xml;application/vnd.olpc-sugar;audio/basic;application/vnd.wqd;application/vnd.symbian.install;application/smil+xml;application/vnd.syncml+xml;application/vnd.syncml.dm+wbxml;application/vnd.syncml.dm+xml;application/x-sv4cpio;application/x-sv4crc;application/sbml+xml;text/tab-separated-values;image/tiff;application/vnd.to.intent-module-archive;application/x-tar;application/x-tcl;application/x-tex;application/x-tex-tfm;application/tei+xml;text/plain;application/vnd.spotfire.dxp;application/vnd.spotfire.sfs;application/timestamped-data;applicationvnd.trid.tpt;application/vnd.triscape.mxs;text/troff;application/vnd.trueapp;application/x-font-ttf;text/turtle;application/vnd.umajin;application/vnd.uoml+xml;application/vnd.unity;application/vnd.ufdl;text/uri-list;application/nd.uiq.theme;application/x-ustar;text/x-uuencode;text/x-vcalendar;text/x-vcard;application/x-cdlink;application/vnd.vsf;model/vrml;application/vnd.vcx;model/vnd.mts;model/vnd.vtu;application/vnd.visionary;video/vnd.vivo;applicatin/ccxml+xml,;application/voicexml+xml;application/x-wais-source;application/vnd.wap.wbxml;image/vnd.wap.wbmp;audio/x-wav;application/davmount+xml;application/x-font-woff;application/wspolicy+xml;image/webp;application/vnd.webturb;application/widget;application/winhlp;text/vnd.wap.wml;text/vnd.wap.wmlscript;application/vnd.wap.wmlscriptc;application/vnd.wordperfect;application/vnd.wt.stf;application/wsdl+xml;image/x-xbitmap;image/x-xpixmap;image/x-xwindowump;application/x-x509-ca-cert;application/x-xfig;application/xhtml+xml;application/xml;application/xcap-diff+xml;application/xenc+xml;application/patch-ops-error+xml;application/resource-lists+xml;application/rls-services+xml;aplication/resource-lists-diff+xml;application/xslt+xml;application/xop+xml;application/x-xpinstall;application/xspf+xml;application/vnd.mozilla.xul+xml;chemical/x-xyz;text/yaml;application/yang;application/yin+xml;application/vnd.ul;application/zip;application/vnd.handheld-entertainment+xml;application/vnd.zzazz.deck+xml",  # noqa
    )
    options = Options()
    if bool(os.environ.get("HEADLESS_TESTS", False)):
        options.add_argument("--headless")
    options.set_capability("unhandledPromptBehavior", "accept")
    options.set_capability("strictFileInteractability", False)
    options.profile = profile

    browser = webdriver.Firefox(options=options)
    # give the browser some time to setup + reset the impl.wait time
    browser.implicitly_wait(10)
    browser.implicitly_wait(0)
    browser.set_window_size(1920, 1080)
    return browser


def login(browser, live_server_url, username, password="p"):
    browser.get(live_server_url)
    WebDriverWait(browser, 5).until(
        ec.element_to_be_clickable((By.ID, "username"))
    )
    browser.find_element(By.ID, 'username').send_keys(username)
    WebDriverWait(browser, 5).until(
        ec.element_to_be_clickable((By.ID, "password"))
    )
    browser.find_element(By.ID, 'password').send_keys(password)
    WebDriverWait(browser, 5).until(
        ec.element_to_be_clickable((By.ID, "access-btn"))
    )
    click_el_center(browser, 'access-btn', By.ID)
    WebDriverWait(browser, 10).until(
        ec.url_contains("/home"),
        message="Login didn't get through to home screen."
    )


def logout(browser):
    # click (possibly obstructed) user-options menu
    temp_btn = browser.find_element(By.ID, "user-options")
    ActionChains(browser).move_to_element(temp_btn).click().perform()
    # click (possibly obstructed) logout button
    WebDriverWait(browser, 5).until(
        ec.element_to_be_clickable((By.ID, "logout-btn")),
        message="Logout Button did not turn clickable",
    )
    temp_btn = browser.find_element(By.ID, "logout-btn")
    ActionChains(browser).move_to_element(temp_btn).click().perform()
    WebDriverWait(browser, 10).until(
        ec.url_contains("/login"),
        message="Logout didn't get through to login screen."
    )


def reset_browser_after_test(browser: webdriver.Firefox, live_server_url):
    max_attempts = 5
    cur_attempts = 0
    while cur_attempts < max_attempts:
        try:
            while len(browser.window_handles) > 1:
                browser.close()
                browser.switch_to.window(
                    browser.window_handles[len(browser.window_handles) - 1]
                )
            browser.get(live_server_url)
            return
        except Exception as err:
            sys.__stdout__.write(f"failed to reset browser (attempt: {cur_attempts})\n")
            sys.__stdout__.write(f"  --> {err}: {type(err).__name__}\n")
        finally:
            cur_attempts = cur_attempts + 1


def nth(iterable, n, default=None):
    "Returns the nth item or a default value"
    return next(islice(iterable, n, None), default)


def extract_hrefs_hashes(web_elements: Sequence[WebElement]):
    return [
        nth(el.get_attribute("href").split("#"), 1, "")
        for el in web_elements
        if el.get_attribute("href")
    ]


# A function that takes the a browser client
# and returns a function that can be used as a condition for
# WebDriverWait
def subscriptions_loaded_cond(browser):
    def loaded(*args):
        for i in range(2):
            try:
                tasks_el = browser.find_element(By.NAME, "subscription-menu")
                tasks_el.find_element(By.CLASS_NAME, "v-progress-circular")
            except StaleElementReferenceException:
                pass
            except NoSuchElementException:
                return True
        return False

    return loaded


# returns a function that can be used as a callback for WebDriverWait
# the resulting functions returns True if the given query would return at least one object
def query_returns_object(model_class, **kwargs):
    def query(*args):
        try:
            model_class.objects.get(**kwargs)
        except ObjectDoesNotExist:
            return False
        return True

    return query


# -- s : Selector to be used for locating the element
# -- p : Parent from which to start searching from; if None, defaults to driver
def check_for_element(driver, el_ID, s=By.ID, p=None):
    if p is None:
        p = driver
    try:
        temp = p.find_elements(s, el_ID)
        return len(temp) > 0
    except Exception as err:
        sys.__stdout__.write(
            f"encountered Exception within check_for_element({el_ID})!\n"
        )
        sys.__stdout__.write(f"  --> {err}: {type(err).__name__}\n")
        return False


# -- s : Selector to be used for locating the element
# -- p : Parent from which to start searching from; if None, defaults to driver
def wait_cond_for_element(driver, el_ID, s=By.ID, p=None):
    def temp_func(driver):
        return check_for_element(driver, el_ID, s=s, p=p)

    return temp_func


# allrounder for a "clean click" - checks existance and moves to target
# Note: this moves the cursor to the (visible) center of the element
# -- s : Selector to be used for locating the element
# -- p : Parent from which to start searching from; if None, defaults to driver
def click_el_center(driver, el_ID, s=By.ID, p=None):
    if p is None:
        p = driver
    # first wait for the element to be available at all
    WebDriverWait(driver, 10).until(
        wait_cond_for_element(driver, el_ID, s=s, p=p),
        message="[in click_el_center()]:\nelement didn't become available.",
    )
    target_el = p.find_element(s, el_ID)
    # then wait for the element to be clickable aswell
    WebDriverWait(driver, 10).until(
        ec.element_to_be_clickable(target_el),
        message="[in click_el_center()]:\nelement didn't become clickable.",
    )
    ActionChains(driver).move_to_element(target_el).click().perform()


# check if the subcription menu finished loading; this is usually pretty fast,
# but when setting a color to it (= simulating extra stress), drawing takes time
def subscription_menu_loaded_inactive(driver):
    subscr_menu = driver.find_element(By.NAME, "subscription-menu")
    subtype_list_found = subscr_menu.find_elements(
        By.CLASS_NAME, "inactive-subscription"
    )
    return len(subtype_list_found) > 0


# if you want to check if the menu loaded and has finished polling for open
# submissions, use this function instead (the other one can run into a race condition)
def subscription_menu_loaded_active(driver):
    subscr_menu = driver.find_element(By.NAME, "subscription-menu")
    subtype_list_found = subscr_menu.find_elements(By.CLASS_NAME, "active-subscription")
    return len(subtype_list_found) > 0


def get_stage_num(stage_name):
    if (stage_name == "initial"):
        return 0
    if (stage_name == "validate"):
        return 1
    if (stage_name == "review"):
        return 2
    return -1


def wait_until_element_finished_scrolling(test_class_instance, element):
    def condition(*args):
        try:
            # this throws ElementCouldNotBeScrolledIntoView if the element
            # is still going through some kind of animation
            element.click()
        except Exception:
            return False
        return True
    # return the function itself
    return condition


# stage can be 'initial', 'validate', or 'conflict'
def go_to_subscription(test_class_instance, stage="initial", sub_type=None):
    subscr_menu = test_class_instance.browser.find_element(By.NAME, "subscription-menu")

    # refresh the menu to be sure we are looking at up-to-date entries only
    WebDriverWait(test_class_instance.browser, 10).until(
        wait_cond_for_element(
            test_class_instance.browser,
            'refresh-subscription-menu',
            s=By.ID,
            p=subscr_menu
        ),
        message='refresh-subscription-menu Button didnt become available'
    )
    subscr_menu.find_element(By.ID, "refresh-subscription-menu").click()
    time.sleep(0.5)

    # find and navigate to the correct stage tab
    subscr_menu.find_element(By.XPATH, f'//*[contains(text(), "{stage}")]').click()
    time.sleep(0.5)
    # wait for the subscription menu to finish loading the correct subtype list
    stage_num = get_stage_num(stage)
    WebDriverWait(test_class_instance.browser, 10).until(
        wait_cond_for_element(test_class_instance.browser, f'subtype-list-stage-{stage_num}'),
        message=f"Subtype-List for Stage {stage} ({stage_num}) didn't become available"
    )
    WebDriverWait(test_class_instance.browser, 10).until(
        subscription_menu_loaded_active,
        message="Subtype List content didnt load (or was empty)\n",
    )

    # subtype_list = subscr_menu.find_element(By.ID, f'subtype-list-stage-{stage_num}')
    # build the correct XPATH
    if sub_type is None:
        sub_type = test_class_instance.sub_types[0]
    sub_type_xpath = (
        f'//*[contains(text(), "{sub_type.name}") '
        f'and not(contains(@class, "inactive-subscription")) '
        f'and contains(@class, "active-subscription") '
        f'and not(ancestor::div[contains(@style,"display: none;")])]'
    )
    WebDriverWait(test_class_instance.browser, 10).until(
        ec.element_to_be_clickable((By.XPATH, sub_type_xpath)),
        message="SubmissionType not clickable",
    )
    # NOTE: this seems wrong, because this feels like we are clicking the first XPATH
    #       match within in the menu, which feels like it should be an "initial" submission,
    #       but screenshot-exports show that this is not the case, so we keep this
    subscr_menu.find_element(By.XPATH, sub_type_xpath).click()
    # debug_take_screenshot(test_class_instance.browser, f'clicked_sub_OfStage_{stage}.png')
    # and wait for the page to change
    WebDriverWait(test_class_instance.browser, 10).until(
        ec.url_contains("correction"),
        message='URL did not change to "correction" URL'
    )


def return_to_home(test_class_instance):
    temp = test_class_instance.browser.find_element(By.ID, "overview")
    temp.click()
    WebDriverWait(test_class_instance.browser, 5).until(
        ec.url_contains("home"), message="Couldn't reach Home."
    )
    temp = test_class_instance.browser.find_element(By.ID, "refresh-subscription-menu")
    temp.click()


def correct_some_submission(test_class_instance, sub_type=None):
    go_to_subscription(test_class_instance, stage="initial", sub_type=sub_type)
    code = reconstruct_submission_code(test_class_instance, sub_type)
    # correct the submission by assigning full score + submit
    test_class_instance.browser.find_element(By.ID, "score-full").click()
    submit_btn = test_class_instance.browser.find_element(By.ID, "submit-feedback")
    submit_btn.click()
    # wait for the browser to change to another page
    WebDriverWait(test_class_instance.browser, 10).until(
        wait_until_code_changes(test_class_instance, code),
        message="Submission Code did not change.",
    )
    return_to_home(test_class_instance)
    return code


def reconstruct_submission_code(test_class_instance, sub_type=None):
    def submission_tables_available(driver):
        return driver.find_element(By.ID, "submission-table") is not None

    WebDriverWait(test_class_instance.browser, 5).until(
        submission_tables_available, message="No Submission Table was available."
    )
    sub_table = test_class_instance.browser.find_element(By.ID, "submission-table")
    return reconstruct_code_from_table(sub_table)


def reconstruct_solution_code(test_class_instance, sub_type=None):
    solution_table = test_class_instance.browser.find_element(
        By.CLASS_NAME, "solution-table"
    )
    return reconstruct_code_from_table(solution_table)


def reconstruct_code_from_table(table_el):
    lines = table_el.find_elements(By.TAG_NAME, "tr")
    try:
        line_no_code_pairs = [
            (
                line.get_attribute("id"),
                # call get_attribute here to get non normalized text
                # https://github.com/SeleniumHQ/selenium/issues/2608
                line.find_element(By.CLASS_NAME, "code-cell-content")
                .find_element(By.CLASS_NAME, "code-line")
                .get_attribute("textContent"),
            )
            for line in lines
        ]
    except StaleElementReferenceException:
        return (
            None  # "None" instead of empty string, because code might actually be empty
        )
    line_no_code_pairs.sort(key=lambda x: x[0])  # sort by ids
    code_lines = list(zip(*line_no_code_pairs))[1]
    return "\n".join(code_lines)


# this fails when the last submission of any type is the one being corrected,
# because the additional reconstruct() call is unable to access any submissions
# -> additionally check, if we reached the "correction/ended" route
def wait_until_code_changes(test_class_instance, old_code, sub_type=None):
    def condition(*args):
        try:
            if "correction/ended" in test_class_instance.browser.current_url:
                return True
            # code might change during the call resulting in the exception
            new_code = reconstruct_submission_code(test_class_instance, sub_type)
            if new_code is None:
                return False
        except Exception:
            # if we reach the "correction/ended" route AFTER checking for it here,
            # the reconstruct() call in the try-Block will poll for sub-tables,
            # which it cannot possibly find; this leads to a TimeOutException.
            # After that exception is thrown, this func will not check the route
            # again; So instead, we catch all exceptions here:
            return False
        return old_code != new_code
    # return the function itself
    return condition


def wait_until_element_count_equals(test_class_instance, by, selector, count):
    def condition(*args):
        try:
            elements = test_class_instance.browser.find_elements(by, selector)
        except Exception:
            return False
        return len(elements) == count

    return condition


def report_time(self, message):
    return f"@ {datetime.datetime.now()} -- {message}\n"


# see test_auto_logout.py
def get_auth_token(driver):
    return driver.execute_script(
        'return document.getElementById("app").__vue__.$store'
        "._modules.root.state.Authentication.accessToken"
    )


def get_refresh_token(driver):
    return driver.execute_script(
        'return document.getElementById("app").__vue__.$store'
        "._modules.root.state.Authentication.refreshToken"
    )


def assertion_is_true(assertion_func):
    def condition(*args):
        try:
            assertion_func()
        except Exception:
            return False
        return True

    return condition


# use this function to get a list of attribute names of a given element
# (since vue renames some of them, this is a quick way to figure them out)
def debug_get_attribute_list(driver, element):
    return driver.execute_script(
        """
        var items = {};
        for (index = 0; index < arguments[0].attributes.length; ++index) {
            items[arguments[0].attributes[index].name] = arguments[0].attributes[index].value
        };
        return items;""",
        element,
    )


# print the amount and names (or IDs) of all currently opened window handles
def debug_print_window_handles(driver):
    sys.__stdout__.write("Current Window Handles:\n")
    cnt = len(driver.window_handles)
    sys.__stdout__.write(f"Handle count = {cnt}\n")
    sys.__stdout__.write("Handle names = \n")
    for handle in driver.window_handles:
        sys.__stdout__.write(f"{handle}\n")


# print something to std during a selenium test
def debug_print_msg(msg):
    f = open("manually_captured.log", "a")
    f.write(f"{msg}\n")
    f.close()
    sys.__stdout__.write(f"{msg}\n")


# make an on-demand screenshot
def debug_take_screenshot(driver, filename):
    driver.get_screenshot_as_file(filename)
