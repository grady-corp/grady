from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By

from selenium.webdriver.support.wait import WebDriverWait

from core.models import UserAccount
from functional_tests.util import (
    GradyTestCase,
    login,
    go_to_subscription,
    reconstruct_submission_code,
    correct_some_submission,
    reset_browser_after_test,
    wait_cond_for_element,
    # debug_print_msg,
    # get_auth_token,
    # get_refresh_token,
    # report_time,
)
from util import factory_boys as fact
import time

from rest_framework.test import APIClient


class TestFeedbackUpdate(GradyTestCase):
    username = None
    password = None

    def setUp(self):
        super().setUp()
        self.username = "tut"
        self.password = "p"
        self.user1 = fact.UserAccountFactory(
            username=self.username, password=self.password, role=UserAccount.TUTOR
        )
        self.other_username = "other_tut"
        self.other_password = "p"
        self.user2 = fact.UserAccountFactory(
            username=self.other_username, password=self.other_password, role=UserAccount.TUTOR
        )
        self.sub_types = [None]
        self.sub_types[0] = fact.SubmissionTypeFactory.create()
        fact.SubmissionFactory.create_batch(2, type=self.sub_types[0])
        self.api_client = APIClient()

    def tearDown(self):
        self.saveScreenshots()
        reset_browser_after_test(self.browser, self.live_server_url)

    def _login(self):
        login(self.browser, self.live_server_url, self.username, self.password)

    def test_updating_own_feedback_doesnt_invalidate_other_tutors_assignment(self):
        # First correct some submission as the first tutor
        login(self.browser, self.live_server_url, self.username, password=self.password)
        self.api_client.force_authenticate(user=self.user1)
        code = correct_some_submission(self, sub_type=self.sub_types[0])
        first_tab = self.browser.current_window_handle

        # open a new tab for a 2nd tutor, and wait for it to finish opening
        self.browser.execute_script("window.open()")
        WebDriverWait(self.browser, 10).until(
            ec.number_of_windows_to_be(2),
        )
        # properly and safely switch to 2nd tab
        for window_handle in self.browser.window_handles:
            if window_handle != first_tab:
                self.browser.switch_to.window(window_handle)
                break
        self.browser.get(self.live_server_url)
        # wait for the 2nd tab to load properly
        WebDriverWait(self.browser, 10).until(
            ec.url_contains("login"),
            "Couldnt locate 'login' in URL of 2nd tab",
        )
        # login as 2nd tutor
        second_tab = self.browser.current_window_handle
        login(self.browser, self.live_server_url, self.other_username, password=self.other_password)
        self.api_client.force_authenticate(user=self.user2)
        # and checkout the correction of tutor #1 under validations (should be the only one)
        go_to_subscription(self, stage="validate", sub_type=self.sub_types[0])
        other_code = reconstruct_submission_code(self)
        self.assertEqual(
            code,
            other_code,
            "Code for validation submissions is different than initial",
        )

        # Go to first tab and update the submission as 1st tutor via the Feedback History page
        self.browser.switch_to.window(first_tab)
        self.api_client.force_authenticate(user=self.user1)

        # relogging
        self.browser.get(self.live_server_url)
        WebDriverWait(self.browser, 10).until(
            ec.url_contains("login"),
            "Couldnt locate 'login' in URL of 1st tab (2nd login)",
        )
        login(self.browser, self.live_server_url, self.username, password=self.password)

        time.sleep(0.5)
        WebDriverWait(self.browser, 10).until(
            wait_cond_for_element(self.browser, "feedback"),
            message="Feedback Tab did not become available in first Window."
        )
        self.browser.find_element(By.ID, "feedback").click()
        WebDriverWait(self.browser, 15).until(
            ec.visibility_of_element_located((By.CLASS_NAME, "feedback-row")),
            message="feedback-row did not become available in first Window again."
        )
        feedback_entry = self.browser.find_element(By.CLASS_NAME, "feedback-row")
        ActionChains(self.browser).move_to_element(feedback_entry).click().perform()

        WebDriverWait(self.browser, 5).until(
            wait_cond_for_element(self.browser, "submit-feedback"),
            message="submit-feedback did not become available in first Window again."
        )
        self.browser.find_element(By.ID, "submit-feedback").click()

        # as the second tutor, submit the validated feedback
        self.browser.switch_to.window(second_tab)
        self.api_client.force_authenticate(user=self.user2)
        self.browser.find_element(By.ID, "submit-feedback").click()
        WebDriverWait(self.browser, 10).until(
            ec.url_contains("ended"),
            "Couldnt locate 'ended' in URL of 2nd tab (thus, Feedback wasnt submitted)",
        )
