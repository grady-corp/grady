from selenium.webdriver import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By

from core.models import UserAccount, Submission, FeedbackComment
from functional_tests.util import (
    GradyTestCase,
    login,
    reset_browser_after_test,
    go_to_subscription,
    wait_until_code_changes,
    correct_some_submission,
    assertion_is_true,
    reconstruct_submission_code,
    wait_until_element_count_equals,
    reconstruct_solution_code,
    wait_cond_for_element
)
from util import factory_boys as fact


class UntestedParent:
    class TestFeedbackCreationGeneric(GradyTestCase):
        username = None
        password = None
        role = None

        def setUp(self):
            self.sub_types = [None]
            self.sub_types[0] = fact.SubmissionTypeFactory.create()
            fact.SubmissionFactory.create_batch(2, type=self.sub_types[0])

        def tearDown(self):
            self.saveScreenshots()
            reset_browser_after_test(self.browser, self.live_server_url)

        def _login(self):
            login(self.browser, self.live_server_url, self.username, self.password)

        def write_comments_on_lines(self, line_comment_tuples):
            """line_comment_tuples is an iterable containing tuples of
            (line_no, comment) where the line number starts at 1
            """
            sub_table = self.browser.find_element(By.CLASS_NAME, "submission-table")
            lines = sub_table.find_elements(By.TAG_NAME, "tr")

            for line_no, comment in line_comment_tuples:
                line = lines[line_no - 1]
                line.find_element(By.CLASS_NAME, "line-number-btn").click()

                WebDriverWait(self.browser, 5).until(
                    wait_cond_for_element(self.browser, "feedback-input-textarea", s=By.ID)
                )
                textarea = self.browser.find_element(By.ID, "feedback-input-textarea")
                textarea.send_keys(comment)
                line.find_element(By.ID, "submit-comment").click()

        def at_least_2_comments_loaded(self, driver):
            comments = driver.find_elements(By.CLASS_NAME, "dialog-box")
            return len(comments) >= 2

        def test_student_text_is_correctly_displayed(self):
            self._login()
            go_to_subscription(self)
            code = reconstruct_submission_code(self)
            # query db for Submission with seen code, throws if not present and test fails
            Submission.objects.get(text=code)

        def test_submission_type_is_correctly_displayed(self):
            self._login()
            go_to_subscription(self)
            sub_type_el = self.browser.find_element(By.ID, "submission-type")
            title = sub_type_el.find_element(By.CLASS_NAME, "title")
            self.assertEqual(
                f"{self.sub_types[0].name} - Full score: {self.sub_types[0].full_score}",
                title.text,
            )
            solution = reconstruct_solution_code(self)
            self.assertEqual(self.sub_types[0].solution, solution)
            description = sub_type_el.find_element(By.CLASS_NAME, "type-description")
            html_el_in_desc = description.find_element(By.TAG_NAME, "h1")
            self.assertEqual("This", html_el_in_desc.text)

        def test_test_output_is_displayed(self):
            # create a test for every submission
            test = None
            for submission in Submission.objects.all():
                test = fact.TestFactory.create(
                    submission=submission, annotation="This is a test"
                )
            self._login()
            go_to_subscription(self)
            tests = self.browser.find_element(By.ID, "submission-tests")
            name_label = tests.find_element(By.NAME, "test-name-label")
            name_label.click()
            self.assertIn(test.name, name_label.text)
            self.assertIn(test.label, name_label.text)
            test_output = tests.find_element(By.CLASS_NAME, "test-output")
            WebDriverWait(self.browser, 10).until(ec.visibility_of(test_output))
            self.assertEqual(test.annotation, test_output.text)

        def test_can_give_max_score(self):
            self._login()
            go_to_subscription(self)
            code = correct_some_submission(self, sub_type=self.sub_types[0])
            submission_for_code = Submission.objects.get(text=code)
            self.assertEqual(
                self.sub_types[0].full_score, submission_for_code.feedback.score
            )

        def test_zero_score_without_warning_gives_error(self):
            self._login()
            go_to_subscription(self)
            self.browser.find_element(By.ID, "score-zero").click()
            submit_btn = self.browser.find_element(By.ID, "submit-feedback")
            assert submit_btn.get_attribute("disabled")

        def test_can_give_zero_score(self):
            self._login()
            go_to_subscription(self)
            code = reconstruct_submission_code(self)
            self.browser.find_element(By.ID, "score-zero").click()
            self.write_comments_on_lines([(0, "A comment")])
            self.browser.find_element(By.ID, "submit-feedback").click()
            WebDriverWait(self.browser, 10).until(wait_until_code_changes(self, code))
            submission_for_code = Submission.objects.get(text=code)
            self.assertEqual(0, submission_for_code.feedback.score)

        def test_can_give_comments_and_decreased_score(self):
            self._login()
            go_to_subscription(self, sub_type=self.sub_types[0])
            code = reconstruct_submission_code(self)

            # give half full score
            score_input = self.browser.find_element(By.ID, "score-input")
            score_input.send_keys(self.sub_types[0].full_score // 2)

            # give feedback on first and last line of submission
            comment_text = "This is feedback"
            self.write_comments_on_lines(
                [(1, comment_text), (0, comment_text)]  # 0 corresponds to the last line
            )

            submit_btn = self.browser.find_element(By.ID, "submit-feedback")
            submit_btn.click()
            WebDriverWait(self.browser, 10).until(wait_until_code_changes(self, code))

            submission_for_code = Submission.objects.get(text=code)
            self.assertEqual(
                self.sub_types[0].full_score // 2, submission_for_code.feedback.score
            )
            self.assertEqual(2, submission_for_code.feedback.feedback_lines.count())
            fst_comment = FeedbackComment.objects.get(
                of_feedback=submission_for_code.feedback, of_line=1
            )
            self.assertEqual(comment_text, fst_comment.text)
            last_line_of_sub = len(submission_for_code.text.split("\n"))
            snd_comment = FeedbackComment.objects.get(
                of_feedback=submission_for_code.feedback, of_line=last_line_of_sub
            )
            self.assertEqual(comment_text, snd_comment.text)

        def test_can_skip_submission(self):
            self._login()
            go_to_subscription(self)
            code = reconstruct_submission_code(self)
            self.browser.find_element(By.ID, "skip-submission").click()
            WebDriverWait(self.browser, 10).until(wait_until_code_changes(self, code))

        def test_can_validate_submission(self):
            self._login()
            go_to_subscription(self)

            def correct():
                code = reconstruct_submission_code(self)
                self.write_comments_on_lines([(0, "A comment by me")])
                self.browser.find_element(By.ID, "score-zero").click()
                self.browser.find_element(By.ID, "submit-feedback").click()
                return code

            code = correct()
            WebDriverWait(self.browser, 10).until(wait_until_code_changes(self, code))
            correct()

            sub_url = "correction/ended"
            WebDriverWait(self.browser, 10).until(ec.url_contains(sub_url))

            # logs out user
            reset_browser_after_test(self.browser, self.live_server_url)

            user_snd = "tutor_snd"
            password = "p"
            fact.UserAccountFactory(username=user_snd, password=password)

            login(self.browser, self.live_server_url, user_snd, password)
            go_to_subscription(self, stage="validate")
            self.write_comments_on_lines([(0, "I disagree"), (1, "Full points!")])
            code_final = reconstruct_submission_code(self)
            self.browser.find_element(By.ID, "score-full").click()
            self.browser.find_element(By.ID, "submit-feedback").click()

            WebDriverWait(self.browser, 10).until(
                wait_until_code_changes(self, code_final)
            )
            code_non_final = reconstruct_submission_code(self)
            self.browser.find_element(By.CLASS_NAME, "final-checkbox").click()
            self.browser.find_element(By.ID, "submit-feedback").click()

            sub_url = "correction/ended"
            WebDriverWait(self.browser, 10).until(ec.url_contains(sub_url))

            # logs out user
            reset_browser_after_test(self.browser, self.live_server_url)

            user_rev = "rev"
            password = "p"
            role = UserAccount.REVIEWER
            fact.UserAccountFactory(username=user_rev, password=password, role=role)
            login(self.browser, self.live_server_url, user_rev, password)

            go_to_subscription(self, stage="review")
            code = reconstruct_submission_code(self)
            self.assertEqual(code, code_non_final)

            submission_for_code = Submission.objects.get(text=code_final)
            self.assertEqual(
                self.sub_types[0].full_score, submission_for_code.feedback.score
            )
            self.assertEqual(3, submission_for_code.feedback.feedback_lines.count())

            submission_for_code = Submission.objects.get(text=code_non_final)
            self.assertEqual(0, submission_for_code.feedback.score)
            self.assertEqual(1, submission_for_code.feedback.feedback_lines.count())

        def test_final_button_not_present_in_review_stage(self):
            self._login()
            go_to_subscription(self)

            def correct():
                code = reconstruct_submission_code(self)
                self.write_comments_on_lines([(0, "Some comment")])
                self.browser.find_element(By.ID, "score-zero").click()
                self.browser.find_element(By.ID, "submit-feedback").click()
                return code

            code = correct()
            WebDriverWait(self.browser, 10).until(wait_until_code_changes(self, code))
            correct()

            sub_url = "correction/ended"
            WebDriverWait(self.browser, 10).until(ec.url_contains(sub_url))

            reset_browser_after_test(
                self.browser, self.live_server_url
            )  # logs out user

            user_snd = "tutor_snd"
            password = "p"
            fact.UserAccountFactory(username=user_snd, password=password)

            login(self.browser, self.live_server_url, user_snd, password)
            go_to_subscription(self, stage="validate")
            self.write_comments_on_lines([(0, "I disagree"), (1, "Full points!")])
            code_final = reconstruct_submission_code(self)
            self.browser.find_element(By.ID, "score-full").click()
            self.browser.find_element(By.ID, "submit-feedback").click()

            WebDriverWait(self.browser, 10).until(
                wait_until_code_changes(self, code_final)
            )
            self.browser.find_element(By.CLASS_NAME, "final-checkbox").click()
            self.browser.find_element(By.ID, "submit-feedback").click()

            sub_url = "correction/ended"
            WebDriverWait(self.browser, 10).until(ec.url_contains(sub_url))

            reset_browser_after_test(self.browser, self.live_server_url)

            user_rev = "rev"
            password = "p"
            role = UserAccount.REVIEWER
            fact.UserAccountFactory(username=user_rev, password=password, role=role)
            login(self.browser, self.live_server_url, user_rev, password)

            go_to_subscription(self, stage="review")
            try:
                WebDriverWait(self.browser, 10).until(
                    ec.presence_of_element_located((By.CLASS_NAME, "final-checkbox"))
                )
                not_found = False
            except Exception:
                not_found = True

            assert not_found

        def test_comments_are_sorted_by_last_updated(self):
            # login as tutor A
            self._login()
            go_to_subscription(self)

            code = reconstruct_submission_code(self)
            self.browser.find_element(By.ID, "score-full").click()
            # give 1st feedback on first line (as tutor A)
            self.write_comments_on_lines([(1, "first ever comment")])
            submit_btn = self.browser.find_element(By.ID, "submit-feedback")
            submit_btn.click()
            WebDriverWait(self.browser, 10).until(wait_until_code_changes(self, code))
            reset_browser_after_test(
                self.browser, self.live_server_url
            )  # logs out user

            # login as tutor B
            user_snd = "tutor_snd"
            password = "p"
            fact.UserAccountFactory(username=user_snd, password=password)
            login(self.browser, self.live_server_url, user_snd, password)
            go_to_subscription(self, stage="validate")

            # give 2nd feedback on first line (as tutor B)
            self.write_comments_on_lines([(1, "the second comment")])
            self.browser.find_element(By.ID, "score-full").click()
            self.browser.find_element(By.CLASS_NAME, "final-checkbox").click()
            self.browser.find_element(By.ID, "submit-feedback").click()
            WebDriverWait(self.browser, 10).until(wait_until_code_changes(self, code))
            reset_browser_after_test(
                self.browser, self.live_server_url
            )  # logs out user

            # login as tutor A AGAIN
            self._login()

            # goto history
            self.browser.find_element(By.ID, "feedback").click()
            WebDriverWait(self.browser, 5).until(
                wait_cond_for_element(self.browser, "feedback-row", s=By.CLASS_NAME)
            )
            feedback_entry = self.browser.find_element(By.CLASS_NAME, "feedback-row")

            ActionChains(self.browser).move_to_element(feedback_entry).click().perform()

            # validate that second comment is under the first comment
            WebDriverWait(self.browser, 5).until(
                wait_until_element_count_equals(self, By.CLASS_NAME, "dialog-box", 2)
            )
            comments = self.browser.find_elements(By.CLASS_NAME, "dialog-box")
            self.assertEqual(len(comments), 2)
            first_text = comments[0].find_element(By.CLASS_NAME, "message")
            second_text = comments[1].find_element(By.CLASS_NAME, "message")
            self.assertEqual(first_text.text, "first ever comment")
            self.assertEqual(second_text.text, "the second comment")

            # update your own comment (as tutor A)
            self.write_comments_on_lines([(1, "first comment updated")])
            self.browser.find_element(By.ID, "score-full").click()
            self.browser.find_element(By.ID, "submit-feedback").click()
            WebDriverWait(self.browser, 5).until(
                wait_until_element_count_equals(self, By.CLASS_NAME, "dialog-box", 2)
            )

            # comments are sorted after about 0.5s of delay
            def assert_comments_are_sorted():
                # grab comments again
                comments = self.browser.find_elements(By.CLASS_NAME, "dialog-box")
                first_text = comments[0].find_element(By.CLASS_NAME, "message")
                second_text = comments[1].find_element(By.CLASS_NAME, "message")
                # check for order
                cond = True
                if not (first_text.text == "the second comment"):
                    cond = False
                if not (second_text.text == "first comment updated"):
                    cond = False
                return self.assertEqual(cond, True)

            # wait for it
            WebDriverWait(self.browser, 5).until(
                assertion_is_true(assert_comments_are_sorted)
            )


class TestFeedbackCreationTutor(UntestedParent.TestFeedbackCreationGeneric):
    def setUp(self):
        super().setUp()
        self.username = "tutor"
        self.password = "p"
        self.role = UserAccount.TUTOR
        fact.UserAccountFactory(
            username=self.username, password=self.password, role=self.role
        )
