import datetime
import logging
import sys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.action_chains import ActionChains
from django.conf import settings

from core.models import UserAccount
from functional_tests.util import (
    GradyTestCase,
    login,
    reset_browser_after_test,
    get_auth_token,
    check_for_element,
)
from util import factory_boys as fact

# from rest_framework_simplejwt.settings import api_settings

log = logging.getLogger(__name__)


class TestAutoLogout(GradyTestCase):
    username = None
    password = None
    role = None
    test_jwt_lifetime = 8  # in seconds
    starting_timestamp = datetime.datetime.now()
    # set this to true to activate the time-reports
    verbose = False

    def setUp(self):
        self.username = "reviewer"
        self.password = "p"
        self.role = UserAccount.TUTOR
        fact.UserAccountFactory(
            username=self.username, password=self.password, role=self.role
        )

    def tearDown(self):
        # api_settings.reload_api_settings()
        self.saveScreenshots()
        reset_browser_after_test(self.browser, self.live_server_url)

    def _login(self):
        login(self.browser, self.live_server_url, self.username, self.password)

    # if this test ever becomes flakey again, this helps debugging
    def report_time(self, message):
        if self.verbose is False:
            return
        time_spent = datetime.datetime.now() - self.starting_timestamp
        sys.__stdout__.write(f"@ {time_spent} -- {message}\n")

    def is_logout_dialog_gone(self, driver):
        # check if the continue button exists
        continue_btn_up = check_for_element(driver, "continue-btn")
        if continue_btn_up:
            continue_btn = driver.find_element(By.ID, "continue-btn")
            if continue_btn.is_displayed() and continue_btn.is_enabled():
                ActionChains(driver).move_to_element(continue_btn).click().perform()
                self.report_time("Clicked Continue-Option")

        dialog_up = check_for_element(driver, "logout-dialog")
        # if the dialog is not there, we pass
        if dialog_up is False:
            return True
        # return wether the dialog is visible
        dialog = driver.find_element(By.ID, "logout-dialog")
        return dialog.is_displayed() is False

    def test_auto_logout_can_be_interrupted(self):
        with self.settings(
            SIMPLE_JWT={
                # setting this with ms to have more similar units to the Date.now() return in JS
                # Note: The Logout Cancellation Dialog Lifetime is set in frontend/src/components/
                #       AutoLogout.vue. It's currently 25% of the JWT delta, which means the Dialog
                #       will appear after the token expired to 75% of its Lifetime -> 0.75 * 8 = 6,
                #       so this test eats up at least 6s...
                "ACCESS_TOKEN_LIFETIME": datetime.timedelta(
                    milliseconds=self.test_jwt_lifetime * 1e3
                ),
                # "ROTATE_REFRESH_TOKENS": True, # would need to rotate refresh token
            }
        ):
            self._login()
            self.report_time("logged in")
            initial_token = get_auth_token(self.browser)

            # wait for the auto-logout warning to appear (at most JWT lifetime -1s)
            # Note: This needs a relatively high re-try rate, because there is not
            #       that much time left before the auto-logout goes through afterwards
            WebDriverWait(self.browser, (self.test_jwt_lifetime - 1), 0.1).until(
                ec.visibility_of_element_located((By.ID, "logout-dialog")),
                message="Auto Logout Warning did not appear.",
            )
            self.report_time("AutoLogout Dialog appeared")
            # also wait (briefly) for the continue option to actually work
            WebDriverWait(self.browser, 5, 0.1).until(
                ec.element_to_be_clickable((By.ID, "continue-btn")),
                message="Continue Button did not turn clickable",
            )
            self.report_time("Continue-Option is now clickable")

            # see if the Auto Logout Dialog went away
            WebDriverWait(self.browser, 10, 0.1).until(
                # this condition also tries to close the dialog on every attempt
                self.is_logout_dialog_gone,
                message="Auto Logout Warning did not DISappear.",
            )
            self.report_time("AutoLogout Dialog DISappeared")

            # check if we are still logged in (if we were logged out, we'd be in the /login route)
            self.assertNotIn("login", self.browser.current_url)
            # check if the JWT token was refreshed correctly
            new_token = get_auth_token(self.browser)
            self.report_time("new Auth Token aquired")
            self.assertNotEqual(initial_token, new_token)

    def test_jwt_delta_resets_correctly(self):
        self.assertNotEqual(
            settings.SIMPLE_JWT['ACCESS_TOKEN_LIFETIME'],
            self.test_jwt_lifetime * 1e3,
            "JWT Access Token Lifetime did not Reset."
        )
