from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains


from core import models
from functional_tests.util import (
    GradyTestCase,
    login,
    query_returns_object,
    reset_browser_after_test,
    assertion_is_true,
    check_for_element,
    click_el_center,
    wait_cond_for_element,
)
from util import factory_boys as fact


class TestSolutionComments(GradyTestCase):
    username = None
    password = None
    role = None

    def setUp(self):
        super().setUp()
        self.username = "tut"
        self.password = "p"
        fact.UserAccountFactory(
            username=self.username,
            password=self.password,
        )
        self.sub_types = [None]
        self.sub_types[0] = fact.SubmissionTypeFactory.create()

    def tearDown(self):
        self.saveScreenshots()
        reset_browser_after_test(self.browser, self.live_server_url)

    def _login(self):
        login(self.browser, self.live_server_url, self.username, self.password)

    def at_least_2_comments_loaded(self, driver):
        comments = driver.find_elements(By.CLASS_NAME, "dialog-box")
        return len(comments) >= 2

    def subtype_div_available(self, driver):
        sub_types_up = check_for_element(driver, "submission-types-list")
        if not sub_types_up:
            return False
        sub_types = driver.find_element(By.ID, "submission-types-list")
        return len(sub_types.find_elements(By.TAG_NAME, "div")) > 0

    def _write_comment(self, text="A comment", line_no=1):
        WebDriverWait(self.browser, 10).until(self.subtype_div_available)
        sub_types = self.browser.find_element(By.ID, "submission-types-list")
        sub_types.find_element(By.TAG_NAME, "div").click()
        WebDriverWait(self.browser, 5).until(
            wait_cond_for_element(self.browser, "solution-table", s=By.CLASS_NAME),
            message="solution-table element did not load",
        )
        solution_table = self.browser.find_element(By.CLASS_NAME, "solution-table")
        tr_of_line = solution_table.find_element(By.ID, f"solution-line-{line_no}")
        WebDriverWait(self.browser, 5).until(
            wait_cond_for_element(
                self.browser, "line-number-btn", s=By.CLASS_NAME, p=tr_of_line
            ),
            message="element line-number-btn didn't load.",
        )
        tr_of_line.find_element(By.CLASS_NAME, "line-number-btn").click()
        comment_input = tr_of_line.find_element(By.NAME, "solution-comment-input")
        comment_input.send_keys(text)
        click_el_center(self.browser, "submit-comment", p=solution_table)
        WebDriverWait(self.browser, 3).until(
            ec.invisibility_of_element_located((By.ID, "submit-comment"))
        )

    def _edit_comment(self, old_text, new_text) -> WebElement:
        solution_table = self.browser.find_element(By.CLASS_NAME, "solution-table")
        comment = solution_table.find_element(
            By.XPATH,
            f"//div[@class='dialog-box'  and .//*[contains(text(), '{old_text}')]]",
        )
        click_el_center(self.browser, "edit-button", s=By.CLASS_NAME, p=comment)
        comment_input = solution_table.find_element(By.NAME, "solution-comment-edit")
        comment_input.send_keys(new_text)
        solution_table.find_element(By.ID, "submit-comment").click()
        return comment

    def test_tutor_can_add_comment(self):
        self._login()
        comment_text = "A comment!"
        self._write_comment(comment_text, 1)
        solution_table = self.browser.find_element(By.CLASS_NAME, "solution-table")
        displayed_text = solution_table.find_element(By.CLASS_NAME, "message").text
        self.assertEqual(comment_text, displayed_text)
        comment_obj = models.SolutionComment.objects.first()
        self.assertEqual(comment_text, comment_obj.text)
        self.assertEqual(1, comment_obj.of_line)

    def test_tutor_can_delete_own_comment(self):
        self._login()
        self._write_comment()
        solution_table = self.browser.find_element(By.CLASS_NAME, "solution-table")
        solution_table.find_element(By.CLASS_NAME, "delete-button").click()
        # self.browser.find_element(By.ID, 'confirm-delete-comment').click()
        delete_btn = self.browser.find_element(By.ID, "confirm-delete-comment")
        ActionChains(self.browser).move_to_element(delete_btn).click().perform()
        WebDriverWait(self.browser, 10).until_not(
            query_returns_object(models.SolutionComment),
            "Solution comment not deleted.",
        )

    def test_tutor_can_edit_own_comment(self):
        self._login()
        old_text = "A comment"
        new_text = "A new text"
        self._write_comment(old_text)
        WebDriverWait(self.browser, 10).until(
            query_returns_object(models.SolutionComment)
        )
        comment_obj = models.SolutionComment.objects.first()
        self.assertEqual(old_text, comment_obj.text)
        comment_el = self._edit_comment(old_text, new_text)

        def text_is_updated():
            displayed_text = comment_el.find_element(By.CLASS_NAME, "message").text
            return self.assertEqual(new_text, displayed_text)

        WebDriverWait(self.browser, 3).until(assertion_is_true(text_is_updated))
        comment_obj.refresh_from_db()
        self.assertEqual(new_text, comment_obj.text)

    def test_tutor_can_not_delete_edit_other_comment(self):
        self._login()
        self._write_comment()
        username = "tut2"
        password = "p"
        fact.UserAccountFactory(username=username, password=password)
        reset_browser_after_test(self.browser, self.live_server_url)
        login(self.browser, self.live_server_url, username, password)
        WebDriverWait(self.browser, 10).until(self.subtype_div_available)
        sub_types = self.browser.find_element(By.ID, "submission-types-list")
        sub_types.find_element(By.TAG_NAME, "div").click()
        solution_table = self.browser.find_element(By.CLASS_NAME, "solution-table")
        # Set the implicit wait for those to shorter, to reduce test run time
        self.browser.implicitly_wait(2)
        edit_buttons = solution_table.find_elements(By.CLASS_NAME, "edit-button")
        delete_buttons = solution_table.find_elements(By.CLASS_NAME, "delete-button")
        self.browser.implicitly_wait(10)
        self.assertEqual(0, len(edit_buttons))
        self.assertEqual(0, len(delete_buttons))

    def test_reviewer_can_delete_tutor_comment(self):
        self._login()
        self._write_comment()
        username = "rev"
        password = "p"
        fact.UserAccountFactory(
            username=username, password=password, role=models.UserAccount.REVIEWER
        )
        reset_browser_after_test(self.browser, self.live_server_url)
        login(self.browser, self.live_server_url, username, password)
        WebDriverWait(self.browser, 10).until(self.subtype_div_available)
        sub_types = self.browser.find_element(By.ID, "submission-types-list")
        sub_types.find_element(By.TAG_NAME, "div").click()
        solution_table = self.browser.find_element(By.CLASS_NAME, "solution-table")
        solution_table.find_element(By.CLASS_NAME, "delete-button").click()
        # self.browser.find_element(By.ID, 'confirm-delete-comment').click()
        delete_btn = self.browser.find_element(By.ID, "confirm-delete-comment")
        ActionChains(self.browser).move_to_element(delete_btn).click().perform()
        WebDriverWait(self.browser, 10).until_not(
            ec.presence_of_element_located((By.CLASS_NAME, "dialog-box"))
        )
        WebDriverWait(self.browser, 10).until_not(
            query_returns_object(models.SolutionComment),
            "Solution comment not deleted.",
        )
